#ifndef EFFECTPEDAL_ACTIVIT_H
#define EFFECTPEDAL_ACTIVIT_H

#include <string>
#include <vector>
#include <stdexcept>
#include "utils.h"
#include "../controller/ButtonEvent.h"
#include "output/DisplayOutput.h"

class Activity {
public:
    Activity(Output *output);

    virtual ~Activity();

    /**
     * Called if a button was pressed. Returns the type of the activity that should be
     * started as a result of the event. If the type is NONE the UI of this activity will
     * be updated.
     *
     * @param event the event
     * @return the type of the activity that should be started as a result of the event
     */
    virtual ActivityType buttonPressed(ButtonEvent &event) = 0;

    virtual void update() {
        if (firstUpdate) {
            output->clear();
            firstUpdate = false;
        }
    }

    void setListener(ActivityListener *listener);

protected:
    Output *output = nullptr;
    bool firstUpdate = true;
    ActivityListener *listener = nullptr;

    void updateLine(int line, std::string text, Output::TextStyle style = Output::TextStyle::LEFT);
};

#endif //EFFECTPEDAL_ACTIVIT_H
