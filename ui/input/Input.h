#ifndef EFFECTPEDAL_INPUT_H
#define EFFECTPEDAL_INPUT_H

#include "../../controller/ButtonEvent.h"

class Input {
public:
    virtual ButtonEvent readEvent() = 0;

    virtual void close() = 0;
};

#endif //EFFECTPEDAL_INPUT_H
