#ifndef EFFECTPEDAL_WIRINGPIINPUT_H
#define EFFECTPEDAL_WIRINGPIINPUT_H

#include "Input.h"
#include <map>

class GPIOInput : public Input {
public:
    static GPIOInput &getInstance() {
        static GPIOInput instance;
        return instance;
    }

    GPIOInput(GPIOInput const &) = delete;

    void operator=(GPIOInput const &) = delete;

    ButtonEvent readEvent() override;

    void close() override;

private:
    const std::chrono::milliseconds SLEEP_INTERVAL = std::chrono::milliseconds(10);
    const std::chrono::milliseconds DOUBLE_INPUT_INTERVAL = std::chrono::milliseconds(100);

    bool running;
    std::map<unsigned int, ButtonEvent::Button> pinMap;
    std::map<unsigned int, int> amountMap;
    uint lastInput;
    std::chrono::steady_clock::time_point lastInputEnd;

    GPIOInput();
};


#endif //EFFECTPEDAL_WIRINGPIINPUT_H
