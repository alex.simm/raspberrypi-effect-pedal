#include "GPIOInput.h"
#include "../../Logger.h"
#include <thread>
#include <wiringPi.h>
#include <chrono>

using namespace std;
using namespace std::chrono;

GPIOInput::GPIOInput() {
    pinMap.insert(pair<unsigned int, ButtonEvent::Button>(2, ButtonEvent::Button::MAIN_BUTTON));
    amountMap.insert(pair<unsigned int, int>(2, 1));
    pinMap.insert(pair<unsigned int, ButtonEvent::Button>(25, ButtonEvent::Button::OK_BUTTON));
    amountMap.insert(pair<unsigned int, int>(25, 1));
    pinMap.insert(pair<unsigned int, ButtonEvent::Button>(23, ButtonEvent::Button::BACK_BUTTON));
    amountMap.insert(pair<unsigned int, int>(23, 1));
    pinMap.insert(pair<unsigned int, ButtonEvent::Button>(27, ButtonEvent::Button::VALUE_KNOB));
    amountMap.insert(pair<unsigned int, int>(27, 1));
    pinMap.insert(pair<unsigned int, ButtonEvent::Button>(24, ButtonEvent::Button::VALUE_KNOB));
    amountMap.insert(pair<unsigned int, int>(24, -1));
    pinMap.insert(pair<unsigned int, ButtonEvent::Button>(28, ButtonEvent::Button::SCROLL_KNOB));
    amountMap.insert(pair<unsigned int, int>(28, 1));
    pinMap.insert(pair<unsigned int, ButtonEvent::Button>(29, ButtonEvent::Button::SCROLL_KNOB));
    amountMap.insert(pair<unsigned int, int>(29, -1));

    wiringPiSetup();

    for (auto iter = pinMap.begin(); iter != pinMap.end(); ++iter) {
        pinMode(iter->first, INPUT);
    }

    lastInput = -1;
    lastInputEnd = steady_clock::now();

    running = true;
}

ButtonEvent GPIOInput::readEvent() {
    try {
        while (running) {
            for (auto iter = pinMap.begin();
                 iter != pinMap.end(); ++iter) {
                if (digitalRead(iter->first) == HIGH) {
                    // Logger::info("Button active: "+to_string(iter->first));

                    // active button found; wait for inactivity
                    auto begin = steady_clock::now();
                    while (digitalRead(iter->first) == HIGH) {
                        this_thread::sleep_for(SLEEP_INTERVAL);
                    }
                    auto end = steady_clock::now();

                    // prevent fast repeated events
                    if ((iter->first != lastInput) ||
                        (duration_cast<milliseconds>(end - lastInputEnd) > DOUBLE_INPUT_INTERVAL)) {
                        lastInput = iter->first;
                        lastInputEnd = end;
                        return ButtonEvent(iter->second, amountMap[iter->first],
                                           duration_cast<milliseconds>(end - begin));
                    }
                }
            }

            this_thread::sleep_for(SLEEP_INTERVAL);
        }
        return ButtonEvent(ButtonEvent::Button::NONE, 0, milliseconds(0));
    }
    catch (exception &e) {
        Logger::error("GPIOInput: error during read: " + string(e.what()));
        return ButtonEvent(ButtonEvent::Button::NONE, 0, milliseconds(0));
    }
}

void GPIOInput::close() {
    try {
        running = false;
    }
    catch (exception &e) {
        Logger::info("GPIOInput: error while closing: " + string(e.what()));
    }
}
