#include "KeyboardInput.h"
#include "../../Logger.h"
#include <iostream>
#include <chrono>

using namespace std;
using namespace std::chrono;

KeyboardInput::KeyboardInput() {
}

ButtonEvent KeyboardInput::readEvent() {
    int amount = 0;
    ButtonEvent::Button button = ButtonEvent::Button::NONE;

    try {
        while (amount == 0) {
            int key;
            cin >> key;

            switch (key) {
                case 1:
                    button = ButtonEvent::Button::MAIN_BUTTON;
                    amount = 1;
                    break;
                case 2:
                    button = ButtonEvent::Button::OK_BUTTON;
                    amount = 1;
                    break;
                case 3:
                    button = ButtonEvent::Button::BACK_BUTTON;
                    amount = 1;
                    break;
                case 4:
                    button = ButtonEvent::Button::SCROLL_KNOB;
                    amount = -1;
                    break;
                case 5:
                    button = ButtonEvent::Button::SCROLL_KNOB;
                    amount = 1;
                    break;
                case 6:
                    button = ButtonEvent::Button::VALUE_KNOB;
                    amount = -1;
                    break;
                case 7:
                    button = ButtonEvent::Button::VALUE_KNOB;
                    amount = 1;
                    break;
            }
        }

        return ButtonEvent(button, amount, milliseconds(1));
    }
    catch (exception &e) {
        Logger::error("KeyboardInput: error during read: " + string(e.what()));
        return ButtonEvent(ButtonEvent::Button::NONE, 0, milliseconds(0));
    }
}

void KeyboardInput::close() {
}