#ifndef EFFECTPEDAL_KEYBOARDINPUT_H
#define EFFECTPEDAL_KEYBOARDINPUT_H

#include "Input.h"
#include "../utils.h"

class KeyboardInput : public Input {
public:
    static KeyboardInput &getInstance() {
        static KeyboardInput instance;
        return instance;
    }

    KeyboardInput(KeyboardInput const &) = delete;

    void operator=(KeyboardInput const &) = delete;

    ButtonEvent readEvent() override;

    void close() override;

private:
    KeyboardInput();
};

#endif //EFFECTPEDAL_KEYBOARDINPUT_H
