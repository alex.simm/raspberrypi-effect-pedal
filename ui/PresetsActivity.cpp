#include "PresetsActivity.h"
#include "../Logger.h"
#include <stdexcept>

using namespace std;

PresetsActivity::PresetsActivity(Output *output, PresetManager *manager, EffectList *effectList)
        : ListActivity(output, "Presets"), manager(manager), effectList(effectList) {
    update();
}

void PresetsActivity::update() {
    vector<string> list = manager->listPresets();
    list.insert(list.begin(), DEFAULT_PRESETS_ENTRY);
    list.insert(list.begin(), SAVE_PRESETS_ENTRY);
    setEntries(list);

    ListActivity::update();
}

ActivityType PresetsActivity::buttonPressed(ButtonEvent &event) {
    ActivityType response = ActivityType::ACTIVITY_NONE;

    switch (event.button) {
        case ButtonEvent::Button::OK_BUTTON:
            if (currentIndex == 0) {
                string name = manager->storePresets(effectList->getEffects());
                listener->showMessage({"", "Preset gespeichert:", name, ""}, 2);
                update();
            } else if (currentIndex == 1)
                resetAll();
            else if (currentIndex > 1)
                load();
            event.consumed = true;
            break;
        case ButtonEvent::Button::SCROLL_KNOB:
            changeIndex(event.amount);
            event.consumed = true;
            break;
        default:
            break;
    }

    return response;
}

void PresetsActivity::load() {
    // reset all processors
    for (AudioProcessor *p : effectList->getEffects()) {
        for (Property *pr : p->getProperties()) {
            pr->reset();
        }
        p->update();
        p->setMode(AudioProcessor::Mode::DISABLED);
    }

    // load presets
    try {
        Logger::debug("Loading presets: " + string(getEntry(currentIndex)));
        manager->loadPresets(getEntry(currentIndex), effectList->getEffects());
        listener->saveLastPreset();
        listener->showMessage({"", "Preset geladen", getEntry(currentIndex), ""}, 2);
    }
    catch (exception &e) {
        listener->showMessage({"Fehler", string(e.what()), "", ""}, 3);
        Logger::error("Error while loading last presets: " + string(e.what()));
    }
}

void PresetsActivity::resetAll() {
    for (AudioProcessor *p : effectList->getEffects()) {
        for (Property *pr : p->getProperties()) {
            pr->reset();
        }
        p->update();
        p->setMode(AudioProcessor::Mode::DISABLED);
    }

    listener->saveLastPreset();
    listener->showMessage({"", "Defaults geladen", "", ""}, 2);
}