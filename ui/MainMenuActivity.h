#ifndef EFFECTPEDAL_MAINMENUACTIVITY_H
#define EFFECTPEDAL_MAINMENUACTIVITY_H

#include "ListActivity.h"

class MainMenuActivity : public ListActivity {
public:
    MainMenuActivity(Output *output);

    ~MainMenuActivity();

    ActivityType buttonPressed(ButtonEvent &event);

private:
    const std::vector<ActivityType> types = {
            ActivityType::ACTIVITY_EFFECT_LIST, ActivityType::ACTIVITY_TUNER,
            ActivityType::ACTIVITY_RECORDER, ActivityType::ACTIVITY_LOOPER,
            ActivityType::ACTIVITY_PRESETS, ActivityType::ACTIVITY_RESTART_PROGRAM
    };
};


#endif //EFFECTPEDAL_MAINMENUACTIVITY_H
