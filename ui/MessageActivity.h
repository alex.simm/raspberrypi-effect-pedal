#ifndef EFFECTPEDAL_MESSAGEACTIVITY_H
#define EFFECTPEDAL_MESSAGEACTIVITY_H

#include "Activity.h"

class MessageActivity : public Activity {
public:
    MessageActivity(Output *output, std::vector<std::string> lines);

    ~MessageActivity() override = default;

    ActivityType buttonPressed(ButtonEvent &event) override;

    void update() override;

private:
    std::vector<std::string> lines;
};


#endif //EFFECTPEDAL_MESSAGEACTIVITY_H
