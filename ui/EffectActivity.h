#ifndef EFFECTPEDAL_EFFECTACTIVITY_H
#define EFFECTPEDAL_EFFECTACTIVITY_H

#include "Activity.h"
#include "../api/AudioProcessor.h"
#include "../api/ListProperty.h"
#include "../presets/PresetManager.h"

class EffectActivity : public Activity {
public:
    EffectActivity(Output *output, AudioProcessor *effect);

    ActivityType buttonPressed(ButtonEvent &event);

    void update();

private:
    AudioProcessor *effect = nullptr;
    Property *currentProperty = nullptr;
    int currentIndex;

    std::string formatValue(Property *property) const;

    void formatListValue(Property *property, std::ostringstream &oss) const;

    void updateValue(int distance);

    void updateListValue(int distance);
};

#endif //EFFECTPEDAL_EFFECTACTIVITY_H
