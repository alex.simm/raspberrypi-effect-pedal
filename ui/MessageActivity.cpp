#include <utility>

#include "MessageActivity.h"

MessageActivity::MessageActivity(Output *output, std::vector<std::string> lines) : Activity(output),
                                                                                   lines(std::move(lines)) {
}

void MessageActivity::update() {
    Activity::update();

    for (int i = 0; i < 4; ++i) {
        updateLine(i, lines[i]);
    }
}

ActivityType MessageActivity::buttonPressed(ButtonEvent &event) {
    return ActivityType::ACTIVITY_NONE;
}