#ifndef EFFECTPEDAL_PRESETSACTIVITY_H
#define EFFECTPEDAL_PRESETSACTIVITY_H

#include "ListActivity.h"
#include "../presets/PresetManager.h"
#include "../effects/EffectList.h"

class PresetsActivity : public ListActivity {
public:
    PresetsActivity(Output *output, PresetManager *manager, EffectList *effectList);

    ~PresetsActivity() override = default;

    ActivityType buttonPressed(ButtonEvent &event) override;

    void update() override;

private:
    const std::string SAVE_PRESETS_ENTRY = "[Speichern]";
    const std::string DEFAULT_PRESETS_ENTRY = "[Defaults]";
    PresetManager *manager;
    EffectList *effectList;

    void load();

    void resetAll();
};


#endif //EFFECTPEDAL_PRESETSACTIVITY_H
