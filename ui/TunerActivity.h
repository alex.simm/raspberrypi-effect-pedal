#ifndef EFFECTPEDAL_TUNERACTIVITY_H
#define EFFECTPEDAL_TUNERACTIVITY_H

#include "Activity.h"
#include "../tuner/Tuner.h"
#include <chrono>

class TunerActivity : public Activity, public Tuner::Listener {
public:
    TunerActivity(Output *output, Tuner *tuner);

    ~TunerActivity() override;

    void update() override;

    ActivityType buttonPressed(ButtonEvent &event) override;

    /**
     * Called by the tuner if a new frequency was found.
     */
    void frequencyUpdated(float frequency, std::string note, int octave,
                          int cents, float logCents) override;

private:
    const std::chrono::milliseconds UPDATE_INTERVAL = std::chrono::milliseconds(300);

    Tuner *tuner;
    int previousCents = -100; //invalid initial value
    std::chrono::steady_clock::time_point previousUpdate;
};


#endif //EFFECTPEDAL_TUNERACTIVITY_H
