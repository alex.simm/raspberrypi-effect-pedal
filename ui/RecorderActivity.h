#ifndef EFFECTPEDAL_RECORDERACTIVITY_H
#define EFFECTPEDAL_RECORDERACTIVITY_H

#include "Activity.h"
#include "../recorder/Recorder.h"
#include "../recorder/Player.h"
#include "../recorder/RecorderList.h"

class RecorderActivity : public Activity {
public:
    RecorderActivity(Output *output, Player *player, Recorder *recorder);

    ~RecorderActivity() override;

    void update() override;

    ActivityType buttonPressed(ButtonEvent &event) override;

private:
    static constexpr std::chrono::seconds DURATION_BUTTON_LONG = std::chrono::seconds(1);
    const std::string TITLE = "Recorder";
    const std::string RECORDING = "== Aufnahme ==";
    const std::string PLAYING = "== Playback == ";
    const std::string PLAYING_RECORDING = "== Playback/Aufnahme == ";
    const std::string MODE_PLAY = "Abspielen";
    const std::string MODE_ADD = "Hinzufuegen";
    const std::string NEW_ENTRY = "[Neue Aufnahme]";

    std::vector<std::string> entries;
    Recorder *recorder = nullptr;
    Player *player = nullptr;
    RecorderList list;
    unsigned int currentIndex = 0;
    bool playRecordMode = false;

    void updateList();
};


#endif //EFFECTPEDAL_RECORDERACTIVITY_H
