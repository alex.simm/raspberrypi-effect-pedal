#include "EffectListActivity.h"

using namespace std;

EffectListActivity::EffectListActivity(Output *output, EffectList *list) : CategoriesListActivity(output, "Effekte"),
                                                                           list(list) {
    setEntryCategories(list->getCategorisedEffectNames());
}

ActivityType EffectListActivity::buttonPressed(ButtonEvent &event) {
    ActivityType type = ActivityType::ACTIVITY_NONE;

    switch (event.button) {
        case ButtonEvent::Button::OK_BUTTON:
            event.additionalInfo = getEntry(currentIndex);
            type = ActivityType::ACTIVITY_EFFECT;
            event.consumed = true;
            break;
        case ButtonEvent::Button::SCROLL_KNOB:
            changeIndex(event.amount);
            event.consumed = true;
            break;
        case ButtonEvent::Button::VALUE_KNOB:
            AudioProcessor *p = list->getEffect(getEntry(currentIndex));
            for (int i = 0; i < abs(event.amount); ++i) {
                if (event.amount > 0)
                    p->setMode(getNextMode(p->getMode(), false));
                else
                    p->setMode(getPreviousMode(p->getMode(), false));
            }
            listener->saveLastPreset();
            event.consumed = true;
            break;
    }

    return type;
}

string EffectListActivity::getSuffix(string entry, unsigned int index) {
    switch (list->getEffect(entry)->getMode()) {
        case AudioProcessor::Mode::LEVEL_ONE:
            return "1";
        case AudioProcessor::Mode::LEVEL_TWO:
            return "2";
        case AudioProcessor::Mode::BOTH:
            return "+";
        default:
            return "";
    }
}