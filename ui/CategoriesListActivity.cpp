#include "CategoriesListActivity.h"
#include <algorithm>
#include "../utils/utils.h"

using namespace std;

CategoriesListActivity::CategoriesListActivity(Output *output) : ListActivity(output) {
    setIgnoreEntry(SEPARATOR_CATEGORY);
}

CategoriesListActivity::CategoriesListActivity(Output *output, string title) : ListActivity(output) {
    setIgnoreEntry(SEPARATOR_CATEGORY);
}

void CategoriesListActivity::setEntryCategories(const vector<vector<string>> &categories) {
    setEntries(Utils::combineVectors(categories, SEPARATOR_CATEGORY));
}