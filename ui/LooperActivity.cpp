#include "LooperActivity.h"

using namespace std;

LooperActivity::LooperActivity(Output *output, Looper *looper)
        : Activity(output), looper(looper) {
}

LooperActivity::~LooperActivity() = default;

void LooperActivity::update() {
    Activity::update();

    updateLine(0, TITLE, DisplayOutput::TextStyle::CENTER);

    string streamString;
    for (int i = 0; i < looper->numStreams(); i++) {
        streamString += to_string(looper->numStreams());
    }
    updateLine(1, streamString);
    updateLine(2, "");

    if (looper != nullptr) {
        switch (looper->getState()) {
            case Looper::State::LOOPER_STATE_RECORDING:
                updateLine(3, RECORDING, DisplayOutput::TextStyle::CENTER);
                break;
            case Looper::State::LOOPER_STATE_WAITING_FOR_THRESHOLD:
                updateLine(3, WAITING, DisplayOutput::TextStyle::CENTER);
                break;
            default:
                updateLine(3, "");
                break;
        }
    } else updateLine(3, "");
}

ActivityType LooperActivity::buttonPressed(ButtonEvent &event) {
    switch (event.button) {
        case ButtonEvent::Button::BACK_BUTTON:
            listener->changeLine(LineType::LINE_MODE_NORMAL);
            break;
        case ButtonEvent::Button::MAIN_BUTTON:
            if (event.duration >= DURATION_BUTTON_LONG) {
                looper->reset();
            } else {
                if (looper->getState() == Looper::State::LOOPER_STATE_RECORDING
                    || looper->getState() == Looper::State::LOOPER_STATE_WAITING_FOR_THRESHOLD) {
                    looper->stopRecording();
                } else {
                    looper->startRecording();
                }
            }
            event.consumed = true;
            break;
    }

    return ActivityType::ACTIVITY_NONE;
}