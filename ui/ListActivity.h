#ifndef EFFECTPEDAL_LISTACTIVITY_H
#define EFFECTPEDAL_LISTACTIVITY_H

#include "Activity.h"
#include <set>

class ListActivity : public Activity {
public:
    explicit ListActivity(Output *output);

    ListActivity(Output *output, std::string title);

    virtual ~ListActivity() override = default;

    virtual void changeIndex(int distance);

    void update() override;

protected:
    const std::string SEPARATOR_END = "====================";
    unsigned int currentIndex;

    const std::vector<std::string> &getEntries() const;

    std::string getEntry(uint index) const;

    void setEntries(const std::vector<std::string> &newEntries);

    void setIgnoreEntry(std::string entry);

    void removeIgnoreEntry(std::string entry);

private:
    const bool showTitle;
    const std::string title;
    std::vector<std::string> entries;
    std::set<std::string> ignoreEntries;

    void updateLinesWithTitle();

    void updateLinesWithoutTitle();

    std::string getString(unsigned int index, std::string prefix);

    /**
     * Hook that allows subclasses to add a trailing string to the entries.
     * @param entry the entry
     * @param index the entry's index in the entries vector
     * @return a trailing string, can be emptys
     */
    virtual std::string getSuffix(std::string entry, unsigned int index);
};

#endif //EFFECTPEDAL_LISTACTIVITY_H
