#include "RecorderActivity.h"

constexpr std::chrono::seconds RecorderActivity::DURATION_BUTTON_LONG;

RecorderActivity::RecorderActivity(Output *output, Player *player, Recorder *recorder)
        : Activity(output), player(player), recorder(recorder) {
    updateList();
    player->setAddSamples(true);
}

RecorderActivity::~RecorderActivity() = default;

void RecorderActivity::updateList() {
    list.update();
    entries = list.getEntries();
    entries.insert(entries.begin(), NEW_ENTRY);
}

void RecorderActivity::update() {
    Activity::update();

    updateLine(0, TITLE, DisplayOutput::TextStyle::CENTER);

    updateLine(1, ">" + entries[currentIndex]);
    if (entries.size() > 1) updateLine(2, " " + entries[(currentIndex + 1) % entries.size()]);
    else updateLine(2, "");

    if (recorder != nullptr && recorder->isRunning()) {
        if (player != nullptr && player->isRunning())
            updateLine(3, PLAYING_RECORDING, DisplayOutput::TextStyle::CENTER);
        else updateLine(3, RECORDING, DisplayOutput::TextStyle::CENTER);
    } else if (player != nullptr && player->isRunning())
        updateLine(3, PLAYING, DisplayOutput::TextStyle::CENTER);
    else
        updateLine(3, playRecordMode ? MODE_ADD : MODE_PLAY, DisplayOutput::TextStyle::RIGHT);
}

ActivityType RecorderActivity::buttonPressed(ButtonEvent &event) {
    switch (event.button) {
        case ButtonEvent::Button::BACK_BUTTON:
            listener->changeLine(LineType::LINE_MODE_NORMAL);
            break;
        case ButtonEvent::Button::MAIN_BUTTON:
            if (recorder->isRunning()) {
                // stop recording
                listener->changeLine(LineType::LINE_MODE_NORMAL);
                if (event.duration >= DURATION_BUTTON_LONG) {
                    remove(recorder->getOutputFile());
                    listener->showMessage({"", "Aufnahme geloescht", "", ""}, 1);
                }
                updateList();
            } else if (player->isRunning()) {
                // stop playing
                listener->changeLine(LineType::LINE_MODE_NORMAL);
                updateList();
            } else {
                // start playing or recording
                if (currentIndex == 0) {
                    // record
                    recorder->setOutputFile(list.createFile());
                    listener->changeLine(LineType::LINE_MODE_RECORDER);
                } else if (playRecordMode) {
                    // play back and record
                    recorder->setOutputFile(list.createFile());
                    player->setInputFile(list.getPath(entries[currentIndex]));
                    listener->changeLine(LineType::LINE_MODE_RECORDER_PLAYBACK);
                } else {
                    // play back
                    player->setInputFile(list.getPath(entries[currentIndex]));
                    listener->changeLine(LineType::LINE_MODE_PLAYBACK);
                }
            }
            event.consumed = true;
            break;
        case ButtonEvent::Button::SCROLL_KNOB:
            if (!list.getEntries().empty() && !player->isRunning() && !recorder->isRunning())
                currentIndex = wrapIndex(currentIndex, event.amount, (unsigned int) entries.size());
            event.consumed = true;
            break;
        case ButtonEvent::Button::VALUE_KNOB:
            if (!list.getEntries().empty() && !player->isRunning() && !recorder->isRunning())
                playRecordMode = !playRecordMode;
            event.consumed = true;
            break;
        default:
            break;
    }
    return ActivityType::ACTIVITY_NONE;
}