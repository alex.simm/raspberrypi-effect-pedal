#include "MainMenuActivity.h"

using namespace std;

MainMenuActivity::MainMenuActivity(Output *output) : ListActivity(output) {
    setEntries({"Effekte", "Tuner", "Recorder", "Looper", "Presets", "Neustart"});
}

MainMenuActivity::~MainMenuActivity() {
}

ActivityType MainMenuActivity::buttonPressed(ButtonEvent &event) {
    ActivityType response = ActivityType::ACTIVITY_NONE;

    switch (event.button) {
        case ButtonEvent::Button::OK_BUTTON:
            event.consumed = true;
            response = types[currentIndex];
            break;
        case ButtonEvent::Button::SCROLL_KNOB:
            changeIndex(event.amount);
            event.consumed = true;
            break;
    }

    return response;
}