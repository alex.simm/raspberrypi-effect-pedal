#include "EffectActivity.h"
#include "../api/FloatProperty.h"
#include "../api/ListProperty.h"
#include "../api/IntegerProperty.h"
#include "../api/BooleanProperty.h"
#include "../Logger.h"
#include <algorithm>
#include <sstream>
#include <iomanip>

using namespace std;

EffectActivity::EffectActivity(Output *output, AudioProcessor *effect) : Activity(output), effect(effect) {
    if (effect->getProperties().empty()) {
        currentIndex = -1;
        currentProperty = nullptr;
    } else {
        currentIndex = 0;
        currentProperty = effect->getProperties()[0];
    }
}

ActivityType EffectActivity::buttonPressed(ButtonEvent &event) {
    ActivityType type = ActivityType::ACTIVITY_NONE;

    switch (event.button) {
        case ButtonEvent::Button::OK_BUTTON:
            if (currentProperty != nullptr) {
                Logger::debug("EffectActivity: resetting "+currentProperty->getName()+", "+formatValue(currentProperty));
                currentProperty->reset();
                Logger::debug("EffectActivity: resetting "+currentProperty->getName()+", "+formatValue(currentProperty));
                effect->update();
            }
            break;
        case ButtonEvent::Button::BACK_BUTTON:
            listener->saveLastPreset();
            type = ActivityType::ACTIVITY_EFFECT_LIST;
            break;
        case ButtonEvent::Button::SCROLL_KNOB:
            if (!effect->getProperties().empty()) {
                currentIndex = wrapIndex(currentIndex, event.amount, (unsigned int) (effect->getProperties().size()));
                currentProperty = effect->getProperties()[currentIndex];
            }
            break;
        case ButtonEvent::Button::VALUE_KNOB:
            if (currentProperty != nullptr)
                updateValue(event.amount);
            break;
    }

    return type;
}

void EffectActivity::update() {
    Activity::update();

    updateLine(0, effect->getName());
    if (currentProperty != nullptr) {
        updateLine(1, currentProperty->getName());
        updateLine(2, formatValue(currentProperty), DisplayOutput::TextStyle::CENTER);
    } else {
        updateLine(1, "[keine Einstellungen]");
        updateLine(2, "");
    }
    updateLine(3, "");
}

std::string EffectActivity::formatValue(Property *property) const {
    if (property == nullptr) return "";

    ostringstream oss;

    switch (property->type) {
        case Property::Type::CONTINUOUS: {
            auto p = dynamic_cast<FloatProperty *>(property);
            oss << setprecision(3) << p->min << " < " << p->getValue() << " < " << p->max;
            break;
        }
        case Property::Type::DISCRETE: {
            auto p = dynamic_cast<IntegerProperty *>(property);
            oss << setprecision(3) << p->min << " < " << p->getValue() << " < " << p->max;
            break;
        }
        case Property::Type::BOOLEAN: {
            auto p = dynamic_cast<BooleanProperty *>(property);
            oss << (p->getValue() ? "an" : "aus");
            break;
        }
        case Property::Type::LIST: {
            formatListValue(property, oss);
            break;
        }
    }

    return oss.str();
}

void EffectActivity::formatListValue(Property *property, std::ostringstream &oss) const {
    if (property != nullptr) {
        if (dynamic_cast<ListProperty<std::string> *>(property) != nullptr) {
            auto p = dynamic_cast<ListProperty<std::string> *>(property);
            oss << "(" << (p->getIndex() + 1) << "/" << p->getSize() << ") " << p->getValue();
        } else if (dynamic_cast<ListProperty<int> *>(property) != nullptr) {
            auto p = dynamic_cast<ListProperty<int> *>(property);
            oss << "(" << (p->getIndex() + 1) << "/" << p->getSize() << ") " << p->getValue();
        } else if (dynamic_cast<ListProperty<unsigned int> *>(property) != nullptr) {
            auto p = dynamic_cast<ListProperty<unsigned int> *>(property);
            oss << "(" << (p->getIndex() + 1) << "/" << p->getSize() << ") " << p->getValue();
        } else if (dynamic_cast<ListProperty<float> *>(property) != nullptr) {
            auto p = dynamic_cast<ListProperty<float> *>(property);
            oss << "(" << (p->getIndex() + 1) << "/" << p->getSize() << ") " << p->getValue();
        }
    }
}

void EffectActivity::updateValue(int distance) {
    if (currentProperty == nullptr) return;

    switch (currentProperty->type) {
        case Property::Type::CONTINUOUS: {
            auto p = dynamic_cast<FloatProperty *>(currentProperty);
            p->setValue(p->getValue() + distance * p->inc);
            break;
        }
        case Property::Type::DISCRETE: {
            auto p = dynamic_cast<IntegerProperty *>(currentProperty);
            p->setValue(p->getValue() + distance * p->inc);
            break;
        }
        case Property::Type::BOOLEAN: {
            auto p = dynamic_cast<BooleanProperty *>(currentProperty);
            if ((distance % 2) != 0) p->toggle();
            break;
        }
        case Property::Type::LIST: {
            updateListValue(distance);
            break;
        }
    }

    effect->update();
}

void EffectActivity::updateListValue(int distance) {
    if (dynamic_cast<ListProperty<std::string> *>(currentProperty) != nullptr) {
        auto p = dynamic_cast<ListProperty<std::string> *>(currentProperty);
        updateWrappedIndex(p, distance);
    } else if (dynamic_cast<ListProperty<int> *>(currentProperty) != nullptr) {
        auto p = dynamic_cast<ListProperty<int> *>(currentProperty);
        updateWrappedIndex(p, distance);
    } else if (dynamic_cast<ListProperty<unsigned int> *>(currentProperty) != nullptr) {
        auto p = dynamic_cast<ListProperty<unsigned int> *>(currentProperty);
        updateWrappedIndex(p, distance);
    } else if (dynamic_cast<ListProperty<float> *>(currentProperty) != nullptr) {
        auto p = dynamic_cast<ListProperty<float> *>(currentProperty);
        updateWrappedIndex(p, distance);
    }
}