#ifndef EFFECTPEDAL_LOOPERACTIVITY_H
#define EFFECTPEDAL_LOOPERACTIVITY_H

#include "Activity.h"
#include "../recorder/Looper.h"
#include <chrono>

class LooperActivity : public Activity {
public:
    LooperActivity(Output *output, Looper *looper);

    ~LooperActivity() override;

    void update() override;

    ActivityType buttonPressed(ButtonEvent &event) override;

private:
    const std::string TITLE = "Looper";
    const std::string WAITING = "=== Warte ===";
    const std::string RECORDING = "== Aufnahme ==";
    const std::chrono::seconds DURATION_BUTTON_LONG = std::chrono::seconds(1);

    Looper *looper = nullptr;
};

#endif //EFFECTPEDAL_LOOPERACTIVITY_H
