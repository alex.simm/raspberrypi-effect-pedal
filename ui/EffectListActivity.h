#ifndef EFFECTPEDAL_EFFECTLISTACTIVITY_H
#define EFFECTPEDAL_EFFECTLISTACTIVITY_H

#include "../effects/EffectList.h"
#include "../presets/PresetManager.h"
#include "CategoriesListActivity.h"

class EffectListActivity : public CategoriesListActivity {
public:
    EffectListActivity(Output *output, EffectList *list);

    ~EffectListActivity() override = default;

    ActivityType buttonPressed(ButtonEvent &event) override;

protected:
    std::string getSuffix(std::string entry, unsigned int index) override;

private:
    EffectList *list;
};


#endif //EFFECTPEDAL_EFFECTLISTACTIVITY_H
