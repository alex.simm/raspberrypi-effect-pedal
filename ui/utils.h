#ifndef EFFECTPEDAL_STRUCTS_H
#define EFFECTPEDAL_STRUCTS_H

#include <experimental/filesystem>
#include "../api/AudioProcessor.h"

enum class ActivityType {
    ACTIVITY_NONE,
    ACTIVITY_MAIN_MENU,
    ACTIVITY_EFFECT_LIST,
    ACTIVITY_EFFECT,
    ACTIVITY_TUNER,
    ACTIVITY_RECORDER,
    ACTIVITY_PRESETS,
    ACTIVITY_LOOPER,
    ACTIVITY_RESTART_PROGRAM
};

enum class LineType {
    LINE_MODE_NORMAL,
    LINE_MODE_RECORDER,
    LINE_MODE_PLAYBACK,
    LINE_MODE_RECORDER_PLAYBACK,
    LINE_MODE_LOOPER,
    LINE_MODE_TUNER
};

class ActivityListener {
public:
    virtual void showMessage(std::vector<std::string> lines, unsigned int seconds) = 0;

    virtual void changeLine(LineType mode) = 0;

    /**
     * Save the current settings in the presets.
     */
    virtual void saveLastPreset() = 0;
};

unsigned int wrapIndex(unsigned int index, int distance, unsigned int size);

template<class P>
void updateWrappedIndex(P *property, int distance) {
    property->setIndex(wrapIndex(property->getIndex(), distance, property->getSize()));
}

AudioProcessor::Mode getPreviousMode(AudioProcessor::Mode mode, bool cyclic);

AudioProcessor::Mode getNextMode(AudioProcessor::Mode mode, bool cyclic);

#endif //EFFECTPEDAL_STRUCTS_H
