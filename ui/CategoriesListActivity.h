#ifndef EFFECTPEDAL_CATEGORIESLISTACTIVITY_H
#define EFFECTPEDAL_CATEGORIESLISTACTIVITY_H

#include "Activity.h"
#include "ListActivity.h"

class CategoriesListActivity : public ListActivity {
public:
    explicit CategoriesListActivity(Output *output);

    CategoriesListActivity(Output *output, std::string title);

    virtual ~CategoriesListActivity() override = default;

    void setEntryCategories(const std::vector<std::vector<std::string>> &categories);

private:
    const std::string SEPARATOR_CATEGORY = "--------------------";
};

#endif //EFFECTPEDAL_LISTACTIVITY_H
