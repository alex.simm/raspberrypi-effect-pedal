#include "utils.h"

using namespace std;

unsigned int wrapIndex(unsigned int index, int distance, unsigned int size) {
    int i = index + distance;
    while (i < 0) {
        i += size;
    }
    return (i % size);
}

AudioProcessor::Mode getPreviousMode(AudioProcessor::Mode mode, bool cyclic) {
    AudioProcessor::Mode newMode;
    switch (mode) {
        case AudioProcessor::Mode::DISABLED:
            newMode = cyclic ? AudioProcessor::Mode::BOTH : mode;
            break;
        case AudioProcessor::Mode::LEVEL_ONE:
            newMode = AudioProcessor::Mode::DISABLED;
            break;
        case AudioProcessor::Mode::LEVEL_TWO:
            newMode = AudioProcessor::Mode::LEVEL_ONE;
            break;
        default: //BOTH
            newMode = AudioProcessor::Mode::LEVEL_TWO;
            break;
    }

    return newMode;
}

AudioProcessor::Mode getNextMode(AudioProcessor::Mode mode, bool cyclic) {
    AudioProcessor::Mode newMode;
    switch (mode) {
        case AudioProcessor::Mode::DISABLED:
            newMode = AudioProcessor::Mode::LEVEL_ONE;
            break;
        case AudioProcessor::Mode::LEVEL_ONE:
            newMode = AudioProcessor::Mode::LEVEL_TWO;
            break;
        case AudioProcessor::Mode::LEVEL_TWO:
            newMode = AudioProcessor::Mode::BOTH;
            break;
        default: //BOTH
            newMode = cyclic ? AudioProcessor::Mode::DISABLED : mode;
            break;
    }

    return newMode;
}