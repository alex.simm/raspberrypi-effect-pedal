#include "ListActivity.h"
#include <algorithm>

using namespace std;

ListActivity::ListActivity(Output *output) : Activity(output), showTitle(false), title(""), currentIndex(0) {
    ignoreEntries.insert(SEPARATOR_END);
}

ListActivity::ListActivity(Output *output, std::string title) : Activity(output), showTitle(true), title(title),
                                                                currentIndex(0) {
    ignoreEntries.insert(SEPARATOR_END);
}

void ListActivity::update() {
    Activity::update();

    if (showTitle)
        return updateLinesWithTitle();
    else
        return updateLinesWithoutTitle();
}

const std::vector<std::string> &ListActivity::getEntries() const {
    return entries;
}

std::string ListActivity::getEntry(uint index) const {
    return entries[index];
}

void ListActivity::setEntries(const std::vector<std::string> &newEntries) {
    entries = newEntries;
    if (!entries.empty())
        entries.push_back(SEPARATOR_END);
}

void ListActivity::setIgnoreEntry(std::string entry) {
    ignoreEntries.insert(entry);
}

void ListActivity::removeIgnoreEntry(std::string entry) {
    ignoreEntries.erase(entry);
}

void ListActivity::updateLinesWithTitle() {
    updateLine(0, title, DisplayOutput::TextStyle::CENTER);

    switch (entries.size()) {
        case 0:
            break;
        case 1:
            updateLine(1, getString(currentIndex, ">"));
            updateLine(2, "");
            updateLine(3, "");
            break;
        case 2:
            updateLine(1, getString(currentIndex, ">"));
            updateLine(2, getString((unsigned int) ((currentIndex + 1) % entries.size()), " "));
            updateLine(3, "");
            break;
        default:
            updateLine(1, getString((unsigned int) ((currentIndex + entries.size() - 1) % entries.size()), " "));
            updateLine(2, getString(currentIndex, ">"));
            updateLine(3, getString((unsigned int) ((currentIndex + 1) % entries.size()), " "));
            break;
    }
}

void ListActivity::updateLinesWithoutTitle() {
    switch (entries.size()) {
        case 0:
            break;
        case 1:
            updateLine(0, getString(currentIndex, ">"));
            updateLine(1, "");
            updateLine(2, "");
            updateLine(3, "");
            break;
        case 2:
            updateLine(0, getString(currentIndex, ">"));
            updateLine(1, getString((unsigned int) ((currentIndex + 1) % entries.size()), "  "));
            updateLine(2, "");
            updateLine(3, "");
            break;
        case 3:
            updateLine(0, getString((unsigned int) ((currentIndex + entries.size() - 1) % entries.size()), " "));
            updateLine(1, getString(currentIndex, ">"));
            updateLine(2, getString((unsigned int) ((currentIndex + 1) % entries.size()), " "));
            updateLine(3, "");
            break;
        default:
            updateLine(0, getString((unsigned int) ((currentIndex + entries.size() - 1) % entries.size()), " "));
            updateLine(1, getString(currentIndex, ">"));
            updateLine(2, getString((unsigned int) ((currentIndex + 1) % entries.size()), " "));
            updateLine(3, getString((unsigned int) ((currentIndex + 2) % entries.size()), " "));
            break;
    }
}

void ListActivity::changeIndex(int distance) {
    currentIndex = wrapIndex(currentIndex, distance, (unsigned int) entries.size());

    // skip all ignored entries
    while (find(ignoreEntries.begin(), ignoreEntries.end(), entries[currentIndex]) != ignoreEntries.end()) {
        currentIndex = wrapIndex(currentIndex, distance, (unsigned int) entries.size());
    }
}

string ListActivity::getString(unsigned int index, string prefix) {
    string entry = entries[index];
    if (find(ignoreEntries.begin(), ignoreEntries.end(), entry) != ignoreEntries.end())
        return entry;

    string suffix = getSuffix(entry, index);
    string line = prefix + entry;
    auto maxLen = (unsigned int) (20 - suffix.size());
    if (line.size() >= maxLen) line = line.substr(0, maxLen - 1);
    else {
        while (line.size() < maxLen - 1) {
            line += " ";
        }
    }
    return line + " " + suffix;
}

string ListActivity::getSuffix(string entry, unsigned int index) {
    return "";
}