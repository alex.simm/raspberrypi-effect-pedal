#ifndef EFFECTPEDAL_CONSOLEOUTPUT_H
#define EFFECTPEDAL_CONSOLEOUTPUT_H

#include "Output.h"
#include <iostream>

class ConsoleOutput : public Output {
public:
    static ConsoleOutput &getInstance() {
        std::cout << "ConsoleOutput" << std::endl;
        static ConsoleOutput instance;
        return instance;
    }

    ConsoleOutput(ConsoleOutput const &) = delete;

    void operator=(ConsoleOutput const &) = delete;

    void clear() override;

    void close() override;

protected:
    void writeLineImpl(int line, std::string text) override;

private:
    ConsoleOutput();
};


#endif //EFFECTPEDAL_CONSOLEOUTPUT_H
