#include "DisplayOutput.h"
#include "../../Logger.h"
#include <stdexcept>
#include <cstdio>
#include <string>
#include <iostream>
#include <unistd.h>

using namespace std;

DisplayOutput::DisplayOutput() : Output() {
    pipe = new std::unique_ptr<FILE, decltype(&pclose)>(popen(CMD.c_str(), "w"), pclose);
    if (!pipe)
        throw std::runtime_error("DisplayOutput: failed to open pipe");

    for (int i = 0; i < 4; ++i) {
        writeLine(i, "", TextStyle::LEFT);
    }
}

void DisplayOutput::writeLineImpl(int line, std::string text) {
    try {
        fputc(line + 1, pipe->get());
        fputs(text.c_str(), pipe->get());
        fflush(pipe->get());
    }
    catch (exception &e) {
        Logger::error("DisplayOutput: error when sending text: " + string(e.what()));
    }
}

void DisplayOutput::clear() {
    try {
        fputc(0, pipe->get());
        fflush(pipe->get());
    }
    catch (exception &e) {
        Logger::error("DisplayOutput: error during clear: " + string(e.what()));
    }
}

void DisplayOutput::close() {
	Logger::debug("DisplayOutput: sending close signal");
    try {
        fputc(5, pipe->get());
        fflush(pipe->get());
    }
    catch (exception &e) {
        Logger::error("DisplayOutput: error during flush: " + string(e.what()));
    }

	sleep(1);

	Logger::debug("DisplayOutput: closing pipe");
    try {
        pclose(pipe->get());
    }
    catch (exception &e) {
        Logger::info("DisplayOutput: error while closing: " + string(e.what()));
    }
}
