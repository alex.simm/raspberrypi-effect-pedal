#include "Output.h"

Output::Output() : currentLines(4) {
    for (int i = 0; i < 4; ++i) {
        currentLines[i] = "";
    }
}

void Output::writeLine(int line, std::string text, Output::TextStyle style) {
    if (line < 0 || line >= ROWS) return;
    if (text.size() > COLUMNS) text = text.substr(0, COLUMNS);

    switch (style) {
        case TextStyle::RIGHT:
            while (text.size() < 20) {
                text = " " + text;
            }
            break;
        case TextStyle::CENTER:
            while (text.size() < 20) {
                text = " " + text;
                if (text.size() < 20) text += " ";
            }
            break;
        default: //LEFT
            while (text.size() < 20) {
                text += " ";
            }
            break;
    }

    if (currentLines[line] != text) {
        currentLines[line] = text;
        writeLineImpl(line, text);
    }
}