#ifndef EFFECTPEDAL_LEDSTATUS_H
#define EFFECTPEDAL_LEDSTATUS_H

#include "../../api/AudioProcessor.h"

class LEDStatus {

public:
    static LEDStatus &getInstance() {
        static LEDStatus instance;
        return instance;
    }

    LEDStatus(LEDStatus const &) = delete;

    void operator=(LEDStatus const &) = delete;

    void displayProcessorMode(AudioProcessor::Mode mode);

private:
    static const unsigned int PIN_LED1 = 1;
    static const unsigned int PIN_LED2 = 0;

    LEDStatus();
};

#endif //EFFECTPEDAL_LEDSTATUS_H
