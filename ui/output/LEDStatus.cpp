#include "LEDStatus.h"
#include <wiringPi.h>

LEDStatus::LEDStatus() {
    wiringPiSetup();

    pinMode(PIN_LED1, OUTPUT);
    pinMode(PIN_LED2, OUTPUT);
}

void LEDStatus::displayProcessorMode(AudioProcessor::Mode mode) {
    int led1 = LOW;
    int led2 = LOW;

    switch (mode) {
        case AudioProcessor::Mode::LEVEL_ONE:
            led1 = HIGH;
            break;
        case AudioProcessor::Mode::LEVEL_TWO:
            led2 = HIGH;
            break;
        case AudioProcessor::Mode::BOTH:
            led1 = HIGH;
            led2 = HIGH;
            break;
        case AudioProcessor::Mode::DISABLED:
        default:
            break;
    }

    digitalWrite(PIN_LED1, led1);
    digitalWrite(PIN_LED2, led2);
}
