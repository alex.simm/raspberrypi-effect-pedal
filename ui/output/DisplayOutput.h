#ifndef EFFECTPEDAL_DISPLAYOUTPUT_H
#define EFFECTPEDAL_DISPLAYOUTPUT_H

#include <string>
#include <memory>
#include <vector>
#include "../../GlobalConfig.h"
#include "Output.h"

class DisplayOutput : public Output {
public:
    static DisplayOutput &getInstance() {
        static DisplayOutput instance;
        return instance;
    }

    DisplayOutput(DisplayOutput const &) = delete;

    void operator=(DisplayOutput const &) = delete;

    void clear() override;

    void close() override;

protected:
    void writeLineImpl(int line, std::string text) override;

private:
    const std::string CMD = "python3 -u " + GlobalConfig::getInstance().getMainDirectory() + "/lcd.py";

    std::unique_ptr<FILE, decltype(&pclose)> *pipe;

    DisplayOutput();
};

#endif //EFFECTPEDAL_DISPLAYOUTPUT_H
