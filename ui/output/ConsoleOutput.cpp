#include "ConsoleOutput.h"
#include <iostream>

using namespace std;

ConsoleOutput::ConsoleOutput() : Output() {
}

void ConsoleOutput::writeLineImpl(int line, std::string text) {
    system("clear");
    for (int i = 0; i < 4; ++i) {
        cout << currentLines[i] << endl;
    }
}

void ConsoleOutput::clear() {
    system("clear");
}

void ConsoleOutput::close() {
}