#ifndef EFFECTPEDAL_OUTPUT_H
#define EFFECTPEDAL_OUTPUT_H

#include <string>
#include <vector>
#include "../../api/AudioProcessor.h"

class Output {
public:
    enum class TextStyle {
        LEFT, CENTER, RIGHT
    };

    Output();

    void writeLine(int line, std::string text, TextStyle style);

    virtual void clear() = 0;

    virtual void close() = 0;

protected:
    static const unsigned int COLUMNS = 20;
    static const unsigned int ROWS = 4;

    std::vector<std::string> currentLines;

    virtual void writeLineImpl(int line, std::string text) = 0;
};

#endif //EFFECTPEDAL_OUTPUT_H
