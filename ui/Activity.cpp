#include "Activity.h"

Activity::Activity(Output *output) : output(output) {
}

Activity::~Activity() {
    //display.close();
}

void Activity::setListener(ActivityListener *listener) {
    this->listener = listener;
}

void Activity::updateLine(int line, std::string text, DisplayOutput::TextStyle style) {
    output->writeLine(line, text, style);
}