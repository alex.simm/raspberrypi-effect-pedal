#include "TunerActivity.h"
#include <cmath>
#include <sstream>
#include <iomanip>

using namespace std;
using namespace std::chrono;

TunerActivity::TunerActivity(Output *output, Tuner *tuner) : Activity(output), tuner(tuner) {
    tuner->setListener(this);
    previousUpdate = steady_clock::now();
}

TunerActivity::~TunerActivity() {
    tuner->setListener(nullptr);
}

void TunerActivity::update() {
    Activity::update();

    updateLine(0, (tuner->isRunning()) ? "Tuner" : "Tuner (aus)");
    updateLine(1, "");
    updateLine(2, "");
    updateLine(3, "");
}

ActivityType TunerActivity::buttonPressed(ButtonEvent &event) {
    ActivityType response = ActivityType::ACTIVITY_NONE;

    switch (event.button) {
        case ButtonEvent::Button::BACK_BUTTON:
            if (tuner->isRunning()) tuner->stop();
            listener->changeLine(LineType::LINE_MODE_NORMAL);
            break;
        case ButtonEvent::Button::OK_BUTTON:
            tuner->fast.toggle();
            tuner->update();
            event.consumed = true;
            break;
        case ButtonEvent::Button::MAIN_BUTTON:
            if (tuner->isRunning()) {
                listener->changeLine(LineType::LINE_MODE_NORMAL);
                tuner->stop();
            } else {
                tuner->start();
                listener->changeLine(LineType::LINE_MODE_TUNER);
            }
            event.consumed = true;
            break;
        default:
            break;
    }

    return response;
}

void TunerActivity::frequencyUpdated(float frequency, std::string note, int octave,
                                     int cents, float logCents) {
    auto time = steady_clock::now();

    if (cents != previousCents && duration_cast<milliseconds>(time - previousUpdate) > UPDATE_INTERVAL) {
        previousUpdate = time;
        previousCents = cents;
        int abs = std::abs(cents);

        string mid = note;
        if (mid.size() == 1) mid += " ";
        mid += to_string(octave);

        string s;
        if (abs < 5) s = "! " + mid + " !";
        else if (cents > 0) {
            if (abs < 10) s = "  " + mid + " <";
            else if (abs < 20) s = "   " + mid + " <<";
            else s = "    " + mid + " <<<";
        } else {
            if (abs < 10) s = "> " + mid + "  ";
            else if (abs < 20) s = ">> " + mid + "   ";
            else s = ">>> " + mid + "    ";
        }
        updateLine(1, s, DisplayOutput::TextStyle::CENTER);

        std::ostringstream oss;
        oss << (cents > 0 ? '+' : '-') << setw(2) << setprecision(2) << abs;
        updateLine(2, oss.str(), DisplayOutput::TextStyle::CENTER);
    }
}