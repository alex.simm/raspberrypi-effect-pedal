#include "Logger.h"
#include "GlobalConfig.h"
#include <ctime>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <experimental/filesystem>

using namespace std;
using namespace std::experimental::filesystem;

Logger::Logger() : Consumer(-1), threshold(Logger::Level::INFO) {
    path p(GlobalConfig::getInstance().getMainDirectory() + "/logfile.log");
    if (exists(p) && file_size(p) > MAX_FILE_SIZE)
        remove(p);

    fileOut.open(p, ios_base::app);
}

Logger::Level Logger::getLevel() const {
    return threshold;
}

void Logger::setLevel(Level level) {
    threshold = level;
}

void Logger::logImpl(Level level, std::string message) {
    if (level >= threshold) {
        time_t t = time(nullptr);
        tm tm = *localtime(&t);

        ostringstream out;
        out << '[' << put_time(&tm, "%d.%m.%Y %H:%M:%S")
            << ", " << LEVEL_NAMES[(unsigned int) level]
            << "] " << message;
        push(new string(out.str()));
    }
}

void Logger::consume(std::string *s) {
    fileOut << *s << endl;
    cout << *s << endl;
}

void Logger::log(Logger::Level level, std::string message) {
    getInstance().logImpl(level, message);
}

void Logger::debug(std::string message) {
    getInstance().logImpl(Level::DEBUG, message);
}

void Logger::info(std::string message) {
    getInstance().logImpl(Level::INFO, message);
}

void Logger::warning(std::string message) {
    getInstance().logImpl(Level::WARNING, message);
}

void Logger::error(std::string message) {
    getInstance().logImpl(Level::ERROR, message);
}

void Logger::fatal(std::string message) {
    getInstance().logImpl(Level::FATAL, message);
}