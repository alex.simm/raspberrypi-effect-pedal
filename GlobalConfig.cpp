#include "GlobalConfig.h"

GlobalConfig::GlobalConfig() {
    levelCalculationMode = LevelCalculationMode::CONSTANT;
}

std::string GlobalConfig::getMainDirectory() const {
    return MAIN_DIRECTORY;
}

bool GlobalConfig::isUsingLCDDisplay() const {
    return USE_LCD_DISPLAY;
}

bool GlobalConfig::isUsingButtonInput() const {
    return USE_BUTTON_INPUT;
}

GlobalConfig::LevelCalculationMode GlobalConfig::getLevelCalculationMode() const {
    return levelCalculationMode;
}

void GlobalConfig::setLevelCalculationMode(GlobalConfig::LevelCalculationMode mode) {
    levelCalculationMode = mode;
}

float GlobalConfig::getMeanInputLevel() const {
    return meanInputLevel;
}

void GlobalConfig::setMeanInputLevel(float level) {
    meanInputLevel = level;
}

float GlobalConfig::getMaxInputLevel() const {
    return maxInputLevel;
}

void GlobalConfig::setMaxInputLevel(float level) {
    maxInputLevel = level;
}

std::string GlobalConfig::getRecordingFormat() {
    return RECORDING_FORMAT;
}