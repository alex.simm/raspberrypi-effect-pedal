#ifndef EFFECTPEDAL_PRESETMANAGER_H
#define EFFECTPEDAL_PRESETMANAGER_H

#include <string>
#include <vector>
#include <experimental/filesystem>
#include <tinyxml.h>
#include "../api/AudioProcessor.h"

class PresetManager {

public:
    PresetManager();

    std::vector<std::string> listPresets();

    void loadPresets(const std::string &name, const std::vector<AudioProcessor *> &processors);

    void loadLastPresets(const std::vector<AudioProcessor *> &processors);

    /**
     * Saves the processor settings into a new file with a unique name and returns the name.
     */
    std::string storePresets(const std::vector<AudioProcessor *> &processors);

    /**
     * Saves the processor settings as the last settings that will be restored after a restart.
     */
    void storeLastPresets(const std::vector<AudioProcessor *> &processors);

private:
    const std::string DIRECTORY = "/presets";
    const std::string ELEMENT_ROOT = "presets";
    const std::string ELEMENT_PROCESSOR = "processor";
    const std::string ELEMENT_PROPERTY = "property";
    const std::string PROPERTY_TYPE_CONTINUOUS = "continuous";
    const std::string PROPERTY_TYPE_DISCRETE = "discrete";
    const std::string PROPERTY_TYPE_BOOL = "bool";
    const std::string PROPERTY_TYPE_LIST = "list";
    const std::string FILE_NAME_LAST = "last";

    std::experimental::filesystem::path mainPath;

    AudioProcessor *findProcessor(const std::vector<AudioProcessor *> &processors, const std::string &name);

    Property *findProperty(AudioProcessor *processor, const std::string &name);

    TiXmlElement *readProperty(Property *property);

    void updateProperty(Property *property, const std::string &type, const std::string &value);

    void storePresetsImpl(const std::string path, const std::vector<AudioProcessor *> &processors);

    AudioProcessor::Mode indexToMode(unsigned int index);

    unsigned int modeToIndex(AudioProcessor::Mode mode);

    std::experimental::filesystem::path findUniqueFileName(std::experimental::filesystem::path directory,
                                                           std::string fileExtension);
};


#endif //EFFECTPEDAL_PRESETMANAGER_H
