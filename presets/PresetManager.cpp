#include "PresetManager.h"
#include <algorithm>
#include <iomanip>
#include <ctime>
#include <tinyxml.h>
#include "../api/IntegerProperty.h"
#include "../api/BooleanProperty.h"
#include "../api/FloatProperty.h"
#include "../api/ListProperty.h"
#include "../utils/utils.h"
#include "../GlobalConfig.h"
#include "../Logger.h"

using namespace std;
using namespace std::experimental::filesystem;

PresetManager::PresetManager() : mainPath(GlobalConfig::getInstance().getMainDirectory() + DIRECTORY) {
    if (!exists(mainPath))
        create_directories(mainPath);
}

vector<string> PresetManager::listPresets() {
    vector<string> vec;

    directory_iterator start(mainPath);
    directory_iterator end;
    transform(start, end, back_inserter(vec),
              [](directory_entry entry) -> string {
                  string name = entry.path().filename();
                  auto end = name.find_last_of('.');
                  if (end != string::npos) name = name.substr(0, end);
                  return name;
              });

    // remove 'last'
    auto pos = find(vec.begin(), vec.end(), FILE_NAME_LAST);
    if (pos != vec.end()) vec.erase(pos);

    sort(vec.begin(), vec.end());

    return vec;
}

void PresetManager::loadLastPresets(const std::vector<AudioProcessor *> &processors) {
    if (exists(path(mainPath.u8string() + "/" + FILE_NAME_LAST + ".xml")))
        loadPresets(FILE_NAME_LAST, processors);
}

void PresetManager::loadPresets(const string &name, const vector<AudioProcessor *> &processors) {
    TiXmlDocument doc(mainPath.u8string() + "/" + name + ".xml");
    if (!doc.LoadFile())
        throw runtime_error("Failed to load preset");

    TiXmlElement *root = doc.RootElement();
    if (ELEMENT_ROOT != root->Value())
        throw runtime_error("Failed to load preset: invalid XML format: root=" + string(root->Value()));

    // iterate effects
    for (TiXmlElement *e = root->FirstChildElement(ELEMENT_PROCESSOR);
         e != nullptr; e = e->NextSiblingElement(ELEMENT_PROCESSOR)) {
        AudioProcessor *proc = findProcessor(processors, e->Attribute("name"));
        if (proc != nullptr) {

            const char *enabledValue = e->Attribute("mode");
            if (enabledValue != nullptr)
                proc->setMode(indexToMode((unsigned int) atoi(enabledValue)));
            else
                proc->setMode(AudioProcessor::Mode::DISABLED);

            //iterate properties
            for (TiXmlElement *e2 = e->FirstChildElement(ELEMENT_PROPERTY);
                 e2 != nullptr; e2 = e2->NextSiblingElement(ELEMENT_PROPERTY)) {
                const string type = e2->Attribute("type");

                Property *prop = findProperty(proc, e2->Attribute("name"));
                if (prop != nullptr)
                    updateProperty(prop, type, e2->Attribute("value"));
                else
                    Logger::warning("PresetManager: property '" + string(e2->Attribute("name"))
                                    + "' not found in processor '" + proc->getName() + "' (preset='" + name +
                                    "')");
            }

            proc->update();
        } else
            Logger::warning("PresetManager: when loading '" + name + "': processor '"
                            + string(e->Attribute("name")) + "' not found!");
    }
}

AudioProcessor::Mode PresetManager::indexToMode(unsigned int index) {
    switch (index) {
        case 1:
            return AudioProcessor::Mode::LEVEL_ONE;
        case 2:
            return AudioProcessor::Mode::LEVEL_TWO;
        case 3:
            return AudioProcessor::Mode::BOTH;
        default:
            return AudioProcessor::Mode::DISABLED;
    }
}

unsigned int PresetManager::modeToIndex(AudioProcessor::Mode mode) {
    switch (mode) {
        case AudioProcessor::Mode::LEVEL_ONE:
            return 1;
        case AudioProcessor::Mode::LEVEL_TWO:
            return 2;
        case AudioProcessor::Mode::BOTH:
            return 3;
        default:
            return 0;
    }
}

string PresetManager::storePresets(const vector<AudioProcessor *> &processors) {
    path p = findUniqueFileName(mainPath, "xml");
    storePresetsImpl(p.u8string(), processors);

    string name = p.filename();
    auto end = name.find_last_of('.');
    if (end != string::npos) name = name.substr(0, end);
    return name;
}

void PresetManager::storeLastPresets(const std::vector<AudioProcessor *> &processors) {
    storePresetsImpl(mainPath.u8string() + "/" + FILE_NAME_LAST + ".xml", processors);
}

void PresetManager::storePresetsImpl(const std::string path, const std::vector<AudioProcessor *> &processors) {
    TiXmlDocument doc;
    doc.LinkEndChild(new TiXmlDeclaration("1.0", "", ""));
    auto *root = new TiXmlElement(ELEMENT_ROOT);
    doc.LinkEndChild(root);

    for (AudioProcessor *p : processors) {
        if (!p->isExcludedFromPresets()) {
            auto *procElem = new TiXmlElement(ELEMENT_PROCESSOR);
            procElem->SetAttribute("name", p->getName());
            procElem->SetAttribute("mode", modeToIndex(p->getMode()));
            root->LinkEndChild(procElem);

            for (Property *prop : p->getProperties()) {
                auto propElem = readProperty(prop);
                if (readProperty(prop)) {
                    propElem->SetAttribute("name", prop->getName());
                    procElem->LinkEndChild(propElem);
                }
            }
        }
    }

    doc.SaveFile(path);
}

AudioProcessor *PresetManager::findProcessor(const std::vector<AudioProcessor *> &processors,
                                             const std::string &name) {
    for (AudioProcessor *p : processors) {
        if (p->getName() == name)
            return p;
    }

    return nullptr;
}

Property *PresetManager::findProperty(AudioProcessor *processor, const std::string &name) {
    for (Property *p: processor->getProperties()) {
        if (p->getName() == name)
            return p;
    }

    return nullptr;
}

void PresetManager::updateProperty(Property *property, const std::string &type, const std::string &value) {
    if (PROPERTY_TYPE_CONTINUOUS == type) {
        auto p = dynamic_cast<FloatProperty *>(property);
        if (p != nullptr) {
            p->setValue(stof(value));
        }
    } else if (PROPERTY_TYPE_DISCRETE == type) {
        auto p = dynamic_cast<IntegerProperty *>(property);
        if (p != nullptr) {
            p->setValue(stoi(value));
        }
    } else if (PROPERTY_TYPE_BOOL == type) {
        auto p = dynamic_cast<BooleanProperty *>(property);
        if (p != nullptr) {
            p->setValue(stoi(value) != 0);
        }
    } else if (PROPERTY_TYPE_LIST == type) {
        if (dynamic_cast<ListProperty<std::string> *>(property) != nullptr) {
            (dynamic_cast<ListProperty<std::string> *>(property))->setIndex((unsigned int) stoi(value));
        } else if (dynamic_cast<ListProperty<int> *>(property) != nullptr) {
            (dynamic_cast<ListProperty<int> *>(property))->setIndex((unsigned int) stoi(value));
        } else if (dynamic_cast<ListProperty<float> *>(property) != nullptr) {
            (dynamic_cast<ListProperty<float> *>(property))->setIndex((unsigned int) stoi(value));
        }
    }
}

TiXmlElement *PresetManager::readProperty(Property *property) {
    TiXmlElement *element = nullptr;

    if (dynamic_cast<FloatProperty *>(property) != nullptr) {
        auto *p = dynamic_cast<FloatProperty *>(property);

        element = new TiXmlElement(ELEMENT_PROPERTY);
        element->SetAttribute("type", PROPERTY_TYPE_CONTINUOUS);
        element->SetAttribute("value", to_string(p->getValue()));
    } else if (dynamic_cast<IntegerProperty *>(property) != nullptr) {
        auto *p = dynamic_cast<IntegerProperty *>(property);

        element = new TiXmlElement(ELEMENT_PROPERTY);
        element->SetAttribute("type", PROPERTY_TYPE_DISCRETE);
        element->SetAttribute("value", to_string(p->getValue()));
    } else if (dynamic_cast<BooleanProperty *>(property) != nullptr) {
        auto *p = dynamic_cast<BooleanProperty *>(property);

        element = new TiXmlElement(ELEMENT_PROPERTY);
        element->SetAttribute("type", PROPERTY_TYPE_BOOL);
        element->SetAttribute("value", to_string(p->getValue() ? 1 : 0));
    } else if (dynamic_cast<ListProperty<std::string> *>(property) != nullptr) {
        auto *p = dynamic_cast<ListProperty<std::string> *>(property);

        element = new TiXmlElement(ELEMENT_PROPERTY);
        element->SetAttribute("type", PROPERTY_TYPE_LIST);
        element->SetAttribute("value", to_string(p->getIndex()));
    } else if (dynamic_cast<ListProperty<int> *>(property) != nullptr) {
        auto *p = dynamic_cast<ListProperty<int> *>(property);

        element = new TiXmlElement(ELEMENT_PROPERTY);
        element->SetAttribute("type", PROPERTY_TYPE_LIST);
        element->SetAttribute("value", to_string(p->getIndex()));
    } else if (dynamic_cast<ListProperty<float> *>(property) != nullptr) {
        auto *p = dynamic_cast<ListProperty<float> *>(property);

        element = new TiXmlElement(ELEMENT_PROPERTY);
        element->SetAttribute("type", PROPERTY_TYPE_LIST);
        element->SetAttribute("value", to_string(p->getIndex()));
    }

    return element;
}

path PresetManager::findUniqueFileName(path directory, std::string fileExtension) {
    const int max = std::numeric_limits<int>::max();
    for (int i = 1; i < max; ++i) {
        path p(directory.u8string() + "/" + to_string(i) + "." + fileExtension);
        if (!exists(p))
            return p;
    }

    // should never happen
    return path(directory.u8string() + "/x." + fileExtension);
}