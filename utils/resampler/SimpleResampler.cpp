#include <cassert>
#include "SimpleResampler.h"

#define MAX_UPSAMPLE 8

SimpleResampler::SimpleResampler() : upResampler(), downResampler(), factor() {
}

void SimpleResampler::setup(int sampleRate, unsigned int factor) {
    assert(factor <= MAX_UPSAMPLE);
    this->factor = factor;

    // upsampler
    upResampler.setup(sampleRate, sampleRate * factor, 1, QUALITY);
    // k == inpsize() == 2 * qual
    // pre-fill with k-1 zeros
    upResampler.inp_count = upResampler.inpsize() - 1;
    upResampler.out_count = 1;
    upResampler.inp_data = upResampler.out_data = 0;
    upResampler.process();
    // downsampler
    downResampler.setup(sampleRate * factor, sampleRate, 1, QUALITY);
    // k == inpsize() == 2 * qual * fact
    // pre-fill with k-1 zeros
    downResampler.inp_count = downResampler.inpsize() - 1;
    downResampler.out_count = 1;
    downResampler.inp_data = downResampler.out_data = 0;
    downResampler.process();
}

void SimpleResampler::up(int numSamples, float *input, float *output) {
    upResampler.inp_count = numSamples;
    upResampler.inp_data = input;
    upResampler.out_count = numSamples * factor;
    upResampler.out_data = output;
    upResampler.process();
    assert(upResampler.inp_count == 0);
    assert(upResampler.out_count == 0);
}

void SimpleResampler::down(int numSamples, float *input, float *output) {
    downResampler.inp_count = numSamples * factor;
    downResampler.inp_data = input;
    downResampler.out_count = numSamples + 1; // +1 == trick to drain input
    downResampler.out_data = output;
    downResampler.process();
    assert(downResampler.inp_count == 0);
    assert(downResampler.out_count == 1);
}