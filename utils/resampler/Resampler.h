#ifndef EFFECTPEDAL_RESAMPLER2_H
#define EFFECTPEDAL_RESAMPLER2_H

#include "Resampler_table.h"

class Resampler {
public:
    Resampler(void);

    ~Resampler(void);

    int setup(unsigned int fs_inp,
              unsigned int fs_out,
              unsigned int nchan,
              unsigned int hlen);

    int setup(unsigned int fs_inp,
              unsigned int fs_out,
              unsigned int nchan,
              unsigned int hlen,
              double frel);

    void clear(void);

    int reset(void);

    int nchan(void) const { return _nchan; }

    int filtlen(void) const { return inpsize(); } // Deprecated
    int inpsize(void) const;

    double inpdist(void) const;

    int process(void);

    unsigned int inp_count;
    unsigned int out_count;
    float *inp_data;
    float *out_data;
    void *inp_list;
    void *out_list;

private:
    Resampler_table *_table;
    unsigned int _nchan;
    unsigned int _inmax;
    unsigned int _index;
    unsigned int _nread;
    unsigned int _nzero;
    unsigned int _phase;
    unsigned int _pstep;
    float *_buff;
    void *_dummy[8];
};

#endif //EFFECTPEDAL_RESAMPLER2_H
