#include "FixedRateResampler.h"
#include <cmath>
#include <cstring>
#include <cassert>

int FixedRateResampler::max_out_count(int in_count) {
    if (inputRate > outputRate) return in_count;
    return static_cast<int>(ceil((in_count * static_cast<double>(outputRate)) / inputRate));
}

int FixedRateResampler::setup(int inputRate, int outputRate) {
    this->inputRate = inputRate;
    this->outputRate = outputRate;

    if (inputRate >= outputRate)
        return 0;

    // upsampler
    int ret = upResampler.setup(inputRate, outputRate, 1, QUALITY);
    if (ret)
        return ret;

    // k == filtlen() == 2 * qual
    // pre-fill with k-1 zeros
    upResampler.inp_count = upResampler.filtlen() - 1;
    upResampler.out_count = 1;
    upResampler.inp_data = upResampler.out_data = 0;
    upResampler.process();
    // downsampler
    ret = downResampler.setup(outputRate, inputRate, 1, QUALITY);
    if (ret)
        return ret;

    // k == filtlen() == 2 * qual * fact
    // pre-fill with k-2 zeros
    downResampler.inp_count = downResampler.filtlen() - 2;
    downResampler.out_count = 1;
    downResampler.inp_data = downResampler.out_data = 0;
    downResampler.process();
    return 0;
}

int FixedRateResampler::up(int count, float *input, float *output) {
    if (inputRate >= outputRate) {
        memcpy(output, input, count * sizeof(float));
        downResampler.out_count = count;
        return count;
    }
    upResampler.inp_count = count;
    downResampler.out_count = count + 1; // +1 == trick to drain input
    upResampler.inp_data = input;
    int m = max_out_count(count);
    upResampler.out_count = m;
    upResampler.out_data = output;
    upResampler.process();
    assert(upResampler.inp_count == 0);
    assert(upResampler.out_count <= 1);
    downResampler.inp_count = m - upResampler.out_count;
    return downResampler.inp_count;
}

void FixedRateResampler::down(float *input, float *output) {
    if (inputRate >= outputRate) {
        memcpy(output, input, downResampler.out_count * sizeof(float));
        return;
    }
    downResampler.inp_data = input;
    downResampler.out_data = output;
    downResampler.process();
    assert(downResampler.inp_count == 0);
    assert(downResampler.out_count == 1);
}