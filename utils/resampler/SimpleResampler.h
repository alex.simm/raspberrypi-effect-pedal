#ifndef EFFECTPEDAL_SIMPLERESAMPLER_H
#define EFFECTPEDAL_SIMPLERESAMPLER_H

#include "Resampler.h"

class SimpleResampler {
public:
    SimpleResampler();

    void setup(int sampleRate, unsigned int factor);

    void up(int numSamples, float *input, float *output);

    void down(int numSamples, float *input, float *output);

private:
    const int QUALITY = 16; //6..96; resulting in a total delay of 2*QUALITY (0.7ms @44100)

    Resampler upResampler, downResampler;
    int factor;
};

#endif //EFFECTPEDAL_SIMPLERESAMPLER_H
