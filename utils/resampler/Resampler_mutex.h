#ifndef EFFECTPEDAL_RESAMPLER_MUTEX_H
#define EFFECTPEDAL_RESAMPLER_MUTEX_H

#include <thread>

class Resampler_mutex {
private:
    friend class Resampler_table;

    Resampler_mutex(void) { pthread_mutex_init(&_mutex, 0); }

    ~Resampler_mutex(void) { pthread_mutex_destroy(&_mutex); }

    void lock(void) { pthread_mutex_lock(&_mutex); }

    void unlock(void) { pthread_mutex_unlock(&_mutex); }

    pthread_mutex_t _mutex;
};

#endif //EFFECTPEDAL_RESAMPLER_MUTEX_H
