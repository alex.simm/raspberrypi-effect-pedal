#ifndef EFFECTPEDAL_FIXEDRATERESAMPLER_H
#define EFFECTPEDAL_FIXEDRATERESAMPLER_H

#include "Resampler.h"

class FixedRateResampler {
public:
    int setup(int inputRate, int outputRate);

    int up(int count, float *input, float *output);

    void down(float *input, float *output);

    int max_out_count(int in_count);

private:
    const int QUALITY = 16; //6..96; resulting in a total delay of 2*QUALITY (0.7ms @44100)

    Resampler upResampler, downResampler;
    int inputRate, outputRate;
};

#endif //EFFECTPEDAL_FIXEDRATERESAMPLER_H
