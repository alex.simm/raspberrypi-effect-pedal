#ifndef EFFECTPEDAL_RESAMPLER_TABLE_H
#define EFFECTPEDAL_RESAMPLER_TABLE_H

#include "Resampler_mutex.h"

class Resampler_table {
    friend class Resampler;

private:
    Resampler_table(double fr, unsigned int hl, unsigned int np);

    ~Resampler_table(void);

    friend class Resampler;

    friend class VResampler;

    Resampler_table *_next;
    unsigned int _refc;
    float *_ctab;
    double _fr;
    unsigned int _hl;
    unsigned int _np;

    double sinc(double x);

    double wind(double x);

    static Resampler_table *create(double fr, unsigned int hl, unsigned int np);

    static void destroy(Resampler_table *T);

    static Resampler_table *_list;
    static Resampler_mutex _mutex;
};

#endif //EFFECTPEDAL_RESAMPLER_TABLE_H
