#ifndef EFFECTPEDAL_RINGBUFFER_H
#define EFFECTPEDAL_RINGBUFFER_H

#include <cstring>
#include <cassert>
#include "../types.h"

/**
 * Ring buffer that allows storing, reading, and peaking. This class is not thread-safe.
 */
class RingBuffer {
public:
    RingBuffer();

    ~RingBuffer();

    /**
     * Sets the internal buffer to size n and the read and write pointers to 0.
     * @param n the new size
     */
    void init(unsigned int n);

    /**
     * Overwrites the internal buffer with 0.
     */
    void clear();

    /**
     * Returns the value that was written i steps ago, i.e. at the position i behind the current write pointer. Does
     * not change the pointers.
     */
    inline sample past(int i) {
        return data[(write - i) & sizeMask];
    }

    /**
     * Inserts a value at the current write position and increases the position.
     */
    inline void put(sample x) {
        data[write] = x;
        write = (write + 1) & sizeMask;
    }

    /**
     * Reads one value from the current read position and increases the position.
     */
    inline sample get() {
        sample x = data[read];
        read = (read + 1) & sizeMask;
        return x;
    }

    /**
     * Reads one value from the current read position without increasing the position.
     */
    inline sample peek() {
        return data[read];
    }

    /*inline float getLinear(float f) {
        int n = (int) f;
        f -= n;

        return (1 - f) * past(n) + f * past(n + 1);
    }

    inline float getCubic(float f) {
        int n = (int) f;
        f -= n;

        float x_1 = past(n - 1);
        float x0 = past(n);
        float x1 = past(n + 1);
        float x2 = past(n + 2);

        float a = (3 * (x0 - x1) - x_1 + x2) * .5;
        float b = 2 * x1 + x_1 - (5 * x0 + x2) * .5;
        float c = (x1 - x_1) * .5;

        return x0 + (((a * f) + b) * f + c) * f;
    }*/

private:
    unsigned int size; //size of data buffer
    unsigned int read, write; // current read and write positions
    unsigned int sizeMask; // mask used for modulo operations
    sample *data; // data buffer
};

#endif //EFFECTPEDAL_RINGBUFFER_H
