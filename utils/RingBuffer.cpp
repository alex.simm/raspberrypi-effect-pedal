#include "RingBuffer.h"

RingBuffer::RingBuffer() {
    read = 0;
    write = 0;
    data = nullptr;
    size = 0;
    sizeMask = 0;
}

RingBuffer::~RingBuffer() {
    delete data;
}

void RingBuffer::init(unsigned int n) {
    assert(size <= (1u << 20u));

    size = n;
    sizeMask = size - 1;
    read = 0;
    write = 0;

    delete data;
    data = new sample[size];
}

void RingBuffer::clear() {
    memset(data, 0, size * sizeof(sample));
}