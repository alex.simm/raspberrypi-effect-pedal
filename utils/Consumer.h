#ifndef EFFECTPEDAL_CONSUMER_H
#define EFFECTPEDAL_CONSUMER_H

#include "Runnable.h"
#include "BlockingQueue.h"

/**
 * A thread that waits for elements to consume. New elements can be pushed and the consume function needs to be
 * overwritten.
 */
template<class T>
class Consumer : public Runnable {
public:
    void push(T *element) {
        queue.push(element);
    }

protected:
    /**
     * Max size = -1 for unlimited.
     */
    Consumer(int maxSize) : queue(maxSize) {
    }

    void run() override {
        while (running) {
            T *element = queue.pop();
            if (element != nullptr) {
                consume(element);
                delete element;
            }
        }

        // flush
        while (!queue.empty()) {
            T *element = queue.pop();
            if (element != nullptr) {
                consume(element);
                delete element;
            }
        }

        closeImpl();
    }

    void stopImpl() {
        queue.push(nullptr);
        queue.wakeUp();
    }

    virtual void consume(T *element) = 0;

    /**
     * Called when the consumer thread finished.
     */
    virtual void closeImpl() {
    }

private:
    BlockingQueue<T *> queue;
};


#endif //EFFECTPEDAL_CONSUMER_H
