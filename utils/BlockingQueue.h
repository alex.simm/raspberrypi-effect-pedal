#ifndef EFFECTPEDAL_PRODUCERCONSUMERQUEUE_H
#define EFFECTPEDAL_PRODUCERCONSUMERQUEUE_H

#include <cstdlib>
#include <atomic>
#include <stdexcept>
#include <cassert>
#include <mutex>
#include <condition_variable>
#include <deque>

template<typename T>
class BlockingQueue {
public:
    /**
     * Max size = -1 for unlimited. Null element is returned by pop if it was woken up without by wakeUp without a new
     * element in the queue.
     */
    BlockingQueue(int maxSize) : maxSize(maxSize) {
    }

    void push(T const &value) {
        {
            std::unique_lock<std::mutex> lock(queueMutex);
            condition.wait(lock, [=] { return maxSize < 0 || queue.size() < maxSize; });
            queue.push_front(value);
        }
        condition.notify_one();
    }

    T pop() {
        {
            std::unique_lock<std::mutex> lock(queueMutex);
            condition.wait(lock, [=] { return !queue.empty(); });
            T rc(std::move(queue.back()));
            queue.pop_back();
            return rc;
        }
    }

    bool empty() {
        std::unique_lock<std::mutex> lock(queueMutex);
        return queue.empty();
    }

    void wakeUp() {
        {
            std::unique_lock<std::mutex> lock(queueMutex);
            condition.notify_one();
        }
    }

private:
    int maxSize;
    std::mutex queueMutex;
    std::condition_variable condition;
    std::deque<T> queue;
};

#endif //EFFECTPEDAL_PRODUCERCONSUMERQUEUE_H
