#ifndef EFFECTPEDAL_RUNNABLE_H
#define EFFECTPEDAL_RUNNABLE_H

#include <thread>

/**
 * A thread that can be started and stopped.
 */
class Runnable {
public:
    Runnable() {
    }

    virtual ~Runnable() {
        stop();
    }

    void start() {
        if (!running && thread == nullptr) {
            running = true;
            thread = new std::thread(staticRun, this);
        }
    }

    void stop() {
        if (running && thread != nullptr) {
            running = false;
            stopImpl();
            thread->join();
            delete thread;
            thread = nullptr;
        }
    }

    bool isRunning() const {
        return running;
    }

protected:
    bool running = false;

    virtual void stopImpl() {
    }

    virtual void run() = 0;

private:
    std::thread *thread = nullptr;

    static void staticRun(Runnable *r) {
        r->run();
    }
};


#endif //EFFECTPEDAL_RUNNABLE_H
