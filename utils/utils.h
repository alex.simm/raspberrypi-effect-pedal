#ifndef EFFECTPEDAL_UTILS_H
#define EFFECTPEDAL_UTILS_H

#include "../types.h"
#include <experimental/filesystem>
#include <string>
#include <cmath>
#include <algorithm>

class Utils {
public:
    /**
 * Converts time in seconds between beats to beats per minute.
 */
    static inline float secondsToBpm(float seconds) {
        return 60.0 / seconds;
    }

    /**
 * Converts beats per minute to the time in seconds between beats.
 */
    static inline float bpmToSeconds(float bpm) {
        return 60.0f / bpm;
    }

    /**
 * Copies multiple arrays into one.
 */
    template<class T>
    static std::vector<T> combineVectors(const std::vector<std::vector<T>> &vectors) {
        std::vector<T> combined;

        for (const std::vector<T> v : vectors) {
            copy(v.begin(), v.end(), back_inserter(combined));
        }

        return combined;
    }

    /**
 * Copies multiple arrays into one with an additional separator in between.
 */
    template<class T>
    static std::vector<T> combineVectors(const std::vector<std::vector<T>> &vectors, T separator) {
        std::vector<T> combined;

        for (uint i = 0; i < vectors.size(); ++i) {
            copy(vectors[i].begin(), vectors[i].end(), back_inserter(combined));

            if (!vectors[i].empty() && i < vectors.size() - 1)
                combined.push_back(separator);
        }

        return combined;
    }

    /**
     * Applies a given lambda function to all elements of the vector and returns a new vector with the results.
     */
    template<class T1, class T2, typename F>
    static std::vector<T1> applyTo(const std::vector<T2> &src, F func) {
        std::vector<T1> dst(src.size());
        transform(src.cbegin(), src.cend(), dst.begin(), func);
        return dst;
    }

    /**
     * Rounds up the integer to the next power of 2.
     */
    static uint nextPowerOf2(uint n);

    /**
     * Converts decibel to linear gain factor.
     */
    inline static double db2lin(double db) {
        return pow(2, DB2LIN_FACTOR * db);
    }

    /**
     * Converts linear gain factor to decibel.
     */
    inline static double lin2db(double lin) {
        return LIN2DB_FACTOR * log2(lin);
    }

private:
    constexpr static double DB2LIN_FACTOR = 0.16609640474436811739351597;
    constexpr static double LIN2DB_FACTOR = 6.020599913;
};

#endif //EFFECTPEDAL_UTILS_H
