#ifndef EFFECTPEDAL_LEVELDETECTOR_H
#define EFFECTPEDAL_LEVELDETECTOR_H

#include <cmath>
#include "SlewRateLimiter.h"
#include "../utils.h"

class LevelDetector {
public:
    LevelDetector();

    /** Sets the sample-rate. */
    void setSampleRate(float newSampleRate);

    inline void process(const sample *samples, uint numSamples) {
        meanLevel = 0;
        maxLevel = 0;

        sample tmp;
        for (int i = 0; i < numSamples; ++i) {
            tmp = fabsf(samples[i]);
            storedSample = tmp + coeff * (storedSample - tmp);
            tmp = attackReleaseFollower.applySample(storedSample);

            meanLevel += tmp;
            if (tmp > maxLevel)
                maxLevel = tmp;
        }

        meanLevel /= numSamples;
    }

    float getMeanLevel() const;

    float getMaxLevel() const;

    /** Resets the internal state. */
    void reset();

private:
    SlewRateLimiter attackReleaseFollower;
    float meanLevel, maxLevel;

    float coeff;     // filter coefficient
    float storedSample; // previous output sample
};

#endif //EFFECTPEDAL_LEVELDETECTOR_H
