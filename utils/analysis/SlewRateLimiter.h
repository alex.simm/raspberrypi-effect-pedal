#ifndef EFFECTPEDAL_SLEWRATELIMITER_H
#define EFFECTPEDAL_SLEWRATELIMITER_H

#include "../../types.h"

/**
 * Slew rate limiter with user adjustable attack and release time constants. It smoothes the incoming signal
 * via an attack-release (AR) averager. The averaging time is determined by the attack and release time constants
 * which are set up in milliseconds.
 */
class SlewRateLimiter {
public:
    SlewRateLimiter();

    /** Sets the sample-rate for this envelope detector. */
    void setSampleRate(float newSampleRate);

    /** Smoothes the input value vith the AR-averager. */
    inline sample applySample(sample sample) {
        if (tmpSample < sample)
            tmpSample = sample + coeffAttack * (tmpSample - sample);
        else
            tmpSample = sample + coeffRelease * (tmpSample - sample);
        return tmpSample;
    }

    void reset();

private:
    float coeffAttack, coeffRelease;   // attack and release filter coefficients
    sample tmpSample;                          // previous output sample
    float sampleRate;                  // the samplerate
    float attackTime, releaseTime;     // in milliseconds

    float calculateCoefficient(float time);
};


#endif //EFFECTPEDAL_SLEWRATELIMITER_H
