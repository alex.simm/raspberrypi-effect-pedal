#include <algorithm>
#include "LevelDetector.h"

LevelDetector::LevelDetector() {
    reset();
}

void LevelDetector::setSampleRate(float newSampleRate) {
    coeff = expf(-1.0f / (newSampleRate * 0.01f));
    attackReleaseFollower.setSampleRate(newSampleRate);
}

void LevelDetector::reset() {
    storedSample = 0;
    attackReleaseFollower.reset();
}

float LevelDetector::getMeanLevel() const {
    return meanLevel;
}

float LevelDetector::getMaxLevel() const {
    return maxLevel;
}