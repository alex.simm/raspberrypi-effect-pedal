#include "SlewRateLimiter.h"
#include <cmath>

SlewRateLimiter::SlewRateLimiter() {
    attackTime = 10.0f;
    releaseTime = 100.0f;
    tmpSample = 0.0;

    coeffAttack = calculateCoefficient(attackTime);
    coeffRelease = calculateCoefficient(releaseTime);
}

void SlewRateLimiter::setSampleRate(float newSampleRate) {
    if (newSampleRate > 0.0) {
        sampleRate = newSampleRate;
        coeffAttack = calculateCoefficient(attackTime);
        coeffRelease = calculateCoefficient(releaseTime);
    }
}

void SlewRateLimiter::reset() {
    tmpSample = 0.0;
}

float SlewRateLimiter::calculateCoefficient(float time) {
    float tau = 0.001f * time;
    if (tau > 0.0f)
        return expf(-1.0f / (sampleRate * tau));
    else
        return 0.0f;
}