#ifndef EFFECTPEDAL_BLACKMANWINDOW_H
#define EFFECTPEDAL_BLACKMANWINDOW_H

#include "Window.h"
#include <cmath>

class BlackmanWindow : public Window {
public:
    BlackmanWindow() = default;

    inline void apply(sample *src, sample *dst, unsigned int len) override {
        sample in = 1.0f / len;

        sample f, b;
        for (uint i = 0; i < len; ++i) {
            f = (sample) i;
            b = 0.42f - 0.5f * cos(2.0f * f * M_PI * in) + 0.08 * cos(4.0f * f * M_PI * in);
            dst[i] = src[i] * b;
        }
    }
};


#endif //EFFECTPEDAL_BLACKMANWINDOW_H
