#include "KaiserWindow.h"

KaiserWindow::KaiserWindow(sample beta, sample step) : Window(), beta(beta), step(step) {
}

sample KaiserWindow::besseli(sample x) {
    sample a = fabs(x);
    if (a < 3.75) {
        sample y = x / 3.75;
        y *= y;
        return 1.0 + y * (3.5156229 + y * (3.0899424 + y * (1.2067492 +
                                                            y * (.2659732 + y * (.0360768 + y * .0045813)))));
    }
    sample y = 3.75 / a;
    return (exp(a) / sqrt(a))
           * (.39894228 + y * (.01328592 + y * (.00225319 + y * (-.00157565 + y * (.00916281 + y * (-.02057706 +
                                                                                                    y *
                                                                                                    (.02635537 +
                                                                                                     y *
                                                                                                     (-.01647633 +
                                                                                                      y *
                                                                                                      .00392377))))))));
}