#ifndef EFFECTPEDAL_KAISERWINDOW_H
#define EFFECTPEDAL_KAISERWINDOW_H

#include "Window.h"
#include <cmath>

class KaiserWindow : public Window {
public:
    KaiserWindow(sample beta, sample step = 1);

    void apply(sample *src, sample *dst, unsigned int len) override {
        sample bb = besseli(beta);
        sample i = -len / 2.0 + 0.5;

        sample a, k;
        for (uint si = 0; si < len; ++si) {
            a = 1 - pow((2 * i / (len - 1)), 2);
            k = besseli((beta * (a < 0 ? 0 : sqrtf(a))) / bb);
            dst[si] = src[si] * k;

            i += step;
        }
    }

private:
    float beta;
    float step;

    static float besseli(float x);
};


#endif //EFFECTPEDAL_KAISERWINDOW_H
