#ifndef EFFECTPEDAL_HAMMINGWINDOW_H
#define EFFECTPEDAL_HAMMINGWINDOW_H

#include "Window.h"
#include <cmath>

class HammingWindow : public Window {
public:
    HammingWindow() = default;

    inline void apply(sample *src, sample *dst, unsigned int len) override {
        auto factor = (sample) (2 * M_PI / len);
        for (uint i = 0; i < len; ++i) {
            dst[i] = src[i] * (FACTOR1 - FACTOR2 * cosf(factor * i));
        }
    }

private:
    static constexpr sample FACTOR1 = 0.54f;
    static constexpr sample FACTOR2 = 0.46f;
};

#endif //EFFECTPEDAL_HAMMINGWINDOW_H
