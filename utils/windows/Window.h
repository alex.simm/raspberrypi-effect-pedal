#ifndef EFFECTPEDAL_WINDOW_H
#define EFFECTPEDAL_WINDOW_H

#include "../../types.h"

class Window {
public:
    /**
     * Applies window on src and saves in dst.
     */
    virtual void apply(sample *src, sample *dst, unsigned int len) = 0;
};

#endif //EFFECTPEDAL_WINDOW_H
