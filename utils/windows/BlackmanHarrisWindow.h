#ifndef EFFECTPEDAL_BLACKMANHARRISWINDOW_H
#define EFFECTPEDAL_BLACKMANHARRISWINDOW_H

#include <cmath>
#include "Window.h"

class BlackmanHarrisWindow : public Window {
public:
    BlackmanHarrisWindow() = default;

    void apply(sample *src, sample *dst, unsigned int len) override {
        sample w1 = 2.f * M_PI / (len - 1);
        sample w2 = 2.f * w1;
        sample w3 = 3.f * w1;

        sample f, bh;
        for (int i = 0; i < len; ++i) {
            f = (sample) i;
            bh = FACTOR1 - FACTOR2 * cos(w1 * f) + FACTOR3 * cos(w2 * f) - FACTOR4 * cos(w3 * f);
            dst[i] = src[i] * bh;
        }
    }

private:
    static constexpr float FACTOR1 = 0.35875f;
    static constexpr float FACTOR2 = 0.48829f;
    static constexpr float FACTOR3 = 0.14128f;
    static constexpr float FACTOR4 = 0.01168f;
};

#endif //EFFECTPEDAL_BLACKMANHARRISWINDOW_H
