#ifndef EFFECTPEDAL_HANNWINDOW_H
#define EFFECTPEDAL_HANNWINDOW_H

#include "Window.h"
#include <cmath>

class HannWindow : public Window {
public:
    HannWindow(sample initialStep);

    void apply(sample *src, sample *dst, unsigned int len) override {
        step = M_PI * step / len;

        sample f, x;
        for (uint i = 0; i < len; ++i) {
            f = i * step;
            x = sinf(f);
            dst[i] = src[i] * x * x;
        }
    }

private:
    sample step;
};

#endif //EFFECTPEDAL_HANNWINDOW_H
