#include "utils.h"
#include <cassert>

using namespace std;

uint Utils::nextPowerOf2(uint n) {
    assert (n <= 0x40000000);

    --n;
    n |= n >> 1u;
    n |= n >> 2u;
    n |= n >> 4u;
    n |= n >> 8u;
    n |= n >> 16u;

    return ++n;
}