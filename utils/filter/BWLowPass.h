#ifndef EFFECTPEDAL_BWLOWPASS_H
#define EFFECTPEDAL_BWLOWPASS_H

#include "../../api/Packet.h"
#include "Filter.h"

class BWLowPass : public Filter {
public:
    BWLowPass(unsigned int order, float sampleRate, float cutoffFrequency);

    ~BWLowPass();

    void updateParams(float sampleRate, float cutoffFrequency);

    sample apply(sample x) override;

private:
    int n;
    sample *A = nullptr;
    sample *d1 = nullptr;
    sample *d2 = nullptr;
    sample *w0 = nullptr;
    sample *w1 = nullptr;
    sample *w2 = nullptr;
};

#endif //EFFECTPEDAL_BWLOWPASS_H
