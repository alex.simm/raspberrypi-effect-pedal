#include "IIRLowPass.h"
#include <cmath>

IIRLowPass::IIRLowPass(double d) {
    setParam(d);
    reset();
}

sample IIRLowPass::apply(sample x) {
    tmpSample = a * x + b * tmpSample;
    return tmpSample;
}

void IIRLowPass::reset() {
    tmpSample = 0;
}

void IIRLowPass::setParam(float d) {
    a = d;
    b = 1 - d;
}

void IIRLowPass::setParamByFrequency(float frequency) {
    setParam(1 - expf(-2 * M_PI * frequency));
}