#include "IIRHighPassB.h"
#include <cmath>

IIRHighPassB::IIRHighPassB(double d) {
    setParam(d);
    reset();
}

sample IIRHighPassB::apply(sample x) {
    y1 = a * (y1 + x - x1);
    x1 = x;
    return y1;
}

void IIRHighPassB::reset() {
    x1 = 0;
    y1 = 0;
}

void IIRHighPassB::setParam(float d) {
    a = d;
}

void IIRHighPassB::setParamByFrequency(float frequency) {
    setParam(expf(-2 * M_PI * frequency));
}