#ifndef EFFECTPEDAL_FILTER_H
#define EFFECTPEDAL_FILTER_H

#include "../../types.h"

class Filter {
public:
    virtual sample apply(sample x) = 0;

    virtual void applyAll(sample *samples, uint len);
};

#endif //EFFECTPEDAL_FILTER_H
