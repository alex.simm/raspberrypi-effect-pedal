#ifndef EFFECTPEDAL_IIRHIGHPASSB_H
#define EFFECTPEDAL_IIRHIGHPASSB_H

#include "Filter.h"

class IIRHighPassB : public Filter {
public:
    explicit IIRHighPassB(double d = 1.0);

    sample apply(sample x) override;

    void reset();

    void setParam(float d);

    void setParamByFrequency(float frequency);

private:
    sample a, x1, y1;
};


#endif //EFFECTPEDAL_IIRHIGHPASSB_H
