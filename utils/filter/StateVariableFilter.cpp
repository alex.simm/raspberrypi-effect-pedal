#include "StateVariableFilter.h"
#include <cmath>

StateVariableFilter::StateVariableFilter(uint oversample, Mode mode) : oversample(oversample) {
    setParams(.1, .1);
    setMode(mode);
}

StateVariableFilter::StateVariableFilter(const StateVariableFilter &s) {
    this->operator=(s);
}

StateVariableFilter &StateVariableFilter::operator=(const StateVariableFilter &a) {
    if (this != &a) {
        oversample = a.oversample;
        setMode(a.mode);
    }
    return *this;
}

StateVariableFilter::Mode StateVariableFilter::getMode() {
    return mode;
}

void StateVariableFilter::setMode(Mode mode) {
    this->mode = mode;
    switch (mode) {
        case LOW:
            out = &lo;
            break;
        case BAND:
            out = &band;
            break;
        default:
            out = &hi;
            break;
    }
}

sample StateVariableFilter::apply(sample x) {
    x *= qnorm;

    for (int pass = 0; pass < oversample; ++pass) {
        hi = x - lo - q * band;
        band += frequency * hi;
        lo += frequency * band;

        // zero padding
        x = 0;
    }

    return *out;
}

void StateVariableFilter::reset() {
    hi = band = lo = 0;
}

void StateVariableFilter::setParams(float cutoffFrequency, float Q) {
    // this is a very tight limit
    frequency = fmin(.25, 2 * sin(M_PI * cutoffFrequency / oversample));

    q = 2 * cos(pow(Q, .1) * M_PI * .5);
    q = fmin(q, fmin(2., 2 / frequency - frequency * .5));
    qnorm = sqrt(fabs(q) / 2. + .001);
}