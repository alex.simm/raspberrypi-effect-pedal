#ifndef EFFECTPEDAL_IIRHIGHPASSA_H
#define EFFECTPEDAL_IIRHIGHPASSA_H

#include "Filter.h"

class IIRHighPassA : public Filter {
public:
    explicit IIRHighPassA(double d = 1.0);

    sample apply(sample x) override;

    void reset();

    void setParam(float d);

    void setParamByFrequency(float frequency);

private:
    sample a0, a1, b1, x1, y1;
};


#endif //EFFECTPEDAL_IIRHIGHPASSA_H
