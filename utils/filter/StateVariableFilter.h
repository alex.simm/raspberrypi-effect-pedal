#ifndef EFFECTPEDAL_STATEVARIABLEFILTER_H
#define EFFECTPEDAL_STATEVARIABLEFILTER_H

#include "Filter.h"

/**
 * State-variable filter in Chamberlin topology. Double samples, 2 Pole Low, High, Band, Notch and Peaking.
 */
class StateVariableFilter : public Filter {
public:
    enum Mode {
        LOW, BAND, HIGH
    };

    StateVariableFilter(uint oversample = 1, Mode mode = LOW);

    StateVariableFilter(const StateVariableFilter &s);

    StateVariableFilter &operator=(const StateVariableFilter &a);

    Mode getMode();

    void setMode(Mode mode);

    void reset();

    void setParams(float cutoffFrequency, float Q);

    sample apply(sample x) override;

private:
    Mode mode;
    uint oversample;

    // loop parameters
    float frequency, q, qnorm;

    // outputs
    sample lo, band, hi;
    sample *out;
};

#endif //EFFECTPEDAL_STATEVARIABLEFILTER_H
