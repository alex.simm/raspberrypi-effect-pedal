#ifndef EFFECTPEDAL_BWBANDSTOP_H
#define EFFECTPEDAL_BWBANDSTOP_H

#include "../../api/Packet.h"
#include "Filter.h"

class BWBandStop : public Filter {
public:
    BWBandStop(uint order, float sampleRate, float frequency1, float frequency2);

    ~BWBandStop();

    sample apply(sample x) override;

private:
    int n;
    sample r, s;
    sample *A = nullptr;
    sample *d1 = nullptr;
    sample *d2 = nullptr;
    sample *d3 = nullptr;
    sample *d4 = nullptr;
    sample *w0 = nullptr;
    sample *w1 = nullptr;
    sample *w2 = nullptr;
    sample *w3 = nullptr;
    sample *w4 = nullptr;
};

#endif //EFFECTPEDAL_BWBANDSTOP_H
