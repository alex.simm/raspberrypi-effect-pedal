#ifndef EFFECTPEDAL_IIRLOWPASS_H
#define EFFECTPEDAL_IIRLOWPASS_H

#include "Filter.h"

class IIRLowPass : public Filter {
public:
    explicit IIRLowPass(double d = 1.0);

    sample apply(sample x) override;

    void reset();

    void setParam(float d);

    void setParamByFrequency(float frequency);

private:
    sample a, b, tmpSample;
};


#endif //EFFECTPEDAL_IIRLOWPASS_H
