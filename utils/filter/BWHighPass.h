#ifndef EFFECTPEDAL_BWHIGHPASS_H
#define EFFECTPEDAL_BWHIGHPASS_H

#include "../../api/Packet.h"
#include "Filter.h"

class BWHighPass : public Filter {
public:
    BWHighPass(unsigned int order, float sampleRate, float halfPowerFrequency);

    ~BWHighPass();

    sample apply(sample x) override;

private:
    int n;
    sample *A = nullptr;
    sample *d1 = nullptr;
    sample *d2 = nullptr;
    sample *w0 = nullptr;
    sample *w1 = nullptr;
    sample *w2 = nullptr;
};

#endif //EFFECTPEDAL_BWHIGHPASS_H
