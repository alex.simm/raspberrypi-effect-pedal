#ifndef EFFECTPEDAL_BWBANDPASS_H
#define EFFECTPEDAL_BWBANDPASS_H

#include "../../api/Packet.h"
#include "Filter.h"

class BWBandPass : public Filter {
public:
    BWBandPass(uint order, float sampleRate, float frequency1, float frequency2);

    ~BWBandPass();

    sample apply(sample x) override;

private:
    int n;
    sample *A = nullptr;
    sample *d1 = nullptr;
    sample *d2 = nullptr;
    sample *d3 = nullptr;
    sample *d4 = nullptr;
    sample *w0 = nullptr;
    sample *w1 = nullptr;
    sample *w2 = nullptr;
    sample *w3 = nullptr;
    sample *w4 = nullptr;
};

#endif //EFFECTPEDAL_BWBANDPASS_H
