#include "Filter.h"

void Filter::applyAll(sample *samples, uint len) {
    for (uint i = 0; i < len; ++i) {
        samples[i] = apply(samples[i]);
    }
}