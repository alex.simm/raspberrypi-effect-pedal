#include "BWBandStop.h"
#include <cmath>
#include <cassert>
#include <cstring>

BWBandStop::BWBandStop(uint order, float sampleRate, float frequency1, float frequency2) {
    assert(order % 4 == 0);
    n = order / 4;
    A = new sample[n];
    d1 = new sample[n];
    d2 = new sample[n];
    d3 = new sample[n];
    d4 = new sample[n];

    w0 = new sample[n];
    memset(w0, 0, n * sizeof(sample));
    w1 = new sample[n];
    memset(w1, 0, n * sizeof(sample));
    w2 = new sample[n];
    memset(w2, 0, n * sizeof(sample));
    w3 = new sample[n];
    memset(w3, 0, n * sizeof(sample));
    w4 = new sample[n];
    memset(w4, 0, n * sizeof(sample));

    const double factor = M_PI / (4.0 * n);
    double a = cos(M_PI * (frequency1 + frequency2) / sampleRate) / cos(M_PI * (frequency1 - frequency2) / sampleRate);
    double a2 = a * a;
    double b = tan(M_PI * (frequency1 - frequency2) / sampleRate);
    double b2 = b * b;
    double tmp1, tmp2;

    for (uint i = 0; i < n; ++i) {
        tmp1 = sin(factor * (2.0 * i + 1.0));
        tmp2 = b2 + 2.0 * b * tmp1 + 1.0;
        A[i] = 1.0 / tmp2;
        d1[i] = 4.0 * a * (1.0 + b * tmp1) / tmp2;
        d2[i] = 2.0 * (b2 - 2.0 * a2 - 1.0) / tmp2;
        d3[i] = 4.0 * a * (1.0 - b * tmp1) / tmp2;
        d4[i] = -(b2 - 2.0 * b * tmp1 + 1.0) / tmp2;
    }

    r = 4.0 * a;
    s = 4.0 * a2 + 2.0;
}

BWBandStop::~BWBandStop() {
    delete A;
    A = nullptr;
    delete d1;
    d1 = nullptr;
    delete d2;
    d2 = nullptr;
    delete w0;
    w0 = nullptr;
    delete w1;
    w1 = nullptr;
    delete w2;
    w2 = nullptr;
    delete w3;
    w3 = nullptr;
    delete w4;
    w4 = nullptr;
}

sample BWBandStop::apply(sample x) {
    for (uint i = 0; i < n; ++i) {
        w0[i] = d1[i] * w1[i] + d2[i] * w2[i] + d3[i] * w3[i] + d4[i] * w4[i] + x;
        x = A[i] * (w0[i] - r * w1[i] + s * w2[i] - r * w3[i] + w4[i]);
        w4[i] = w3[i];
        w3[i] = w2[i];
        w2[i] = w1[i];
        w1[i] = w0[i];
    }
    return x;
}