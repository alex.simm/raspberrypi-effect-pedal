#include "BWHighPass.h"
#include <cmath>
#include <cassert>
#include <cstring>

BWHighPass::BWHighPass(unsigned int order, float sampleRate, float halfPowerFrequency) {
    assert(order % 2 == 0);
    n = order / 2;
    A = new sample[n];
    d1 = new sample[n];
    d2 = new sample[n];
    w0 = new sample[n];
    memset(w0, 0, n * sizeof(sample));
    w1 = new sample[n];
    memset(w1, 0, n * sizeof(sample));
    w2 = new sample[n];
    memset(w2, 0, n * sizeof(sample));

    const float factor = M_PI / (4.0 * n);
    double a = tan(M_PI * halfPowerFrequency / sampleRate);
    double a2 = a * a;
    double r, f;

    for (unsigned int i = 0; i < n; ++i) {
        r = sin(factor * (2.0 * i + 1.0));
        f = a2 + 2.0 * a * r + 1.0;
        A[i] = 1.0 / f;
        d1[i] = 2.0 * (1 - a2) / f;
        d2[i] = -(a2 - 2.0 * a * r + 1.0) / f;
    }
}

BWHighPass::~BWHighPass() {
    delete A;
    A = nullptr;
    delete d1;
    d1 = nullptr;
    delete d2;
    d2 = nullptr;
    delete w0;
    w0 = nullptr;
    delete w1;
    w1 = nullptr;
    delete w2;
    w2 = nullptr;
}

sample BWHighPass::apply(sample x) {
    for (uint i = 0; i < n; ++i) {
        w0[i] = d1[i] * w1[i] + d2[i] * w2[i] + x;
        x = A[i] * (w0[i] - 2.0 * w1[i] + w2[i]);
        w2[i] = w1[i];
        w1[i] = w0[i];
    }
    return x;
}