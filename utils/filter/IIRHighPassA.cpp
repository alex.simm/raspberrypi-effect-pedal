#include <math.h>
#include "IIRHighPassA.h"

IIRHighPassA::IIRHighPassA(double d) {
    setParam(d);
    reset();
}

sample IIRHighPassA::apply(sample x) {
    y1 = a0 * x + a1 * x1 + b1 * y1;
    x1 = x;
    return y1;
}

void IIRHighPassA::reset() {
    x1 = 0;
    y1 = 0;
}

void IIRHighPassA::setParam(float d) {
    a0 = .5 * (1. + d);
    a1 = -.5 * (1. + d);
    b1 = d;
}

void IIRHighPassA::setParamByFrequency(float frequency) {
    setParam(expf(-2 * M_PI * frequency));
}