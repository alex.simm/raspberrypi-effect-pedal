#include "controller/Controller.h"
#include "Logger.h"
#include <unistd.h>
#include <stdexcept>

using namespace std;

//TODO: line-in und -out immer auf 100% (?) / Level auf True-Bypass-Niveau normalisieren
//TODO: presets erstellen
//TODO: Zeit fuer alle Effekte messen und verbessern
//TODO: Tuner ist zu sensibel -> mean/max fuer threshold verwenden
//TODO: aus caps: autofilter, phaser2, chorus1 (like flanger?), spiceX2 (stereo of spice), plateX2 (stereo of plate; reverb)
//TODO: auf double umstellen -> weniger Diskretisierungsrauschen?

int main() {
    Logger::getInstance().setLevel(Logger::Level::DEBUG);
    Logger::getInstance().start();

    // start controller
    try {
        Logger::info("Main: starting controller");
        Controller controller;
        Logger::info("Main: going to sleep");
        sleep(-1);
        Logger::info("Main: sleep is over");
    }
    catch (exception &e) {
        Logger::fatal("Error in main: " + string(e.what()));
    }
}
