#include "FloatProperty.h"

FloatProperty::FloatProperty(std::string name, float min, float max, float value,
                             float inc) : Property(name, Type::CONTINUOUS),
                                          min(min), max(max), value(value),
                                          defaultValue(value), inc(inc) {
}

float FloatProperty::getValue() const {
    return value;
}

void FloatProperty::setValue(float value) {
    this->value = std::max(min, std::min(max, value));
}

void FloatProperty::reset() {
    value = defaultValue;
}
