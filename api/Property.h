#ifndef EFFECTPEDAL_PROPERTY_H
#define EFFECTPEDAL_PROPERTY_H

#include <string>

class Property {
public:
    enum Type {
        CONTINUOUS, DISCRETE, BOOLEAN, LIST
    };
    const Type type;

    virtual ~Property() = default;

    std::string getName() const;

    virtual void reset() = 0;

protected:
    Property(const std::string &name, Type type);

private:
    std::string name;
};

#endif //EFFECTPEDAL_PROPERTY_H
