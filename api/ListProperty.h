#ifndef EFFECTPEDAL_LISTPROPERTY_H
#define EFFECTPEDAL_LISTPROPERTY_H

#include "Property.h"
#include <vector>
#include <algorithm>

template<class T>
class ListProperty : public Property {
public:
    ListProperty(std::string name, std::vector<T> list, unsigned int index) : Property(name, Type::LIST),
                                                                              list(list),
                                                                              index(index),
                                                                              defaultIndex(index) {
    }

    const std::vector<T> &getList() const {
        return list;
    }

    unsigned int getSize() const {
        return (unsigned int) list.size();
    }

    T getValue() const {
        return list[index];
    }

    void setValue(T value) {
        T elem = std::find(list.begin(), list.end(), value);
        if (elem != list.end()) {
            auto newIndex = (unsigned int) std::distance(list.begin(), elem);
            if (newIndex != index)
                index = newIndex;
        }
    }

    unsigned int getIndex() {
        return index;
    }

    void setIndex(unsigned int index) {
        if (!list.empty())
            this->index = std::max(0u, std::min((unsigned int) (list.size() - 1), index));
    }

    int getDefaultIndex() const {
        return defaultIndex;
    }

    T getDefaultValue() const {
        return list[defaultIndex];
    }

    void reset() {
        index = defaultIndex;
    }

private:
    std::vector<T> list;
    unsigned int index, defaultIndex;
};


#endif //EFFECTPEDAL_LISTPROPERTY_H
