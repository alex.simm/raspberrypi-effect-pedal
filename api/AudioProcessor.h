#ifndef EFFECTPEDAL_AUDIOPROCESSOR_H
#define EFFECTPEDAL_AUDIOPROCESSOR_H

#include "Packet.h"
#include "Property.h"
#include <string>
#include <vector>

class AudioProcessor {

public:
    enum class Mode : unsigned int {
        DISABLED = 0, LEVEL_ONE = 1, LEVEL_TWO = 2, BOTH = 3
    };

    explicit AudioProcessor(const std::string &name);

    explicit AudioProcessor(const AudioProcessor &a);

    virtual ~AudioProcessor() = default;

    AudioProcessor &operator=(const AudioProcessor &a);

    std::string getName() const;

    const std::vector<Property *> &getProperties() const;

    Mode getMode() const;

    void setMode(Mode enabled);

    bool isExcludedFromPresets() const;

    /**
     * Called in the beginning to initialise this unit.
     */
    virtual void init(unsigned int sampleRate, unsigned int maxFrameSize);

    /**
    * Called if any of this unit's properties have been modified.
    */
    virtual void update();

    /**
    * Called when a line ends to stop the processor.
    */
    virtual void stop();

    /**
     * Called repeatedly to process frames.
     */
    virtual void process(Packet &packet) = 0;

protected:
    std::vector<Property *> properties;
    unsigned int maxFrameSize = 0;
    unsigned int sampleRate = 0;
    Mode mode = Mode::DISABLED;
    bool excludeFromPresets = false;

private:
    std::string name;
};

#endif //EFFECTPEDAL_AUDIOPROCESSOR_H
