#ifndef EFFECTPEDAL_AUDIOOUTPUT_H
#define EFFECTPEDAL_AUDIOOUTPUT_H

#include <vector>
#include <jack/jack.h>
#include "../AudioProcessor.h"
#include "AudioLine.h"

class AudioOutput : public AudioLine {

public:
    explicit AudioOutput(std::vector<AudioProcessor *> processors, AudioProcessor *source);

    ~AudioOutput();

    void start() override;

    void stop() override;

protected:
    void process() override;

private:
    AudioProcessor *source = nullptr;
    jack_port_t *output_port = nullptr;

    static int processCallback(jack_nframes_t numSamples, void *arg);
};


#endif //EFFECTPEDAL_AUDIOOUTPUT_H
