#ifndef EFFECTPEDAL_AUDIOINPUTOUTPUT_H
#define EFFECTPEDAL_AUDIOINPUTOUTPUT_H

#include "AudioLine.h"

class AudioInputOutput : public AudioLine {
public:
    AudioInputOutput(std::vector<AudioProcessor *> processors, AudioProcessor *firstProcessor = nullptr,
                     AudioProcessor *finalProcessor = nullptr);

    AudioInputOutput(std::vector<AudioProcessor *> processors, std::vector<AudioProcessor *> firstProcessors,
                     std::vector<AudioProcessor *> finalProcessors);

    ~AudioInputOutput();

    void start() override;

    void stop() override;

protected:
    void process() override;

private:
    std::vector<AudioProcessor *> firstProcessors, finalProcessors;
    jack_port_t *input_port = nullptr;
    jack_port_t *output_port = nullptr;

    static int processCallback(jack_nframes_t numSamples, void *arg);
};


#endif //EFFECTPEDAL_AUDIOINPUTOUTPUT_H
