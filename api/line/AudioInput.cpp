#include "AudioInput.h"
#include "../../Logger.h"
#include <cstring>
#include <stdexcept>

using namespace std;

AudioInput::AudioInput(std::vector<AudioProcessor *> processors, AudioProcessor *target) :
        AudioLine(processors), target(target) {
    if (target == nullptr) throw runtime_error("Audio target must not be null");
}

AudioInput::~AudioInput() {
}

void AudioInput::start() {
    try {
        // open client and set callback
        openJackClient();
        int err = jack_set_process_callback(client, processCallback, this);
        if (err != 0)
            throw runtime_error("AudioInput: error when setting callback: " + to_string(err));

        // initialise processors and target
        initProcessors();
        target->init(sampleRate, frameSize);
        target->update();

        // create ports
        Logger::debug("AudioInput: opening Jack ports");

        input_port = jack_port_register(client, "inOffset", JACK_DEFAULT_AUDIO_TYPE, JackPortIsInput, 0);
        if (input_port == nullptr)
            throw runtime_error("failed to register JACK ports (no more JACK ports available?)");

        int error;
        if ((error = jack_activate(client)) != 0)
            throw runtime_error("failed to activate client: " + to_string(error));

        /* Connect the ports.  You can't do this before the client is
         * activated, because we can't make connections to clients
         * that aren't running.  Note the confusing (but necessary)
         * orientation of the driver backend ports: playback ports are
         * "inOffset" to the backend, and capture ports are "outOffset" from
         * it. */
        if (input_port != nullptr) {
            const char **ports = jack_get_ports(client, nullptr, nullptr, JackPortIsPhysical | JackPortIsOutput);
            if (ports == nullptr)
                throw runtime_error("failed to get inOffset port");
            if ((error = jack_connect(client, ports[0], jack_port_name(input_port))) != 0)
                throw runtime_error("failed to connect inOffset ports: " + to_string(error));
            free(ports);
        }

        Logger::debug("AudioInput: line started");
    } catch (exception &e) {
        Logger::error("Error when starting input line: " + string(e.what()));
    }
}

void AudioInput::stop() {
    Logger::debug("AudioInput: stopping line");

    Logger::debug("AudioInput: stopping jack");
    try {
        if (client != nullptr) jack_client_close(client);
    } catch (exception &e) {
        Logger::error("AudioInput: error while closing jack client: " + string(e.what()));
    }

    Logger::debug("AudioInput: stopping processors");
    for (AudioProcessor *p : processors) {
        try {
            p->stop();
        } catch (exception &e) {
            Logger::error("AudioInput: error while stopping processor '"
                                 + p->getName() + "': " + string(e.what()));
        }
    }

    Logger::debug("AudioInput: stopping target " + target->getName());
    try {
        target->stop();
    } catch (exception &e) {
        Logger::error("AudioInput: error while stopping source '" + target->getName()
                             + "': " + string(e.what()));
    }
}

void AudioInput::process() {
    startMeasurement();

    AudioLine::process();

    try {
        target->process(packet);
    }
    catch (exception e) {
        Logger::error("exception in target '" + target->getName() + "': " + string(e.what()));
    }

    stopMeasurement();
}

int AudioInput::processCallback(jack_nframes_t numSamples, void *arg) {
    auto line = (AudioInput *) arg;
    auto in = (float *) jack_port_get_buffer(line->input_port, numSamples);

    line->packet.samples = in;
    line->packet.numSamples = numSamples;
    line->process();

    return 0;
}