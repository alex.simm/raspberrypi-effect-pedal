#ifndef EFFECTPEDAL_AUDIOLINE_H
#define EFFECTPEDAL_AUDIOLINE_H

#include <vector>
#include <jack/jack.h>
#include "../AudioProcessor.h"
#include "../../utils/analysis/LevelDetector.h"
#include <chrono>

class AudioLine {
public:
    AudioLine(std::vector<AudioProcessor *> processors);

    virtual ~AudioLine();

    virtual void start() = 0;

    virtual void stop() = 0;

    AudioProcessor::Mode getProcessorMode();

    void setProcessorMode(AudioProcessor::Mode mode);

protected:
    std::vector<AudioProcessor *> processors;
    jack_client_t *client = nullptr;
    unsigned int sampleRate, frameSize;
    Packet packet;
    AudioProcessor::Mode processorMode = AudioProcessor::Mode::BOTH;

    // level measurement
    LevelDetector levelDetector;
    float longTermMean = 0, longTermMax = 0;
    unsigned long longTermFrameCount = 0;

    virtual void process();

    void openJackClient();

    void initProcessors();

    void startMeasurement();

    void stopMeasurement();

private:
    std::chrono::microseconds availableTimePerFrame, averageTimePerFrame;
    std::chrono::system_clock::time_point frameStart;
    uint numAveragedFrames;
};


#endif //EFFECTPEDAL_AUDIOLINE_H
