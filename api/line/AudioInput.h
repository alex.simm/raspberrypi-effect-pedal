#ifndef EFFECTPEDAL_AUDIOINPUT_H
#define EFFECTPEDAL_AUDIOINPUT_H

#include <vector>
#include <jack/jack.h>
#include "../AudioProcessor.h"
#include "AudioLine.h"

class AudioInput : public AudioLine {

public:
    AudioInput(std::vector<AudioProcessor *> processors, AudioProcessor *target);

    ~AudioInput() override;

    void start() override;

    void stop() override;

protected:
    void process() override;

private:
    AudioProcessor *target = nullptr;
    jack_port_t *input_port = nullptr;

    static int processCallback(jack_nframes_t numSamples, void *arg);
};


#endif //EFFECTPEDAL_AUDIOINPUT_H
