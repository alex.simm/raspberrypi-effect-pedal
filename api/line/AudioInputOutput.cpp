#include "AudioInputOutput.h"
#include <cstring>
#include <stdexcept>
#include "../../Logger.h"

using namespace std;

AudioInputOutput::AudioInputOutput(std::vector<AudioProcessor *> processors, AudioProcessor *firstProcessor,
                                   AudioProcessor *finalProcessor)
        : AudioLine(processors), firstProcessors({firstProcessor}), finalProcessors({finalProcessor}) {
}

AudioInputOutput::AudioInputOutput(std::vector<AudioProcessor *> processors,
                                   std::vector<AudioProcessor *> firstProcessors,
                                   std::vector<AudioProcessor *> finalProcessors)
        : AudioLine(processors), firstProcessors(firstProcessors), finalProcessors(finalProcessors) {
}

AudioInputOutput::~AudioInputOutput() {
}

void AudioInputOutput::start() {
    try {
        // open client and set callback
        openJackClient();

        // initialise processors
        initProcessors();
        for (AudioProcessor *p : firstProcessors) {
            if (p != nullptr) {
                p->init(sampleRate, frameSize);
                p->update();
            }
        }
        for (AudioProcessor *p : finalProcessors) {
            if (p != nullptr) {
                p->init(sampleRate, frameSize);
                p->update();
            }
        }

        int err = jack_set_process_callback(client, processCallback, this);
        if (err != 0)
            throw runtime_error("AudioInputOutput: error when setting callback: " + to_string(err));

        // create ports
        Logger::debug("AudioInputOutput: opening Jack ports");

        input_port = jack_port_register(client, "inOffset", JACK_DEFAULT_AUDIO_TYPE, JackPortIsInput, 0);
        output_port = jack_port_register(client, "outOffset", JACK_DEFAULT_AUDIO_TYPE, JackPortIsOutput, 0);
        if (input_port == nullptr || output_port == nullptr)
            throw runtime_error("failed to register JACK ports (no more JACK ports available?)");

        int error;
        if ((error = jack_activate(client)) != 0)
            throw runtime_error("failed to activate client: " + to_string(error));

        /* Connect the ports.  You can't do this before the client is
         * activated, because we can't make connections to clients
         * that aren't running.  Note the confusing (but necessary)
         * orientation of the driver backend ports: playback ports are
         * "inOffset" to the backend, and capture ports are "outOffset" from
         * it. */
        if (input_port != nullptr) {
            const char **ports = jack_get_ports(client, nullptr, nullptr, JackPortIsPhysical | JackPortIsOutput);
            if (ports == nullptr)
                throw runtime_error("failed to get input port");
            if ((error = jack_connect(client, ports[0], jack_port_name(input_port))) != 0)
                throw runtime_error("failed to connect input ports: " + to_string(error));
            free(ports);
        }

        if (output_port != nullptr) {
            const char **ports = jack_get_ports(client, nullptr, nullptr, JackPortIsPhysical | JackPortIsInput);
            if (ports == nullptr)
                throw runtime_error("failed to get output port");
            if ((error = jack_connect(client, jack_port_name(output_port), ports[0])) != 0)
                throw runtime_error("failed to connect output ports: " + to_string(error));
            free(ports);
        }

        Logger::debug("AudioInputOutput: line started");
    }
    catch (exception &e) {
        Logger::error("Error when starting input/output line: " + string(e.what()));
    }
}

void AudioInputOutput::stop() {
    Logger::debug("AudioInputOutput: stopping line");

    Logger::debug("AudioInputOutput: stopping jack");
    try {
        if (client != nullptr) jack_client_close(client);
    } catch (exception &e) {
        Logger::error("AudioInputOutput: error while closing jack client: " + string(e.what()));
    }

    Logger::debug("AudioInputOutput: stopping processors");
    for (AudioProcessor *p : processors) {
        try {
            if (p != nullptr) p->stop();
        } catch (exception &e) {
            Logger::error("AudioInputOutput: error while stopping processor '"
                                 + p->getName() + "': " + string(e.what()));
        }
    }

    for (AudioProcessor *p : firstProcessors) {
        try {
            if (p != nullptr) p->stop();
        } catch (exception &e) {
            Logger::error("AudioOutput: error while stopping processor '" + p->getName()
                                 + "': " + string(e.what()));
        }
    }

    for (AudioProcessor *p : finalProcessors) {
        try {
            if (p != nullptr) p->stop();
        } catch (exception &e) {
            Logger::error("AudioOutput: error while stopping processor '" + p->getName()
                                 + "': " + string(e.what()));
        }
    }
}

void AudioInputOutput::process() {
    startMeasurement();

    for (AudioProcessor *p : firstProcessors) {
        try {
            if (p != nullptr) p->process(packet);
        }
        catch (exception &e) {
            Logger::info(
                "AudioInputOutput: exception in processor '" + p->getName() + "': " + string(e.what()));
        }
    }

    AudioLine::process();

    for (AudioProcessor *p : finalProcessors) {
        try {
            if (p != nullptr) p->process(packet);
        }
        catch (exception &e) {
            Logger::info(
                "AudioInputOutput: exception in processor '" + p->getName() + "': " + string(e.what()));
        }
    }

    stopMeasurement();
}

int AudioInputOutput::processCallback(jack_nframes_t numSamples, void *arg) {
    auto line = (AudioInputOutput *) arg;
    auto in = (float *) jack_port_get_buffer(line->input_port, numSamples);
    auto out = (float *) jack_port_get_buffer(line->output_port, numSamples);

    line->packet.samples = in;
    line->packet.numSamples = numSamples;
    line->process();
    memcpy(out, in, sizeof(jack_default_audio_sample_t) * numSamples);

    return 0;
}