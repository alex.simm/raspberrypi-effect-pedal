#include "AudioLine.h"
#include "../../Logger.h"
#include "../../GlobalConfig.h"
#include <cstring>

using namespace std;

AudioLine::AudioLine(std::vector<AudioProcessor *> processors) : processors(processors) {
}

AudioLine::~AudioLine() = default;

void AudioLine::openJackClient() {
    Logger::debug("AudioLine: opening Jack client");

    std::string clientName = "EffectPedal";
    jack_options_t options = JackNullOption;
    jack_status_t status;

    // open connection
    client = jack_client_open(clientName.c_str(), options, &status, NULL);
    if (client == nullptr) {
        if (status & JackServerFailed)
            throw runtime_error("unable to connect to JACK server");
        else
            throw runtime_error("failed to open JACK client (status = " + to_string(status) + ")");
    }

    if (status & JackServerStarted)
        Logger::debug("JACK server started");
    if (status & JackNameNotUnique) {
        clientName = string(jack_get_client_name(client));
        Logger::info("unique client name: " + clientName);
    }
}

void AudioLine::initProcessors() {
    Logger::debug("AudioLine: initialising processors");

    // initialise processors
    sampleRate = jack_get_sample_rate(client);
    Logger::info("sample rate: " + std::to_string(sampleRate));
    frameSize = jack_get_buffer_size(client);
    Logger::info("frame size: " + std::to_string(frameSize));
    for (AudioProcessor *p : processors) {
        p->init(sampleRate, frameSize);
        p->update();
    }

    levelDetector.setSampleRate(sampleRate);

    availableTimePerFrame = chrono::microseconds(int(2 * float(frameSize) / sampleRate * 1e6));
    Logger::info("available time per frame: " + std::to_string(availableTimePerFrame.count()));

    numAveragedFrames = 0;
}

void AudioLine::startMeasurement() {
    frameStart = chrono::high_resolution_clock::now();
}

//empty line: ~ 140 mus
void AudioLine::stopMeasurement() {
    averageTimePerFrame += chrono::duration_cast<chrono::microseconds>(
            chrono::high_resolution_clock::now() - frameStart);
    numAveragedFrames++;

    // prevent overflow
    if (numAveragedFrames == 100) {
        //Logger::debug("available time: " + to_string(availableTimePerFrame.count()) +
        //                     ", average: " + to_string(averageTimePerFrame.count() / numAveragedFrames));

        averageTimePerFrame = chrono::microseconds(0);
        numAveragedFrames = 0;
    }
}

#include <iostream>

int count = 0;

void AudioLine::process() {
    // calculate input level
    switch (GlobalConfig::getInstance().getLevelCalculationMode()) {
        case GlobalConfig::LevelCalculationMode::FRAME:
            levelDetector.process(packet.samples, packet.numSamples);
            packet.meanLevel = levelDetector.getMeanLevel();
            packet.maxLevel = levelDetector.getMaxLevel();
            break;
        case GlobalConfig::LevelCalculationMode::LONG_TERM:
            levelDetector.process(packet.samples, packet.numSamples);
            longTermMean += levelDetector.getMeanLevel();
            longTermMax += levelDetector.getMaxLevel();
            longTermFrameCount++;

            // prevent float overflow
            if (longTermFrameCount == 1000) {
                longTermMax /= longTermFrameCount;
                longTermMean /= longTermFrameCount;
                longTermFrameCount = 1;
            }

            packet.meanLevel = longTermMean / longTermFrameCount;
            packet.maxLevel = longTermMax / longTermFrameCount;
            break;
        case GlobalConfig::LevelCalculationMode::CONSTANT:
        default:
            packet.meanLevel = GlobalConfig::getInstance().getMeanInputLevel();
            packet.maxLevel = GlobalConfig::getInstance().getMaxInputLevel();
            break;
    }

    //levelDetector.process(packet.samples, packet.numSamples);
    //float mean = levelDetector.getMeanLevel();
    //float max = levelDetector.getMaxLevel();

    // pass through processors
    if (processorMode > AudioProcessor::Mode::DISABLED) {
        for (AudioProcessor *p : processors) {
            try {
                if (((uint) p->getMode() & (uint) processorMode) != 0)
                    p->process(packet);
            }
            catch (exception &e) {
                Logger::error("exception in processor '" + p->getName() + "': " + string(e.what()));
            }
        }
    }

    /*count++;
    if (count == 100) {
        levelDetector.process(packet.samples, packet.numSamples);

        cout << mean << " / " << levelDetector.getMeanLevel() << "\t"
             << max << " / " << levelDetector.getMaxLevel()
             << endl;
        count = 0;
    }*/
}

AudioProcessor::Mode AudioLine::getProcessorMode() {
    return processorMode;
}

void AudioLine::setProcessorMode(AudioProcessor::Mode mode) {
    processorMode = mode;
}