#include "AudioOutput.h"
#include <cstring>
#include <stdexcept>
#include "../../Logger.h"

using namespace std;

AudioOutput::AudioOutput(std::vector<AudioProcessor *> processors, AudioProcessor *source)
        : AudioLine(processors), source(source) {
    if (source == nullptr) throw runtime_error("Audio source must not be null");
}

AudioOutput::~AudioOutput() {
}

void AudioOutput::start() {
    try {
        // open client and set callback
        openJackClient();
        int err = jack_set_process_callback(client, processCallback, this);
        if (err != 0)
            throw runtime_error("AudioOutput: error when setting callback: " + to_string(err));

        // initialise processors and source
        initProcessors();
        source->init(sampleRate, frameSize);
        source->update();

        // create port
        Logger::debug("AudioOutput: opening Jack ports");
        output_port = jack_port_register(client, "outOffset", JACK_DEFAULT_AUDIO_TYPE, JackPortIsOutput, 0);

        if (output_port == nullptr)
            throw runtime_error("AudioOutput: failed to register JACK port (no more JACK ports available?)");

        int error;
        if ((error = jack_activate(client)) != 0)
            throw runtime_error("AudioOutput: failed to activate client: " + to_string(error));

        /* Connect the ports.  You can't do this before the client is
         * activated, because we can't make connections to clients
         * that aren't running.  Note the confusing (but necessary)
         * orientation of the driver backend ports: playback ports are
         * "inOffset" to the backend, and capture ports are "outOffset" from
         * it. */
        const char **ports = jack_get_ports(client, nullptr, nullptr, JackPortIsPhysical | JackPortIsInput);
        if (ports == nullptr)
            throw runtime_error("AudioOutput: failed to get outOffset port");
        if ((error = jack_connect(client, jack_port_name(output_port), ports[0])) != 0)
            throw runtime_error("AudioOutput: failed to connect outOffset ports: " + to_string(error));

        free(ports);
        Logger::debug("AudioOutput: line started");
    }
    catch (exception &e) {
        Logger::error("AudioOutput: Error when starting output line: " + string(e.what()));
    }
}

void AudioOutput::stop() {
    Logger::debug("AudioOutput: stopping line");

    Logger::debug("AudioOutput: stopping jack");
    try {
        if (client != nullptr) jack_client_close(client);
    } catch (exception &e) {
        Logger::error("AudioOutput: error while closing jack client: " + string(e.what()));
    }

    Logger::debug("AudioOutput: stopping processors");
    for (AudioProcessor *p : processors) {
        try {
            p->stop();
        } catch (exception &e) {
            Logger::error("AudioOutput: error while stopping processor '"
                                 + p->getName() + "': " + string(e.what()));
        }
    }

    Logger::debug("AudioOutput: stopping source " + source->getName());
    try {
        source->stop();
    } catch (exception &e) {
        Logger::error("AudioOutput: error while stopping source '" + source->getName()
                             + "': " + string(e.what()));
    }
}

void AudioOutput::process() {
    startMeasurement();

    try {
        source->process(packet);
    }
    catch (exception &e) {
        Logger::info("AudioOutput: exception in source '" + source->getName() + "': " + string(e.what()));
    }

    AudioLine::process();

    stopMeasurement();
}

int AudioOutput::processCallback(jack_nframes_t numSamples, void *arg) {
    auto line = (AudioOutput *) arg;
    auto out = (float *) jack_port_get_buffer(line->output_port, numSamples);

    line->packet.samples = out;
    line->packet.numSamples = numSamples;
    line->process();

    return 0;
}