#include "BooleanProperty.h"

BooleanProperty::BooleanProperty(std::string name, bool value)
        : Property(name, Type::BOOLEAN), value(value), defaultValue(value) {
}

bool BooleanProperty::getValue() const {
    return value;
}

void BooleanProperty::setValue(bool value) {
    if (value != this->value)
        this->value = value;
}

void BooleanProperty::toggle() {
    setValue(!value);
}

bool BooleanProperty::getDefaultValue() const {
    return defaultValue;
}

void BooleanProperty::reset() {
    value = defaultValue;
}
