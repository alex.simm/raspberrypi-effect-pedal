#ifndef EFFECTPEDAL_DISCRETEPROPERYT_H
#define EFFECTPEDAL_DISCRETEPROPERYT_H

#include "Property.h"

class IntegerProperty : public Property {

public:
    const int min, max, inc;

    IntegerProperty(std::string name, int min, int max, int value, int inc = 1);

    int getValue() const;

    void setValue(int value);

    int getDefaultValue() const;

    void reset();

private:
    int value, defaultValue;
};


#endif //EFFECTPEDAL_DISCRETEPROPERYT_H
