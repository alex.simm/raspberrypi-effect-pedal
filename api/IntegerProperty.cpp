#include "IntegerProperty.h"

IntegerProperty::IntegerProperty(std::string name, int min, int max, int value, int inc)
        : Property(name, Type::DISCRETE), min(min), max(max), value(value), defaultValue(value), inc(inc) {
}

int IntegerProperty::getValue() const {
    return value;
}

void IntegerProperty::setValue(int value) {
    this->value = std::max(min, std::min(max, value));
}

int IntegerProperty::getDefaultValue() const {
    return defaultValue;
}

void IntegerProperty::reset() {
    value = defaultValue;
}
