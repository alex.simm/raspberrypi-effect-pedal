#ifndef EFFECTPEDAL_BOOLEANPROPERTY_H
#define EFFECTPEDAL_BOOLEANPROPERTY_H

#include "Property.h"

class BooleanProperty : public Property {

public:
    BooleanProperty(std::string name, bool value = false);

    bool getValue() const;

    void setValue(bool value);

    void toggle();

    bool getDefaultValue() const;

    void reset();

private:
    bool value, defaultValue;
};

#endif //EFFECTPEDAL_BOOLEANPROPERTY_H
