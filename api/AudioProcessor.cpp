#include "AudioProcessor.h"

AudioProcessor::AudioProcessor(const std::string &name) : name(name) {
}

AudioProcessor::AudioProcessor(const AudioProcessor &a) : name(a.name) {
}

AudioProcessor &AudioProcessor::operator=(const AudioProcessor &a) {
    name = a.name;
    return *this;
}

std::string AudioProcessor::getName() const {
    return name;
}

const std::vector<Property *> &AudioProcessor::getProperties() const {
    return properties;
}

AudioProcessor::Mode AudioProcessor::getMode() const {
    return mode;
}

void AudioProcessor::setMode(Mode enabled) {
    this->mode = enabled;
}

bool AudioProcessor::isExcludedFromPresets() const {
    return excludeFromPresets;
}

void AudioProcessor::init(unsigned int sampleRate, unsigned int maxFrameSize) {
    this->sampleRate = sampleRate;
    this->maxFrameSize = maxFrameSize;
}

void AudioProcessor::update() {
}

void AudioProcessor::stop() {
}
