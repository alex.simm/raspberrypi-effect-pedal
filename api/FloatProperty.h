#ifndef EFFECTPEDAL_CONTINUOUSPROPERTY_H
#define EFFECTPEDAL_CONTINUOUSPROPERTY_H

#include "Property.h"

class FloatProperty : public Property {

public:
    const float min, max, inc;

    FloatProperty(std::string name, float min, float max, float value, float inc);

    float getValue() const;

    void setValue(float value);

    void reset();

private:
    float value, defaultValue;
};

#endif //EFFECTPEDAL_CONTINUOUSPROPERTY_H
