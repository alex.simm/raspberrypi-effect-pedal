#ifndef EFFECTPEDAL_PACKET_H
#define EFFECTPEDAL_PACKET_H

class Packet {
public:
    float *samples;
    unsigned int numSamples = 0;
    float meanLevel, maxLevel;
};


#endif //EFFECTPEDAL_PACKET_H
