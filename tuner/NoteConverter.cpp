#include <cmath>
#include "NoteConverter.h"

NoteConverter::NoteConverter() {
}

void NoteConverter::setFrequency(float frequency) {
    // find note
    const float fvis = 12 * log2f(frequency / referencePitch);
    int vis = int(roundf(fvis));
    const float scale = (fvis - vis) / 2.0f;
    vis %= 12;
    if (vis < 0)
        vis += 12;
    noteIdx = vis;

    // find octave
    octave = -1;
    if (fabsf(scale) < 0.1) {
        if (frequency < 31.78 && frequency > 0.0) {
            octave = 0;
        } else if (frequency < 63.57) {
            octave = 1;
        } else if (frequency < 127.14) {
            octave = 2;
        } else if (frequency < 254.28) {
            octave = 3;
        } else if (frequency < 509.44) {
            octave = 4;
        } else if (frequency < 1017.35) {
            octave = 5;
        } else if (frequency < 2034.26) {
            octave = 6;
        } else if (frequency < 4068.54) {
            octave = 7;
        } else {
            octave = 8;
        }
    }

    // find cents and log cents
    cents = static_cast<int>((floorf(scale * 10000) / 50));
    if (cents == 0) {
        logCents = scale;
    } else if (cents < 2 && cents > -2) { // 1 cents
        if (scale < 0.0) logCents = scale - 0.0155f;
        else logCents = scale + 0.0155f;
    } else if (cents < 3 && cents > -3) { // 2 cents
        if (scale < 0.0) logCents = scale - 0.0265f;
        else logCents = scale + 0.0265f;
    } else if (cents < 4 && cents > -4) { // 3 cents
        if (scale < 0.0) logCents = scale - 0.0355f;
        else logCents = scale + 0.0355f;
    } else if (cents < 5 && cents > -5) { // 4 cents
        if (scale < 0.0) logCents = scale - 0.0455f;
        else logCents = scale + 0.0455f;
    } else if (cents < 6 && cents > -6) { // 5 cents
        if (scale < 0.0) logCents = scale - 0.0435f;
        else logCents = scale + 0.0435f;
    } else if (cents < 7 && cents > -7) { // 6 cents
        if (scale < 0.0) logCents = scale - 0.0425f;
        else logCents = scale + 0.0425f;
    } else if (cents < 8 && cents > -8) { // 7 cents
        if (scale < 0.0) logCents = scale - 0.0415f;
        else logCents = scale + 0.0415f;
    } else if (cents < 9 && cents > -9) { // 8 cents
        if (scale < 0.0) logCents = scale - 0.0405f;
        else logCents = scale + 0.0405f;
    } else if (cents < 10 && cents > -10) { // 9 cents
        if (scale < 0.0) logCents = scale - 0.0385f;
        else logCents = scale + 0.0385f;
    } else if (cents < 11 && cents > -11) { // 10 cents
        if (scale < 0.0) logCents = scale - 0.0365f;
        else logCents = scale + 0.0365f;
    } else if (cents < 51 && cents > -51) { // < 50 cents
        logCents = scale + (0.4f / cents);
    } else logCents = scale;
}

int NoteConverter::getOctave() {
    return octave;
}

int NoteConverter::getCents() {
    return cents;
}

std::string NoteConverter::getNote() {
    return note[noteIdx];
}

float NoteConverter::getLogCents() {
    return logCents;
}