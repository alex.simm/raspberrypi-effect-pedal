#include "LowHighCut.h"
#include <cmath>

using namespace std;

void LowHighCut::init(int samplerate) {
    this->sampleRate = samplerate;
    fConst1 = tan(3138.4510609362032 / samplerate);
    fConst2 = 2 * (1 - 1.0 / (fConst1 * fConst1));
    fConst3 = 1.0 / fConst1;
    fConst4 = 1 + (fConst3 - 0.7653668647301795) / fConst1;
    fConst5 = 1.0 / (1 + (0.7653668647301795 + fConst3) / fConst1);
    fConst6 = 1 + (fConst3 - 1.8477590650225735) / fConst1;
    fConst7 = 1.0 / (1 + (fConst3 + 1.8477590650225735) / fConst1);
    fConst8 = 72.25663103256524 / samplerate;
    fConst9 = 1 - fConst8;
    fConst10 = 1.0 / (1 + fConst8);

    for (int i = 0; i < 2; i++) {
        fVec1[i] = 0;
        fRec3[i] = 0;
        fRec2[i] = 0;
    }
    for (int i = 0; i < 3; i++) {
        fRec1[i] = 0;
        fRec0[i] = 0;
    }
}

void LowHighCut::compute(int count, const float *input0, float *output0) {
    for (unsigned int i = 0; i < count; i++) {
        fVec1[0] = (double) input0[i];
        fRec3[0] = fConst10 * (fVec1[0] - fVec1[1] + fConst9 * fRec3[1]);
        fRec2[0] = fConst10 * (fRec3[0] - fRec3[1] + fConst9 * fRec2[1]);
        fRec1[0] = fRec2[0] - fConst7 * (fConst6 * fRec1[2] + fConst2 * fRec1[1]);
        fRec0[0] = fConst7 * (fRec1[2] + (fRec1[0] + 2 * fRec1[1])) -
                   fConst5 * (fConst4 * fRec0[2] + fConst2 * fRec0[1]);
        output0[i] = (float) (fConst5 * (fRec0[2] + (fRec0[0] + 2 * fRec0[1])));

        // post processing
        fRec0[2] = fRec0[1];
        fRec0[1] = fRec0[0];
        fRec1[2] = fRec1[1];
        fRec1[1] = fRec1[0];
        fRec2[1] = fRec2[0];
        fRec3[1] = fRec3[0];
        fVec1[1] = fVec1[0];
    }
}

