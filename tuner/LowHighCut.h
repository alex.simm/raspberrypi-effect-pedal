#ifndef EFFECTPEDAL_LOWHIGHCUT_H
#define EFFECTPEDAL_LOWHIGHCUT_H

class LowHighCut {
public:
    void init(int samplerate);

    void compute(int count, const float * input0, float *output0);

private:
    int sampleRate;
    double fConst1;
    double fConst2;
    double fConst3;
    double fConst4;
    double fConst5;
    double fConst6;
    double fConst7;
    double fConst8;
    double fConst9;
    double fVec1[2];
    double fConst10;
    double fRec3[2];
    double fRec2[2];
    double fRec1[3];
    double fRec0[3];
};


#endif //EFFECTPEDAL_LOWHIGHCUT_H
