#ifndef EFFECTPEDAL_TUNER_H
#define EFFECTPEDAL_TUNER_H

#include "PitchTracker.h"
#include "NoteConverter.h"
#include "../api/BooleanProperty.h"
#include "../api/AudioProcessor.h"
#include "../api/FloatProperty.h"
#include "../api/AudioProcessor.h"

class Tuner : public AudioProcessor, PitchTracker::Listener {
public:
    class Listener {
    public:
        virtual void frequencyUpdated(float frequency, std::string note, int octave,
                                      int cents, float logCents) = 0;
    };

    BooleanProperty fast;

    Tuner();

    Tuner &operator=(const Tuner &t) {
        AudioProcessor::operator=(t);
        fast.setValue(t.fast.getValue());
        return *this;
    }

    void init(unsigned int sampleRate, unsigned int maxFrameSize) override;

    void update() override;

    void process(Packet &packet) override;

    bool isRunning();

    void start();

    void stop() override;

    void frequencyUpdated() override;

    void setListener(Listener *listener) {
        this->listener = listener;
    }

private:
    PitchTracker tracker;
    NoteConverter converter;
    Listener *listener = nullptr;
};


#endif //EFFECTPEDAL_TUNER_H
