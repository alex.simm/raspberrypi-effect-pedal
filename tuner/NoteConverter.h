#ifndef EFFECTPEDAL_NOTECONVERTER_H
#define EFFECTPEDAL_NOTECONVERTER_H

#include <string>
#include <vector>

class NoteConverter {
private:
    friend class Tuner;

    NoteConverter();

    void setFrequency(float frequency);

    int getOctave();

    int getCents();

    float getLogCents();

    std::string getNote();

    const char *const note[12] = {"A", "A#", "B", "C", "C#", "D", "D#", "E", "F", "F#", "G", "G#"};
    const unsigned int referencePitch = 440;

    int noteIdx, octave, cents;
    float logCents;
};

#endif //EFFECTPEDAL_NOTECONVERTER_H
