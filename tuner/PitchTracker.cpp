#include "PitchTracker.h"
#include <cstring>
#include <cassert>
#include <cmath>
#include <stdexcept>
#include <stdexcept>

//TODO: aufraeumen, verbessern
//TODO: threshold verwenden
using namespace std;

PitchTracker::PitchTracker() : busy(false), packetCount(0), resamp(), m_sampleRate(), fixed_sampleRate(41000),
                               detectedFrequency(-1),
                               signal_threshold_on(SIGNAL_THRESHOLD_ON), signal_threshold_off(SIGNAL_THRESHOLD_OFF),
                               tracker_period(TRACKER_PERIOD), m_buffersize(), m_fftSize(),
                               m_buffer(new float[FFT_SIZE]), m_bufferIndex(0),
                               m_input(new float[FFT_SIZE]), m_audioLevel(false),
                               fftForward(nullptr), fftInverse(nullptr), low_high_cut(),
                               trigger() {
    const int size = FFT_SIZE + (FFT_SIZE + 1) / 2;
    fftBufferTime = reinterpret_cast<float *>(fftwf_malloc(size * sizeof(*fftBufferTime)));
    fftBufferFrequency = reinterpret_cast<float *>(fftwf_malloc(size * sizeof(*fftBufferFrequency)));

    memset(m_buffer, 0, FFT_SIZE * sizeof(*m_buffer));
    memset(m_input, 0, FFT_SIZE * sizeof(*m_input));
    memset(fftBufferTime, 0, size * sizeof(*fftBufferTime));
    memset(fftBufferFrequency, 0, size * sizeof(*fftBufferFrequency));

    sem_init(&trigger, 0, 0);

    if (!fftBufferTime || !fftBufferFrequency)
        throw runtime_error("Tuner: unable to allocate FFT");
}


PitchTracker::~PitchTracker() {
    stop();

    fftwf_destroy_plan(fftForward);
    fftwf_destroy_plan(fftInverse);
    fftwf_free(fftBufferTime);
    fftwf_free(fftBufferFrequency);
    delete m_input;
    m_input = nullptr;
    delete m_buffer;
    m_buffer = nullptr;
}

void PitchTracker::setListener(PitchTracker::Listener *listener) {
    this->listener = listener;
}

void PitchTracker::setThreshold(float v) {
    signal_threshold_on = v;
    signal_threshold_off = v * 0.9f;
}

float PitchTracker::getThreshold() {
    // Value of the threshold above which the processing is activated.
    return signal_threshold_on;
}

void PitchTracker::setFastNoteDetection(bool enabled) {
    if (enabled) {
        signal_threshold_on = SIGNAL_THRESHOLD_ON * 5;
        signal_threshold_off = SIGNAL_THRESHOLD_OFF * 5;
        tracker_period = TRACKER_PERIOD / 10;
    } else {
        signal_threshold_on = SIGNAL_THRESHOLD_ON;
        signal_threshold_off = SIGNAL_THRESHOLD_OFF;
        tracker_period = TRACKER_PERIOD;
    }
}

void PitchTracker::init(int sampleRate) {
    m_sampleRate = fixed_sampleRate / DOWNSAMPLE;
    resamp.setup((unsigned int) sampleRate, (unsigned int) m_sampleRate, 1, 16); // 16 == least quality

    if (m_buffersize != FFT_SIZE) {
        m_buffersize = FFT_SIZE;
        m_fftSize = m_buffersize + (m_buffersize + 1) / 2;
        fftwf_destroy_plan(fftForward);
        fftwf_destroy_plan(fftInverse);
        fftForward = fftwf_plan_r2r_1d(m_fftSize, fftBufferTime, fftBufferFrequency, FFTW_R2HC, FFTW_ESTIMATE);
        fftInverse = fftwf_plan_r2r_1d(m_fftSize, fftBufferFrequency, fftBufferTime, FFTW_HC2R, FFTW_ESTIMATE);
    }

    if (!fftForward || !fftInverse)
        throw runtime_error("Tuner: failed to initialise FFT");

    if (!isRunning())
        start();

    low_high_cut.init(sampleRate);
}

void PitchTracker::stopImpl() {
    sem_post(&trigger);
}

void PitchTracker::reset() {
    packetCount = 0;
    m_bufferIndex = 0;
    resamp.reset();
    detectedFrequency = -1;
}

void PitchTracker::add(int numSamples, float *input) {
    float output[numSamples];
    low_high_cut.compute(numSamples, input, output);
    resamp.inp_count = (unsigned int) numSamples;
    resamp.inp_data = output;
    for (;;) {
        resamp.out_data = &m_buffer[m_bufferIndex];
        int n = FFT_SIZE - m_bufferIndex;
        resamp.out_count = (unsigned int) n;
        resamp.process();
        n -= resamp.out_count; // n := number of output samples
        if (!n) { // all soaked up by filter
            return;
        }
        m_bufferIndex = (m_bufferIndex + n) % FFT_SIZE; //TODO: &(FFT_SIZE-1)
        if (resamp.inp_count == 0) {
            break;
        }
    }

    packetCount++;
    if (packetCount * numSamples >= m_sampleRate * DOWNSAMPLE * tracker_period) {
        if (busy) {
            return;
        }
        busy = true;
        packetCount = 0;
        copy();
        sem_post(&trigger);
    }
}

void PitchTracker::copy() {
    int start = (FFT_SIZE + m_bufferIndex - m_buffersize) % FFT_SIZE; //TODO: &(FFT_SIZE-1)
    int end = (FFT_SIZE + m_bufferIndex) % FFT_SIZE;
    int count = 0;
    if (start >= end) {
        count = FFT_SIZE - start;
        memcpy(m_input, &m_buffer[start], count * sizeof(*m_input));
        start = 0;
    }
    memcpy(&m_input[count], &m_buffer[start], (end - start) * sizeof(*m_input));
}

float PitchTracker::getEstimatedFrequency() {
    return detectedFrequency < 0 ? 0 : detectedFrequency;
}

inline float PitchTracker::parabolaTurningPoint(float y_1, float y0, float y1, float xOffset) {
    float yTop = y_1 - y1;
    float yBottom = y1 + y_1 - 2 * y0;
    return (yBottom != 0.0) ? xOffset + yTop / (2 * yBottom) : xOffset;
}

int PitchTracker::findMaxima(const float *input, int len, int *maxPositions, int *length, int maxLen) {
    int pos = 0;
    int curMaxPos = 0;
    int overallMaxIndex = 0;

    while (pos < (len - 1) / 3 && input[pos] > 0.0) {
        pos++;  // find the first negitive zero crossing
    }
    while (pos < len - 1 && input[pos] <= 0.0) {
        pos++;  // loop over all the values below zero
    }
    if (pos == 0) {
        pos = 1;  // can happen if output[0] is NAN
    }

    while (pos < len - 1) {
        if (input[pos] > input[pos - 1] && input[pos] >= input[pos + 1]) {  // a local maxima
            if (curMaxPos == 0) {
                curMaxPos = pos;  // the first maxima (between zero crossings)
            } else if (input[pos] > input[curMaxPos]) {
                curMaxPos = pos;  // a higher maxima (between the zero crossings)
            }
        }
        pos += 1;
        if (pos < len - 1 && input[pos] <= 0.0) {  // a negative zero crossing
            if (curMaxPos > 0) {  // if there was a maximum
                maxPositions[*length] = curMaxPos;  // add it to the vector of maxima
                *length += 1;
                if (overallMaxIndex == 0) {
                    overallMaxIndex = curMaxPos;
                } else if (input[curMaxPos] > input[overallMaxIndex]) {
                    overallMaxIndex = curMaxPos;
                }
                if (*length >= maxLen) {
                    return overallMaxIndex;
                }
                curMaxPos = 0;  // clear the maximum position, so we start looking for a new ones
            }
            while (pos < len - 1 && input[pos] <= 0.0) {
                pos += 1;  // loop over all the values below zero
            }
        }
    }
    if (curMaxPos > 0) {  // if there was a maximum in the last part
        maxPositions[*length] = curMaxPos;  // add it to the vector of maxima
        *length += 1;
        if (overallMaxIndex == 0) {
            overallMaxIndex = curMaxPos;
        } else if (input[curMaxPos] > input[overallMaxIndex]) {
            overallMaxIndex = curMaxPos;
        }
        //curMaxPos = 0;  // clear the maximum position, so we start looking for a new ones
    }
    return overallMaxIndex;
}

int PitchTracker::findsubMaximum(float *input, int len, float threshold) {
    int indices[10];
    int length = 0;
    int overallMaxIndex = findMaxima(input, len, indices, &length, 10);
    if (length == 0)
        return -1;

    threshold += (1.0 - threshold) * (1.0 - input[overallMaxIndex]);
    float cutoff = input[overallMaxIndex] * threshold;
    for (int j = 0; j < length; j++) {
        if (input[indices[j]] >= cutoff) {
            return indices[j];
        }
    }

    // should never get here
    return -1;
}

void PitchTracker::run() {
    while (running) {
        busy = false;
        sem_wait(&trigger);

        // check threshold
        float sum = 0.0;
        for (int k = 0; k < m_buffersize; ++k) {
            sum += fabs(m_input[k]);
        }

        float threshold = m_audioLevel ? signal_threshold_off : signal_threshold_on;
        m_audioLevel = (sum / m_buffersize >= threshold);
        if (!m_audioLevel) {
            if (detectedFrequency != 0)
                detectedFrequency = 0;
            continue;
        }

        // prepare FFT
        memcpy(fftBufferTime, m_input, m_buffersize * sizeof(*fftBufferTime));
        memset(fftBufferTime + m_buffersize, 0, (m_fftSize - m_buffersize) * sizeof(*fftBufferTime));
        fftwf_execute(fftForward);
        for (int k = 1; k < m_fftSize / 2; k++) {
            fftBufferFrequency[k] = (fftBufferFrequency[k] * fftBufferFrequency[k]) +
                                    (fftBufferFrequency[m_fftSize - k] * fftBufferFrequency[m_fftSize - k]);
            fftBufferFrequency[m_fftSize - k] = 0.0;
        }
        fftBufferFrequency[0] = fftBufferFrequency[0] * fftBufferFrequency[0];
        fftBufferFrequency[m_fftSize / 2] = fftBufferFrequency[m_fftSize / 2] * fftBufferFrequency[m_fftSize / 2];

        // execute FFT
        fftwf_execute(fftInverse);

        double sumSq = 2.0 * static_cast<double>(fftBufferTime[0]) / static_cast<double>(m_fftSize);
        for (int k = 0; k < m_fftSize - m_buffersize; k++) {
            fftBufferTime[k] = fftBufferTime[k + 1] / static_cast<float>(m_fftSize);
        }

        // normalise
        int count = (m_buffersize + 1) / 2;
        for (int k = 0; k < count; k++) {
            sumSq -= (m_input[m_buffersize - 1 - k] * m_input[m_buffersize - 1 - k]) + (m_input[k] * m_input[k]);
            // dividing by zero is very slow, so deal with it seperately
            if (sumSq > 0.0) {
                fftBufferTime[k] *= 2.0 / sumSq;
            } else {
                fftBufferTime[k] = 0.0;
            }
        }

        // find frequency
        const float thres = 0.99;
        int maxAutocorrIndex = findsubMaximum(fftBufferTime, count, thres);

        float x = 0.0;
        if (maxAutocorrIndex >= 0) {
            x = parabolaTurningPoint(fftBufferTime[maxAutocorrIndex - 1],
                                 fftBufferTime[maxAutocorrIndex],
                                 fftBufferTime[maxAutocorrIndex + 1],
                                 maxAutocorrIndex + 1);
            x = m_sampleRate / x;

            // precision drops above 1000 Hz
            if (x > 1060.0)
                x = 0.0;
        }
        if (detectedFrequency != x) {
            detectedFrequency = x;
            if (listener) listener->frequencyUpdated();
        }
    }
}