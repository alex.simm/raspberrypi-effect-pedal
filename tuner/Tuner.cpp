#include <cstring>
#include "Tuner.h"

using namespace std;

Tuner::Tuner() : AudioProcessor("Tuner"),
                 fast(BooleanProperty("Fast detection", false)) {
}

void Tuner::init(unsigned int sampleRate, unsigned int maxFrameSize) {
    AudioProcessor::init(sampleRate, maxFrameSize);

    tracker.init(sampleRate);
    tracker.setListener(this);
}

void Tuner::update() {
    tracker.setFastNoteDetection(fast.getValue());
}

void Tuner::process(Packet &packet) {
    tracker.add(packet.numSamples, packet.samples);
}

bool Tuner::isRunning() {
    return tracker.isRunning();
}

void Tuner::start() {
    tracker.start();
}

void Tuner::stop() {
    tracker.stop();
}

void Tuner::frequencyUpdated() {
    if (listener) {
        float frequency = tracker.getEstimatedFrequency();
        converter.setFrequency(frequency);
        listener->frequencyUpdated(frequency, converter.getNote(), converter.getOctave(),
                                   converter.getCents(), converter.getLogCents());
    }
}