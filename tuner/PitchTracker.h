#ifndef EFFECTPEDAL_PITCHTRACKER_H
#define EFFECTPEDAL_PITCHTRACKER_H

#include "LowHighCut.h"
#include "../utils/resampler/Resampler.h"
#include "../utils/Runnable.h"
#include <semaphore.h>
#include <fftw3.h>
#include <thread>

class PitchTracker : public Runnable {
public:
    class Listener {
    public:
        virtual void frequencyUpdated() = 0;
    };

    explicit PitchTracker();

    ~PitchTracker();

    void setListener(Listener *listener);

    void init(int samplerate);

    void stopImpl() override;

    void add(int numSamples, float *input);

    float getEstimatedFrequency();

    void reset();

    void setThreshold(float v);

    float getThreshold();

    void setFastNoteDetection(bool enabled);

private:
    static const unsigned int DOWNSAMPLE = 2;
    static constexpr float SIGNAL_THRESHOLD_ON = 0.001;
    static constexpr float SIGNAL_THRESHOLD_OFF = 0.0009;
    static constexpr float TRACKER_PERIOD = 0.1;
    static const unsigned int FFT_SIZE = 2048;

    float parabolaTurningPoint(float y_1, float y0, float y1, float xOffset);

    int findMaxima(const float *input, int len, int *maxPositions, int *length, int maxLen);

    int findsubMaximum(float *input, int len, float threshold);

    Listener *listener = nullptr;
    LowHighCut low_high_cut;
    volatile bool busy;
    int packetCount;
    sem_t trigger;
    Resampler resamp;
    int m_sampleRate;
    int fixed_sampleRate;
    float detectedFrequency;
    float signal_threshold_on; // Value of the threshold above which the processing is activated.
    float signal_threshold_off; // Value of the threshold below which the input audio signal is deactivated.
    float tracker_period; // Time between frequency estimates (in seconds)
    int m_buffersize; // number of samples in input buffer
    int m_fftSize; // Size of the FFT window.
    float *m_buffer = nullptr;; // The audio buffer that stores the input signal.
    int m_bufferIndex; // Index of the first empty position in the buffer.
    float *m_input = nullptr;; // buffer for input signal
    bool m_audioLevel; // Whether or not the input level is high enough.
    float *fftBufferTime = nullptr, *fftBufferFrequency = nullptr; //time/frequency domain buffers
    fftwf_plan fftForward = nullptr, fftInverse = nullptr; //forward/inverse FFT

    void run() override;

    void copy();
};

#endif //EFFECTPEDAL_PITCHTRACKER_H
