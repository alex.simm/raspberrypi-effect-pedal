#ifndef EFFECTPEDAL_INPUTTHREAD_H
#define EFFECTPEDAL_INPUTTHREAD_H

#include "../utils/Runnable.h"
#include "../ui/input/Input.h"
#include "EventListener.h"

class InputThread : public Runnable {
public:
    InputThread(EventListener *listener);

    ~InputThread();

protected:
    void run() override;

    void stopImpl() override;

private:
    Input *input = nullptr;
    EventListener *listener = nullptr;
};

#endif //EFFECTPEDAL_INPUTTHREAD_H
