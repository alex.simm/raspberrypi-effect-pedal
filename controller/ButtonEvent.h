#ifndef EFFECTPEDAL_BUTTONEVENT_H
#define EFFECTPEDAL_BUTTONEVENT_H

#include "../ui/utils.h"
#include "Event.h"
#include <chrono>

class ButtonEvent : public Event {
public:
    enum class Button : uint {
        /**
         * Used as a return value in case of errors.
         */
                NONE,
        /**
         * Main foot switch.
         */
                MAIN_BUTTON,
        /**
         * OK/forward button.
         */
                OK_BUTTON,
        /**
         * Back/cancel button.
         */
                BACK_BUTTON,
        /**
         * Knob for scrolling.
         */
                SCROLL_KNOB,
        /**
         * Knob for selecting a value.
         */
                VALUE_KNOB
    };

    /**
     * The button that was pressed (button) or turned (knob).
     */
    Button button;
    /**
     * The amount by which the button was turned or pushed: positive or negative for a knob,
     * 1 for a push button.
     */
    int amount;
    /**
     * Indicates whether the event was consumed by an activity.
     */
    bool consumed;

    std::chrono::milliseconds duration;

    std::string additionalInfo;

    ButtonEvent(Button button, int amount, std::chrono::milliseconds duration)
            : button(button), amount(amount), duration(duration), consumed(false), additionalInfo("") {
    }

    ButtonEvent(ButtonEvent &b) = default;

    ButtonEvent(const ButtonEvent &b) = default;

    ButtonEvent &operator=(const ButtonEvent &b) = default;
};

#endif //EFFECTPEDAL_BUTTONEVENT_H
