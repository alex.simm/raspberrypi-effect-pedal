#ifndef EFFECTPEDAL_LOOPERSTATEEVENT_H
#define EFFECTPEDAL_LOOPERSTATEEVENT_H

#include "../recorder/Looper.h"
#include "Event.h"

class LooperStateEvent : public Event {
public:
    /**
     * The new state of the looper.
     */
    Looper::State state;

    LooperStateEvent(Looper::State state) : state(state) {
    }

    LooperStateEvent(LooperStateEvent &b) = default;

    LooperStateEvent(const LooperStateEvent &b) = default;

    LooperStateEvent &operator=(const LooperStateEvent &b) = default;
};

#endif //EFFECTPEDAL_LOOPERSTATEEVENT_H
