#include "Controller.h"
#include "../ui/MainMenuActivity.h"
#include "../ui/TunerActivity.h"
#include "../ui/EffectActivity.h"
#include "../api/line/AudioInputOutput.h"
#include "../api/line/AudioInput.h"
#include "../api/line/AudioOutput.h"
#include "../ui/EffectListActivity.h"
#include "../ui/PresetsActivity.h"
#include "../ui/RecorderActivity.h"
#include "../Logger.h"
#include "../ui/MessageActivity.h"
#include "../ui/input/KeyboardInput.h"
#include "../ui/output/ConsoleOutput.h"
#include "../ui/input/GPIOInput.h"
#include "PlaybackFinishedEvent.h"
#include "../ui/LooperActivity.h"
#include "LooperStateEvent.h"
#include <thread>

using namespace std;

Controller::Controller() : Consumer(-1) {
    // initialise output
    if (GlobalConfig::getInstance().isUsingLCDDisplay())
        output = &DisplayOutput::getInstance();
    else
        output = &ConsoleOutput::getInstance();

    // set initial activity
    changeActivity(ActivityType::ACTIVITY_MAIN_MENU);

    // start line
    currentLineType = LineType::LINE_MODE_PLAYBACK;
    changeLine(LineType::LINE_MODE_NORMAL);
    changeMode(AudioProcessor::Mode::DISABLED);

    // load presets
    try {
        presetManager.loadLastPresets(effectList.getEffects());
    }
    catch (exception &e) {
        Logger::error("Controller: Error while loading last presets: " + string(e.what()));
    }

    player.setListener(this);
    looper.setListener(this);

    inputThread = new InputThread(this);
    inputThread->start();

    // start event queue
    start();
}

void Controller::handleEvent(Event *e) {
    // all incoming events are pushed into the event queue
    push(e);
}

void Controller::consume(Event *e) {
    // consume events from the queue by forwarding them to specific functions
    if (dynamic_cast<ButtonEvent *>(e) != nullptr) {
        consumeButtonEvent(*dynamic_cast<ButtonEvent *>(e));
    } else if (dynamic_cast<PlaybackFinishedEvent *>(e) != nullptr) {
        changeLine(LineType::LINE_MODE_NORMAL);
    } else if (dynamic_cast<LooperStateEvent *>(e) != nullptr) {
        Logger::info("LooperStateEvent!");
        if (currentActivity) currentActivity->update();
    }
}

void Controller::consumeButtonEvent(ButtonEvent event) {
    if (currentActivity != nullptr) {
        ActivityType result = currentActivity->buttonPressed(event);

        // handle unconsumed event
        if (!event.consumed) {
            switch (event.button) {
                case ButtonEvent::Button::BACK_BUTTON:
                    if (!activityStack.empty()) {
                        delete currentActivity;
                        currentActivity = activityStack.top();
                        activityStack.pop();
                    }
                    result = ActivityType::ACTIVITY_NONE;
                    break;
                case ButtonEvent::Button::MAIN_BUTTON:
                    changeMode((line == nullptr) ? AudioProcessor::Mode::DISABLED
                                                 : getNextMode(line->getProcessorMode(), true));
                    result = ActivityType::ACTIVITY_NONE;
                    break;
                default:
                    break;
            }
        }

        // change and update activity
        if (result != ActivityType::ACTIVITY_NONE)
            changeActivity(result, event.additionalInfo);

        currentActivity->update();
    }
}

void Controller::changeActivity(ActivityType type, const string &additionalInfo) {
    if (currentActivity != nullptr) activityStack.push(currentActivity);

    Logger::debug("Controller: changing activity");
    switch (type) {
        case ActivityType::ACTIVITY_RESTART_PROGRAM:
            finish();
            break;
        case ActivityType::ACTIVITY_MAIN_MENU:
            currentActivity = new MainMenuActivity(output);
            break;
        case ActivityType::ACTIVITY_TUNER:
            currentActivity = new TunerActivity(output, &tuner);
            break;
        case ActivityType::ACTIVITY_PRESETS:
            currentActivity = new PresetsActivity(output, &presetManager, &effectList);
            break;
        case ActivityType::ACTIVITY_EFFECT_LIST:
            currentActivity = new EffectListActivity(output, &effectList);
            break;
        case ActivityType::ACTIVITY_EFFECT: {
            AudioProcessor *effect = effectList.getEffect(additionalInfo);
            if (effect != nullptr)
                currentActivity = new EffectActivity(output, effect);
            else
                Logger::error("Controller: Effect '" + additionalInfo + "' not found!");
            break;
        }
        case ActivityType::ACTIVITY_RECORDER:
            player.setAddSamples(false);
            currentActivity = new RecorderActivity(output, &player, &recorder);
            break;
        case ActivityType::ACTIVITY_LOOPER:
            currentActivity = new LooperActivity(output, &looper);
            changeLine(LineType::LINE_MODE_LOOPER);
            break;
        default:
            break;
    }

    currentActivity->setListener(this);
    currentActivity->update();
}

void Controller::changeMode(AudioProcessor::Mode mode) {
    if (line != nullptr) {
        line->setProcessorMode(mode);
        LEDStatus::getInstance().displayProcessorMode(mode);
        Logger::debug("Controller: new mode: " + toString(mode));
    }
}

void Controller::stopLine() {
    if (line != nullptr) {
        Logger::debug("Controller: stopping line");
        try {
            line->stop();
        }
        catch (exception &e) {
            Logger::debug("Controller: error while stopping line: " + string(e.what()));
        }

        delete line;
        line = nullptr;
    }
}

void Controller::changeLine(LineType lineType) {
    if (lineType != currentLineType) {
        // stop old line
        AudioProcessor::Mode mode = (line == nullptr) ? AudioProcessor::Mode::DISABLED : line->getProcessorMode();
        stopLine();

        // create new line
        switch (lineType) {
            case LineType::LINE_MODE_TUNER: {
                Logger::debug("Controller: starting line 'tuner'");
                line = new AudioInput(vector<AudioProcessor *>(), &tuner);
                break;
            }
            case LineType::LINE_MODE_RECORDER: {
                Logger::debug("Controller: starting line 'recorder'");
                line = new AudioInputOutput(effectList.getEffects(), nullptr, &recorder);
                break;
            }
            case LineType::LINE_MODE_PLAYBACK: {
                Logger::debug("Controller: starting line 'playback'");
                line = new AudioInputOutput(effectList.getEffects(), nullptr, &player);
                break;
            }
            case LineType::LINE_MODE_RECORDER_PLAYBACK: {
                Logger::debug("Controller: starting line 'playback'");
                line = new AudioInputOutput(effectList.getEffects(), vector<AudioProcessor *>(), {&player, &recorder});
                break;
            }
            case LineType::LINE_MODE_LOOPER: {
                Logger::debug("Controller: starting line 'looper'");
                line = new AudioInputOutput(effectList.getEffects(), nullptr, &looper);
                break;
            }
            case LineType::LINE_MODE_NORMAL:
            default: {
                Logger::debug("Controller: starting line 'normal'");
                line = new AudioInputOutput(effectList.getEffects());
                break;
            }
        }

        // replace and start line
        line->setProcessorMode(mode);
        line->start();
        currentLineType = lineType;
        currentActivity->update();
    }
}

void Controller::showMessage(std::vector<std::string> lines, unsigned int seconds) {
    activityStack.push(currentActivity);

    MessageActivity ma(output, lines);
    currentActivity = &ma;
    currentActivity->update();

    std::this_thread::sleep_for(std::chrono::seconds(seconds));

    currentActivity = activityStack.top();
    activityStack.pop();
    currentActivity->update();
}

void Controller::saveLastPreset() {
    presetManager.storeLastPresets(effectList.getEffects());
}

void Controller::finish() {
    // TODO: EventQueue hier anhalten fuehrt zu "std::system_error: Resource deadlock avoided" -> mutual thread join
    //Logger::debug("Controller: stopping event queue");
    //stop();

    // stop line
    Logger::debug("Controller: stopping line");
    stopLine();
    delete line;
    line = nullptr;

    // stop IO
    Logger::debug("Controller: stopping input thread");
    inputThread->stop();
    delete inputThread;
    inputThread = nullptr;

    Logger::debug("Controller: stopping output");
    output->clear();
    output->close();
    output = nullptr;

    // stop activity
    Logger::debug("Controller: deleting activities");
    delete currentActivity;
    currentActivity = nullptr;

    // clear activity stack
    while (!activityStack.empty()) {
        //Activity *a = activityStack.top();
        //delete a;
        activityStack.pop();
    }

    Logger::debug("Controller: exit");
    quick_exit(0);
}

std::string Controller::toString(AudioProcessor::Mode mode) {
    switch (mode) {
        case AudioProcessor::Mode::DISABLED:
            return "disabled";
        case AudioProcessor::Mode::LEVEL_ONE:
            return "level 1";
        case AudioProcessor::Mode::LEVEL_TWO:
            return "level 2";
        default: //BOTH
            return "both";
    }
}