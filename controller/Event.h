#ifndef EFFECTPEDAL_EVENT_H
#define EFFECTPEDAL_EVENT_H

class Event {
public:
    virtual ~Event() = default;
};

#endif //EFFECTPEDAL_EVENT_H
