#include "InputThread.h"
#include "ButtonEvent.h"
#include "../ui/input/KeyboardInput.h"
#include "../ui/input/GPIOInput.h"
#include "../GlobalConfig.h"
#include "../Logger.h"

using namespace std;

InputThread::InputThread(EventListener *listener) : Runnable(), input(input), listener(listener) {
    if (GlobalConfig::getInstance().isUsingButtonInput())
        input = &GPIOInput::getInstance();
    else
        input = &KeyboardInput::getInstance();
}

InputThread::~InputThread() {
    input = nullptr;
}

void InputThread::stopImpl() {
    if (input != nullptr) {
        input->close();
        input = nullptr;
    }
}

void InputThread::run() {
    while (running) {
        ButtonEvent event = input->readEvent();
        listener->handleEvent(new ButtonEvent(event));
    }
}
