#ifndef EFFECTPEDAL_CONTROLLER_H
#define EFFECTPEDAL_CONTROLLER_H

#include <thread>
#include <stack>
#include "../api/line/AudioLine.h"
#include "../effects/EffectList.h"
#include "../presets/PresetManager.h"
#include "../recorder/Recorder.h"
#include "../recorder/Player.h"
#include "../tuner/Tuner.h"
#include "../ui/Activity.h"
#include "../ui/utils.h"
#include "../ui/input/Input.h"
#include "../ui/output/LEDStatus.h"
#include "../utils/Runnable.h"
#include "InputThread.h"
#include "PlaybackFinishedEvent.h"
#include "../recorder/Looper.h"

class Controller : Consumer<Event>, ActivityListener, EventListener {
public:
    Controller();

    void showMessage(std::vector<std::string> lines, unsigned int seconds) override;

    void changeLine(LineType lineType) override;

    void saveLastPreset() override;

    /**
     * Handles all asynchronously occuring events (input, playback finished) via a queued thread.
     */
    void handleEvent(Event *e) override;

protected:
    void consume(Event *event);

private:
    // data/model
    EffectList effectList;
    PresetManager presetManager;
    Tuner tuner;
    Player player;
    Recorder recorder;
    Looper looper;

    // audio
    AudioLine *line = nullptr;
    LineType currentLineType;

    //ui
    Output *output = nullptr;
    InputThread *inputThread = nullptr;
    Activity *currentActivity = nullptr;
    std::stack<Activity *> activityStack;

    void consumeButtonEvent(ButtonEvent e);

    /**
    * Stops the current line and releases all resources.
    */
    void stopLine();

    void changeActivity(ActivityType type, const std::string &additionalInfo = "");

    /**
     * Changes the mode of the current line to the value.
     * @see AudioLine::setProcessorMode
     */
    void changeMode(AudioProcessor::Mode mode);

    /**
     * Closes all resources and stops the program.
     */
    void finish();

    std::string toString(AudioProcessor::Mode mode);
};


#endif //EFFECTPEDAL_CONTROLLER_H
