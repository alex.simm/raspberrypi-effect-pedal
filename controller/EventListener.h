#ifndef EFFECTPEDAL_EVENTLISTENER_H
#define EFFECTPEDAL_EVENTLISTENER_H

#include "Event.h"

class EventListener {
public:
    virtual void handleEvent(Event *e) = 0;
};

#endif //EFFECTPEDAL_EVENTLISTENER_H
