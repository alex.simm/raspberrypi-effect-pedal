#include "FileReader.h"

FileReader::FileReader() {
}

FileReader::~FileReader() {
}

void FileReader::setListener(Listener *listener) {
    this->listener = listener;
}

void FileReader::notifyListener() {
    if (listener != nullptr) listener->fileFinished();
}