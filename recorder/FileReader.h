#ifndef EFFECTPEDAL_FILEREADER_H
#define EFFECTPEDAL_FILEREADER_H

#include <string>

class FileReader {
public:
    class Listener {
    public:
        virtual void fileFinished() = 0;
    };

    FileReader();

    virtual ~FileReader();

    virtual void init(unsigned int sampleRate, unsigned int frameSize, const std::string &filename) = 0;

    virtual void read(float *samples, unsigned int numSamples) = 0;

    virtual void close() = 0;

    void setListener(Listener *listener);

protected:
    void notifyListener();

private:
    Listener *listener = nullptr;
};

#endif //EFFECTPEDAL_FILEREADER_H
