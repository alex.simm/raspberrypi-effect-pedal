#ifndef EFFECTPEDAL_RECORDER_H
#define EFFECTPEDAL_RECORDER_H

#include "../api/AudioProcessor.h"
#include "FileWriter.h"
#include <experimental/filesystem>

class Recorder : public AudioProcessor {

public:
    Recorder();

    ~Recorder();

    Recorder &operator=(const Recorder &r);

    std::experimental::filesystem::path getOutputFile() const;

    void setOutputFile(std::experimental::filesystem::path file);

    void init(unsigned int sampleRate, unsigned int maxFrameSize) override;

    void process(Packet &packet) override;

    void stop() override;

    bool isRunning();

private:
    std::experimental::filesystem::path file;
    FileWriter *writer = nullptr;
};

#endif //EFFECTPEDAL_RECORDER_H
