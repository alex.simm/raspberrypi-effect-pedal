#include "Recorder.h"
#include "wav/WAVWriter.h"
#include <stdexcept>
#include "../Logger.h"

using namespace std;
using namespace std::experimental::filesystem;

Recorder::Recorder() : AudioProcessor("Recorder") {
    mode = Mode::BOTH;
}

Recorder::~Recorder() {
    stop();
}

Recorder &Recorder::operator=(const Recorder &r) {
    file = r.file;
    writer = r.writer;

    return *this;
}

std::experimental::filesystem::path Recorder::getOutputFile() const {
    return file;
}

void Recorder::setOutputFile(path file) {
    this->file = file;
}

void Recorder::init(unsigned int sampleRate, unsigned int maxFrameSize) {
    if (writer == nullptr) {
        AudioProcessor::init(sampleRate, maxFrameSize);

        writer = new WAVWriter();
        writer->init(sampleRate, maxFrameSize, file.u8string());
    }
}

void Recorder::process(Packet &packet) {
    writer->write(packet.samples, packet.numSamples);
}

void Recorder::stop() {
    if (writer != nullptr) {
        writer->close();
        writer = nullptr;
    }
}

bool Recorder::isRunning() {
    return writer != nullptr;
}