#ifndef EFFECTPEDAL_PLAYER_H
#define EFFECTPEDAL_PLAYER_H

#include "../api/AudioProcessor.h"
#include "../controller/EventListener.h"
#include "FileReader.h"
#include <experimental/filesystem>

class Player : public AudioProcessor, FileReader::Listener {

public:
    Player();

    ~Player();

    Player &operator=(const Player &r);

    void setInputFile(std::experimental::filesystem::path file);

    void init(unsigned int sampleRate, unsigned int maxFrameSize) override;

    void process(Packet &packet) override;

    void stop() override;

    bool isRunning();

    void fileFinished() override;

    void setListener(EventListener *listener);

    bool isAddingSamples() const;

    void setAddSamples(bool addSamples);

private:
    EventListener *listener = nullptr;
    std::experimental::filesystem::path file;
    bool running = false;
    FileReader *reader = nullptr;

    // add-samples mode
    bool addSamples = false;
    float *buffer = nullptr;
};

#endif //EFFECTPEDAL_PLAYER_H
