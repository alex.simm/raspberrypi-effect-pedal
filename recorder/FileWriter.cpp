#include "FileWriter.h"
#include "../Logger.h"
#include <cstring>
#include <string>

using namespace std;

FileWriter::FileWriter() : Consumer(-1) {
}

FileWriter::~FileWriter() {
    close();
}

void FileWriter::init(unsigned int sampleRate, unsigned int frameSize, const std::string &filename) {
    this->numSamples = frameSize;
    start();
}

void FileWriter::write(float *samples, unsigned int numSamples) {
    auto buf = new float[numSamples];
    memcpy(buf, samples, numSamples * sizeof(float));
    push(buf);
}

void FileWriter::close() {
    stop();
}