#include "Player.h"
#include "wav/WAVReader.h"
#include "../controller/PlaybackFinishedEvent.h"
#include <stdexcept>

using namespace std;
using namespace std::experimental::filesystem;

Player::Player() : AudioProcessor("Player") {
}

Player::~Player() {
    stop();
}

Player &Player::operator=(const Player &r) {
    file = r.file;
    reader = r.reader;
    running = r.running;

    return *this;
}

void Player::setInputFile(path file) {
    this->file = file;
}

void Player::init(unsigned int sampleRate, unsigned int maxFrameSize) {
    if (!running && reader == nullptr) {
        AudioProcessor::init(sampleRate, maxFrameSize);

        reader = new WAVReader();
        reader->setListener(this);
        reader->init(sampleRate, maxFrameSize, file.u8string());

        delete buffer;
        buffer = new float[maxFrameSize];

        running = true;
    }
}

void Player::process(Packet &packet) {
    if (addSamples) {
        reader->read(buffer, packet.numSamples);
        for (int i = 0; i < packet.numSamples; ++i) {
            packet.samples[i] += buffer[i];
        }
    } else reader->read(packet.samples, packet.numSamples);
}

void Player::stop() {
    if (running && reader != nullptr) {
        reader->close();
        reader = nullptr;
        running = false;
    }
}

bool Player::isRunning() {
    return running;
}

void Player::fileFinished() {
    listener->handleEvent(new PlaybackFinishedEvent());
}

void Player::setListener(EventListener *listener) {
    this->listener = listener;
}

bool Player::isAddingSamples() const {
    return addSamples;
}

void Player::setAddSamples(const bool addSamples) {
    this->addSamples = addSamples;
}