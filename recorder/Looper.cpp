#include <cstring>
#include <cmath>
#include "Looper.h"
#include "../Logger.h"
#include "../controller/LooperStateEvent.h"

using namespace std;

//TODO: unterschiedlich lange Dubs unterstuetzen
Looper::Looper() : AudioProcessor("Looper") {
}

Looper::~Looper() {
    stop();

    delete[] m_storage1;
}

void Looper::init(unsigned int sampleRate, unsigned int maxFrameSize) {
    AudioProcessor::init(sampleRate, maxFrameSize);

    m_storageSize = sampleRate * STORAGE_MEMORY_SECONDS * 2;
    delete m_storage1;
    m_storage1 = new float[m_storageSize];

    levelDetector.setSampleRate(sampleRate);
}

void Looper::update() {
}

void Looper::stop() {
    reset();
}

void Looper::process(Packet &packet) {
    m_now += double(packet.numSamples) / sampleRate;
    if (m_state == LOOPER_STATE_INACTIVE) {
        for (uint32_t s = 0; s < packet.numSamples; ++s) {
            packet.samples[s] *= m_dryAmount;
        }
        return;
    }

    //TODO: Messung in AudioLine verwenden
    levelDetector.process(packet.samples, packet.numSamples);

    // Check if we reached the threshold to start recording.
    if (m_state == LOOPER_STATE_WAITING_FOR_THRESHOLD && (levelDetector.getMeanLevel() >= packet.meanLevel)) {
        Dub &dub = m_dubs[m_nrOfDubs];
        dub.m_startIndex = m_currentLoopIndex;
        m_state = LOOPER_STATE_RECORDING;
        if (listener) listener->handleEvent(new LooperStateEvent(m_state));
    }

    for (uint32_t s = 0; s < packet.numSamples; ++s) {
        // Use the live input
        float in1 = packet.samples[s];

        // If we are recoding do the record.
        if (m_state == LOOPER_STATE_RECORDING) {
            m_storage1[m_nrOfUsedSamples] = in1;
            m_nrOfUsedSamples++;
            Dub &dub = m_dubs[m_nrOfDubs];
            dub.m_length++;
        }

        // Playback all active dubs.
        float out1 = m_dryAmount * in1;
        for (size_t t = 0; t < m_nrOfDubs; t++) {
            Dub &dub = m_dubs[t];
            if (m_currentLoopIndex < dub.m_startIndex)
                continue;
            if (m_currentLoopIndex >= dub.m_startIndex + dub.m_length)
                continue;
            size_t index = dub.m_storageOffset + (m_currentLoopIndex - dub.m_startIndex);
            out1 += m_storage1[index];
        }

        // Store accumulated output.
        packet.samples[s] = out1;

        if (m_nrOfDubs > 0)
            // Only once we are actually playing anything the loop length is known.
            m_currentLoopIndex++;

        // At the end increment the loop index and check if we are at the end
        // of the loop. The first dub governs the length of the whole loop.
        // So if still recording when we reach the end of the loop, we stop
        // the recording! Note that if we don't have a dub, yet, then m_loopLength
        // is 0, so no extra check is needed.
        if (m_currentLoopIndex > m_loopLength || m_nrOfUsedSamples >= m_storageSize) {
            // Reached the end of the loop, either because we exhausted storage
            // or the end of the loop is there.
            m_currentLoopIndex = 0;

            if (m_state == LOOPER_STATE_RECORDING)
                // Stop the recording only, if we did not have the threshold, yet.
                // That allows to start recording right at the start of the loop.
                stopRecording();
        }
    }
}

void Looper::reset() {
    stopRecording();

    m_nrOfDubs = 0;
    m_maxUsedDubs = 0;
    m_nrOfUsedSamples = 0;
    m_state = LOOPER_STATE_INACTIVE;
    m_currentLoopIndex = 0;
    m_loopLength = 0;
    m_nrOfUsedSamples = 0;
}

Looper::State Looper::getState() const {
    return m_state;
}

unsigned int Looper::numStreams() const {
    return m_nrOfDubs;
}

void Looper::setListener(EventListener *listener) {
    this->listener = listener;
}

void Looper::startRecording() {
    if (m_nrOfDubs >= NR_OF_DUBS)
        // Reached maximum number of dubs, cannot start recording.
        return;
    if (m_nrOfUsedSamples >= m_storageSize)
        // Memory full, cannot start recording.
        return;

    // Prepare the dub.
    Dub &dub = m_dubs[m_nrOfDubs];
    dub.m_storageOffset = m_nrOfUsedSamples;
    dub.m_length = 0;

    // Now start the recording.
    m_state = LOOPER_STATE_WAITING_FOR_THRESHOLD;
    if (listener) listener->handleEvent(new LooperStateEvent(m_state));
}

/// Finish the recording.
void Looper::stopRecording() {
    if (m_state == LOOPER_STATE_WAITING_FOR_THRESHOLD) {
        // We did not actually record anything, yet. So nothing to do. Just
        // go back to the previous state.
        if (m_nrOfDubs == 0)
            m_state = LOOPER_STATE_INACTIVE;
        else
            m_state = LOOPER_STATE_PLAYING;

        if (listener) listener->handleEvent(new LooperStateEvent(m_state));
        return;
    }

    // We did record something, so make sure we will use it.
    m_state = LOOPER_STATE_PLAYING;
    if (listener) listener->handleEvent(new LooperStateEvent(m_state));
    Dub &dub = m_dubs[m_nrOfDubs];
    if (m_nrOfDubs == 0) {
        // This was the first dub which governs the loop length.
        m_loopLength = dub.m_length;
        m_currentLoopIndex = 0;
    }

    // Fixup the start and the end of the loop. We simply fade in and out over
    // 32 samples for now. Not sure if that's good for everything, seems to work
    // nicely enough, though.
    size_t length = dub.m_length > NR_OF_BLEND_SAMPLES ? NR_OF_BLEND_SAMPLES : dub.m_length;
    size_t startIndex = dub.m_storageOffset;
    size_t endIndex = dub.m_storageOffset + dub.m_length - 1;
    for (size_t s = 0; s < length; s++) {
        float factor = float(s) / length;
        m_storage1[startIndex] *= factor;
        startIndex++;
        m_storage1[endIndex] *= factor;
        endIndex--;
    }

    // Now the dub is officially ready for playing...
    m_nrOfDubs++;

    // Note that when recording a new dub we need to reset max dubs as well, even if
    // once had more dubs: They have been overwritten and cannot be redone!
    m_maxUsedDubs = m_nrOfDubs;
}
