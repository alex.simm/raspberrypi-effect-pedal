#include "RecorderList.h"
#include "../GlobalConfig.h"
#include <algorithm>
#include <ctime>
#include <iomanip>

using namespace std;
using namespace std::experimental::filesystem;

RecorderList::RecorderList() : path(GlobalConfig::getInstance().getMainDirectory() + DIRECTORY) {
    update();
}

void RecorderList::update() {
    entries.clear();

    std::experimental::filesystem::create_directories(path);
    directory_iterator start(path);
    directory_iterator end;
    transform(start, end, std::back_inserter(entries),
              [](directory_entry entry) -> string {
                  string s = entry.path().filename();
                  size_t idx = s.find_last_of('.');
                  s = s.substr(0, idx);
                  return s;
              });

    sort(entries.begin(), entries.end(), [](string s1, string s2) { return s1.compare(s2) < 0; });
}

const vector<string> &RecorderList::getEntries() {
    return entries;
}

path RecorderList::getPath(string entry) {
    return u8path(path + "/" + entry + "." + GlobalConfig::getInstance().getRecordingFormat());
}

path RecorderList::createFile() {
    std::time_t t = std::time(nullptr);
    std::tm tm = *std::localtime(&t);

    stringstream buffer;
    buffer << std::put_time(&tm, "%d.%m.%Y %H:%M:%S");
    return getPath(buffer.str());
}