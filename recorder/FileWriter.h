#ifndef EFFECTPEDAL_FILEWRITER_H
#define EFFECTPEDAL_FILEWRITER_H

#include "../utils/Runnable.h"
#include "../utils/BlockingQueue.h"
#include "../utils/Consumer.h"

class FileWriter : Consumer<float> {
public:
    FileWriter();

    virtual ~FileWriter();

    virtual void init(unsigned int sampleRate, unsigned int frameSize, const std::string &filename);

    void write(float *samples, unsigned int numSamples);

    void close();

protected:
    unsigned int numSamples;
};


#endif //EFFECTPEDAL_FILEWRITER_H
