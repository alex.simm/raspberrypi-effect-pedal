#ifndef EFFECTPEDAL_LOOPER_H
#define EFFECTPEDAL_LOOPER_H

#include "../api/AudioProcessor.h"
#include "../utils/analysis/LevelDetector.h"
#include "../controller/EventListener.h"

class Looper : public AudioProcessor {
public:
    enum State {
        LOOPER_STATE_INACTIVE, //no active dub, not recording
        LOOPER_STATE_WAITING_FOR_THRESHOLD, //recording has started, but audio did not exceed threshold yet
        LOOPER_STATE_RECORDING, //recording
        LOOPER_STATE_PLAYING //playing all active dubs
    };

    Looper();

    ~Looper() override;

    void init(unsigned int sampleRate, unsigned int maxFrameSize) override;

    void update() override;

    void process(Packet &packet) override;

    void stop() override;

    /**
     * Starts recording of a new stream.
     */
    void startRecording();

    /**
     * Stops the current recording, if any.
     */
    void stopRecording();

    /**
     * Resets the looper by stopping the recording and clearing all recorded streams-
     */
    void reset();

    State getState() const;

    /**
     * Returns the number of recorded streams not including the currently recording one.
     * @return
     */
    unsigned int numStreams() const;

    void setListener(EventListener *listener);

private:
    class Dub {
    public:
        //Where is the dub's audio memory starting in the global audio storage?
        size_t m_storageOffset = 0;
        /// The length of the dub. Each dub can have an individual length, but they
        /// will still stay in sync!
        size_t m_length = 0;
        /// The start index in the loop. This allows to save the memory before there
        /// is actual audio in the loop. A dub only needs the memory between the
        /// first and the last audio saved in the dub.
        size_t m_startIndex = 0;
    };

    // The maximum number of dubs that can be recorded
    static const size_t NR_OF_DUBS = 128;
    /**
     * The maximum number of seconds which can be recorded for all dubs. Note that each dub can have an individual
     * length. If audio starts after the loop start and/or finishes before the end of the loop it will consume less
     * memory.
     */
    static const size_t STORAGE_MEMORY_SECONDS = 360;
    static const size_t NR_OF_BLEND_SAMPLES = 64;

    // Internal state
    EventListener *listener = nullptr;
    State m_state = LOOPER_STATE_INACTIVE;
    float m_dryAmount = 1.0f; //stored dry amount
    size_t m_currentLoopIndex = 0; //Where are we with the first (main) loop. The first loop governs all the loops!
    size_t m_loopLength = 0; //length of the main loop
    double m_now = 0; //current time, sample accurate used for buttons

    // Storage memory for audio
    size_t m_storageSize = 0; //Overall storage size for audio (number of floats per channel)
    size_t m_nrOfUsedSamples = 0; //Number of samples already used
    float *m_storage1 = nullptr; //Storage for first channel

    // Store information about the dubs
    size_t m_nrOfDubs = 0; //number of currently active dubs
    size_t m_maxUsedDubs = 0; //number of dubs which we recorded and thus could be redone
    Dub m_dubs[NR_OF_DUBS];

    LevelDetector levelDetector;
};


#endif //EFFECTPEDAL_LOOPER_H
