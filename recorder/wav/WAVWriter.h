#ifndef EFFECTPEDAL_WAVWRITER_H
#define EFFECTPEDAL_WAVWRITER_H

#include "../FileWriter.h"
#include "File.h"

class WAVWriter : public FileWriter {
public:
    WAVWriter();

    ~WAVWriter();

    void init(unsigned int sampleRate, unsigned int frameSize, const std::string &filename) override;

protected:
    void consume(float *f) override;

private:
    File file;
};


#endif //EFFECTPEDAL_WAVWRITER_H
