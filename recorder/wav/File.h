#ifndef WAVE_WAVE_FILE_H_
#define WAVE_WAVE_FILE_H_

#include <string>
#include <ios>
#include <vector>
#include <system_error>
#include <memory>
#include <stdint.h>

enum OpenMode {
    kIn, kOut
};

class File {
public:
    File();

    ~File();

    /**
     * @brief Open wave file at given path
     */
    void Open(const std::string &path, OpenMode mode);

    /**
     * @brief Read the given number of frames from file.
     * @note: File has to be opened in kOut mode or kNotOpen will be returned.
     * If file is too small, kInvalidFormat is returned
     */
    bool Read(float *output, unsigned int numSamples);

    /**
     * @brief Write the given data
     * @note: File has to be opened in kIn mode or kNotOpen will be returned.
     */
    void Write(const float *data, unsigned int numSamples);

    /**
     * Move to the given frame in the file
     */
    void Seek(uint64_t frame_index);

    /**
     * Give the current frame position in the file
     */
    uint64_t Tell() const;

    uint16_t channel_number() const;

    void set_channel_number(uint16_t channel_number);

    uint32_t sample_rate() const;

    void set_sample_rate(uint32_t sample_rate);

    uint16_t bits_per_sample() const;

    void set_bits_per_sample(uint16_t bits_per_sample);

    uint64_t frame_number() const;

private:
    class Impl;

    Impl *impl_;
};

#endif  // WAVE_WAVE_FILE_H_
