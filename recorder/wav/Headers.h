#ifndef EFFECTPEDAL_HEADERS_H
#define EFFECTPEDAL_HEADERS_H

#include <cstdint>
#include <cstring>

struct DataHeader {
    char sub_chunk_2_id[4];
    uint32_t sub_chunk_2_size;
};

DataHeader MakeDataHeader();

struct FMTHeader {
    char sub_chunk_1_id[4];
    uint32_t sub_chunk_1_size;
    uint16_t audio_format;
    uint16_t num_channel;
    uint32_t sample_rate;
    uint32_t byte_rate;
    uint16_t byte_per_block;
    uint16_t bits_per_sample;
};

FMTHeader MakeFMTHeader();

struct RIFFHeader {
    char chunk_id[4];
    uint32_t chunk_size;
    char format[4];
};

RIFFHeader MakeRIFFHeader();

struct WAVEHeader {
    RIFFHeader riff;
    FMTHeader fmt;
    DataHeader data;
};

WAVEHeader MakeWAVEHeader();

#endif //EFFECTPEDAL_HEADERS_H
