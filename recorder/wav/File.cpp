#include "File.h"
#include <fstream>
#include <cstring>
#include <limits>
#include <stdexcept>
#include "HeaderList.h"
#include "Headers.h"

using namespace std;

#define INT24_MAX 8388607

namespace internal {
    void NoEncrypt(char *data, size_t size) {}

    void NoDecrypt(char *data, size_t size) {}
}  // namespace internal

enum Format {
    WAVE_FORMAT_PCM = 0x0001,
    WAVE_FORMAT_IEEE_FLOAT = 0x0003,
    WAVE_FORMAT_ALAW = 0x0006,
    WAVE_FORMAT_MULAW = 0x0007,
    WAVE_FORMAT_EXTENSIBLE = 0xFFFE
};

class File::Impl {
public:
    void WriteHeader(uint64_t data_size) {
        if (!ostream.is_open())
            throw runtime_error("WAV file not open (in writeHeader)");

        auto original_position = ostream.tellp();
        // Position to beginning of file
        ostream.seekp(0);

        // make header
        auto bits_per_sample = header.fmt.bits_per_sample;
        auto bytes_per_sample = bits_per_sample / 8;
        auto channel_number = header.fmt.num_channel;
        auto sample_rate = header.fmt.sample_rate;

        header.riff.chunk_size =
                sizeof(WAVEHeader) + (data_size * (bits_per_sample / 8)) - 8;
        // fmt header
        header.fmt.byte_per_block = (uint16_t) (bytes_per_sample * channel_number);
        header.fmt.byte_rate = sample_rate * header.fmt.byte_per_block;
        // data header
        header.data.sub_chunk_2_size = (uint32_t) (data_size * bytes_per_sample);

        ostream.write(reinterpret_cast<char *>(&header), sizeof(WAVEHeader));
        if (ostream.fail())
            throw runtime_error("Write error in WAV file");

        // reposition to old position if was > to current position
        if (ostream.tellp() < original_position) {
            ostream.seekp(original_position);
        }

        // the offset of data will be right after the headers
        data_offset_ = sizeof(WAVEHeader);
    }

    template<typename T>
    void ReadHeader(Header generic_header, T *output) {
        istream.seekg(generic_header.position(), std::ios::beg);
        istream.read(reinterpret_cast<char *>(output), sizeof(T));
    }

    void ReadHeader(HeaderList *headers) {
        if (!istream.is_open())
            throw runtime_error("WAV file is not open");

        istream.seekg(0, std::ios::end);
        auto file_size = istream.tellg();
        // If not enough data
        if (file_size < sizeof(WAVEHeader))
            throw runtime_error("Invalid WAV format: not enough data");
        istream.seekg(0, std::ios::beg);

        // read headers
        ReadHeader(headers->riff(), &header.riff);
        ReadHeader(headers->fmt(), &header.fmt);
        ReadHeader(headers->data(), &header.data);
        // data offset is right after data header's ID and size
        auto data_header = headers->data();
        data_offset_ = data_header.position() + sizeof(data_header.chunk_size()) +
                       (data_header.chunk_id().size() * sizeof(char));

        // check headers ids (make sure they are set)
        if (std::string(header.riff.chunk_id, 4) != "RIFF")
            throw runtime_error("Invalid WAV format: corrupt RIFF header");
        if (std::string(header.riff.format, 4) != "WAVE")
            throw runtime_error("Invalid WAV format: corrupt WAVE header");
        if (std::string(header.fmt.sub_chunk_1_id, 4) != "fmt ")
            throw runtime_error("Invalid WAV format: corrupt FMT header");
        if (std::string(header.data.sub_chunk_2_id, 4) != "data")
            throw runtime_error("Invalid WAV format: corrupt DATA header");

        // we only support 8 / 16 / 32  bit per sample
        auto bps = header.fmt.bits_per_sample;
        if (bps != 8 && bps != 16 && bps != 24 && bps != 32)
            throw runtime_error("Invalid WAV format: invalid bit size: " + to_string(bps));

        // And only support uncompressed PCM format
        if (header.fmt.audio_format != Format::WAVE_FORMAT_PCM)
            throw runtime_error("Invalid WAV format: no PCM");
    }

    uint64_t current_sample_index() {
        auto bits_per_sample = header.fmt.bits_per_sample;
        auto bytes_per_sample = bits_per_sample / 8;
        uint64_t data_index = 0;
        if (ostream.is_open()) {
            data_index = static_cast<uint64_t>(ostream.tellp()) - data_offset_;
        } else if (istream.is_open()) {
            data_index = static_cast<uint64_t>(istream.tellg()) - data_offset_;
        } else {
            return 0;
        }
        return data_index / bytes_per_sample;
    }

    void set_current_sample_index(uint64_t sample_idx) {
        auto bits_per_sample = header.fmt.bits_per_sample;
        auto bytes_per_sample = bits_per_sample / 8;

        std::streampos stream_index =
                data_offset_ + (sample_idx * bytes_per_sample);
        if (ostream.is_open()) {
            ostream.seekp(stream_index);
        } else if (istream.is_open()) {
            istream.seekg(stream_index);
        }
    }

    uint64_t sample_number() {
        auto bits_per_sample = header.fmt.bits_per_sample;
        auto bytes_per_sample = bits_per_sample / 8;

        auto total_data_size = header.data.sub_chunk_2_size;
        return total_data_size / bytes_per_sample;
    }

    std::ifstream istream;
    std::ofstream ostream;
    WAVEHeader header;
    uint64_t data_offset_;
};

File::File() : impl_(new Impl()) { impl_->header = MakeWAVEHeader(); }

File::~File() {
    if (impl_->istream.is_open()) {
        impl_->ostream.flush();
    }
    delete impl_;
}

void File::Open(const std::string &path, OpenMode mode) {
    if (mode == OpenMode::kOut) {
        impl_->ostream.open(path.c_str(), std::ios::binary | std::ios::trunc);
        if (!impl_->ostream.is_open())
            throw runtime_error("Failed to open WAV file");
        return impl_->WriteHeader(0);
    }

    impl_->istream.open(path.c_str(), std::ios::binary);
    if (!impl_->istream.is_open())
        throw runtime_error("Failed to open WAV file");

    HeaderList headers;
    headers.Init(path);
    return impl_->ReadHeader(&headers);
}

uint16_t File::channel_number() const { return impl_->header.fmt.num_channel; }

void File::set_channel_number(uint16_t channel_number) {
    impl_->header.fmt.num_channel = channel_number;
}

uint32_t File::sample_rate() const { return impl_->header.fmt.sample_rate; }

void File::set_sample_rate(uint32_t sample_rate) {
    impl_->header.fmt.sample_rate = sample_rate;
}

uint16_t File::bits_per_sample() const {
    return impl_->header.fmt.bits_per_sample;
}

void File::set_bits_per_sample(uint16_t bits_per_sample) {
    impl_->header.fmt.bits_per_sample = bits_per_sample;
}

uint64_t File::frame_number() const {
    return impl_->sample_number() / channel_number();
}

bool File::Read(float *output, unsigned int numSamples) {
    if (!impl_->istream.is_open())
        throw runtime_error("WAV file not open (in read)");
    auto requested_samples = numSamples * channel_number();

    // check if we have enough data available
    if (impl_->sample_number() <
        requested_samples + impl_->current_sample_index())
        return false;

    // read every sample one after another
    for (unsigned int sample_idx = 0; sample_idx < numSamples; sample_idx++) {
        if (impl_->header.fmt.bits_per_sample == 8) {
            int8_t value;
            impl_->istream.read(reinterpret_cast<char *>(&value), sizeof(value));
            if (impl_->istream.eof())
                return false;
            output[sample_idx] = static_cast<float>(value) / std::numeric_limits<int8_t>::max();
        } else if (impl_->header.fmt.bits_per_sample == 16) {
            int16_t value;
            impl_->istream.read(reinterpret_cast<char *>(&value), sizeof(value));
            if (impl_->istream.eof())
                return false;
            output[sample_idx] = static_cast<float>(value) / std::numeric_limits<int16_t>::max();
        } else if (impl_->header.fmt.bits_per_sample == 24) {
            unsigned char value[3];
            impl_->istream.read(reinterpret_cast<char *>(&value), sizeof(value));
            if (impl_->istream.eof())
                return false;

            int integerValue;
            // check if value is negative
            if (value[2] & 0x80) {
                integerValue =
                        (0xff << 24) | (value[2] << 16) | (value[1] << 8) | (value[0] << 0);
            } else {
                integerValue = (value[2] << 16) | (value[1] << 8) | (value[0] << 0);
            }
            output[sample_idx] = static_cast<float>(integerValue) / INT24_MAX;
        } else if (impl_->header.fmt.bits_per_sample == 32) {
            int32_t value;
            impl_->istream.read(reinterpret_cast<char *>(&value), sizeof(value));
            if (impl_->istream.eof())
                return false;

            output[sample_idx] = static_cast<float>(value) / std::numeric_limits<int32_t>::max();
        } else
            throw runtime_error("Invalid WAV format: invalid sample size");
    }
    return true;
}

void File::Write(const float *samples, unsigned int numSamples) {
    if (!impl_->ostream.is_open())
        throw runtime_error("WAV file not open (in write)");

    auto current_data_size = impl_->current_sample_index();
    auto bits_per_sample = impl_->header.fmt.bits_per_sample;

    // write each samples
    for (unsigned int i = 0; i < numSamples; i++) {
        if (bits_per_sample == 8) {
            // 8bits case
            int8_t value =
                    static_cast<int8_t>(samples[i] * std::numeric_limits<int8_t>::max());
            impl_->ostream.write(reinterpret_cast<char *>(&value), sizeof(value));
        } else if (bits_per_sample == 16) {
            // 16 bits
            int16_t value =
                    static_cast<int16_t>(samples[i] * std::numeric_limits<int16_t>::max());
            impl_->ostream.write(reinterpret_cast<char *>(&value), sizeof(value));
        } else if (bits_per_sample == 24) {
            // 24bits int doesn't exist in c++. We create a 3 * 8bits struct to
            // simulate
            int v = (int) (samples[i] * INT24_MAX);
            int8_t value[3];
            value[0] = reinterpret_cast<char *>(&v)[0];
            value[1] = reinterpret_cast<char *>(&v)[1];
            value[2] = reinterpret_cast<char *>(&v)[2];

            impl_->ostream.write(reinterpret_cast<char *>(&value), sizeof(value));
        } else if (bits_per_sample == 32) {
            // 32bits
            int32_t value =
                    static_cast<int32_t>(samples[i] * std::numeric_limits<int32_t>::max());
            impl_->ostream.write(reinterpret_cast<char *>(&value), sizeof(value));
        } else
            throw runtime_error("Invalid WAV format: invalid sample size");
    }

    // update header to show the right data size
    impl_->WriteHeader(current_data_size + numSamples);
}

void File::Seek(uint64_t frame_index) {
    if (!impl_->ostream.is_open() && !impl_->istream.is_open())
        throw runtime_error("WAV file not open (in seek)");

    if (frame_index > frame_number())
        throw runtime_error("Invalid seek in WAV file");

    impl_->set_current_sample_index(frame_index * channel_number());
}

uint64_t File::Tell() const {
    if (!impl_->ostream.is_open() && !impl_->istream.is_open()) {
        return 0;
    }

    auto sample_position = impl_->current_sample_index();
    return sample_position / channel_number();
}
