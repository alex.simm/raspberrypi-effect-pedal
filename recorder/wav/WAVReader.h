#ifndef EFFECTPEDAL_WAVREADER_H
#define EFFECTPEDAL_WAVREADER_H

#include "../FileReader.h"
#include "File.h"

class WAVReader : public FileReader {
public:
    WAVReader();

    ~WAVReader();

    void init(unsigned int sampleRate, unsigned int frameSize, const std::string &filename);

    void read(float *samples, unsigned int numSamples);

    void close();

private:
    File file;
};


#endif //EFFECTPEDAL_WAVREADER_H
