#include "WAVWriter.h"

WAVWriter::WAVWriter() {
}

WAVWriter::~WAVWriter() {
}

void WAVWriter::init(unsigned int sampleRate, unsigned int frameSize, const std::string &filename) {
    FileWriter::init(sampleRate, frameSize, filename);

    file.Open(filename, OpenMode::kOut);
    file.set_sample_rate(sampleRate);
    file.set_bits_per_sample(24);
    file.set_channel_number(1);
}

void WAVWriter::consume(float *f) {
    if (f != nullptr)
        file.Write(f, numSamples);
}