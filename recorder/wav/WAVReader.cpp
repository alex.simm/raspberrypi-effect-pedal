#include "WAVReader.h"
#include "../../Logger.h"

WAVReader::WAVReader() {
}

WAVReader::~WAVReader() {
}

void WAVReader::init(unsigned int sampleRate, unsigned int frameSize, const std::string &filename) {
    file.Open(filename, OpenMode::kIn);
}

void WAVReader::read(float *samples, unsigned int numSamples) {
    if (!file.Read(samples, numSamples))
        notifyListener();
}

void WAVReader::close() {
}