#ifndef EFFECTPEDAL_RECORDERLIST_H
#define EFFECTPEDAL_RECORDERLIST_H

#include <string>
#include <vector>
#include <experimental/filesystem>

class RecorderList {
public:
    RecorderList();

    void update();

    /**
     * Returns the names of all recordings.
     */
    const std::vector<std::string> &getEntries();

    /**
     * Returns the file path for a specific entry.
     */
    std::experimental::filesystem::path getPath(std::string entry);

    /**
     * Creates a path for a new recording.
     */
    std::experimental::filesystem::path createFile();

private:
    const std::string DIRECTORY = "/recordings";
    std::string path;
    std::vector<std::string> entries;
};

#endif //EFFECTPEDAL_RECORDERLIST_H
