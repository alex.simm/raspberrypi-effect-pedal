#include "Distortion.h"
#include <cmath>
#include <cstring>

using namespace std;

Distortion::Distortion() : AudioProcessor("Distortion"),
                           trigger("Trigger", 0.0f, 1.0f, 0.12f, 0.02f),
                           lowHighCut("Low/High cut", true),
                           highcut("High cut", 1000, 10000, 5000, 50),
                           lowcut("Low cut", 5, 1000, 130, 5),
                           lowHighPass("Low/High pass", true),
                           lowpass("Low pass", 1000, 10000, 5000, 50),
                           highpass("High pass", 5, 1000, 130, 5),
                           driveLevel("Drive level", 0.0f, 1.0f, 0.05f, 0.05f),
                           drive("Drive", 0.0f, 1.0f, 0.4f, 0.05f),
                           driveGain("Drive gain", -20, 20, -13) {
    properties.push_back(&trigger);
    properties.push_back(&lowHighCut);
    properties.push_back(&lowcut);
    properties.push_back(&highcut);
    properties.push_back(&lowHighPass);
    properties.push_back(&lowpass);
    properties.push_back(&highpass);
    properties.push_back(&driveLevel);
    properties.push_back(&drive);
    properties.push_back(&driveGain);
}

void Distortion::init(unsigned int sampleRate, unsigned int maxFrameSize) {
    AudioProcessor::init(sampleRate, maxFrameSize);

    iota = 0;
    piOverSR = M_PI / sampleRate;
    srPi = M_PI * sampleRate;
    twoOverSR = 0.5f / float(sampleRate);

    memset(fVec0, 0, 4096 * sizeof(float));
    memset(fVec1, 0, 2 * sizeof(float));
    memset(fVec2, 0, 2 * sizeof(float));
    memset(fVec3, 0, 2 * sizeof(float));
    memset(fVec4, 0, 2 * sizeof(float));
    memset(fVec5, 0, 2 * sizeof(float));
    memset(fVec6, 0, 2 * sizeof(float));
    memset(fVec7, 0, 2 * sizeof(float));

    memset(fRec0, 0, 2 * sizeof(float));
    memset(fRec1, 0, 2 * sizeof(float));
    memset(fRec2, 0, 2 * sizeof(float));
    memset(fRec3, 0, 2 * sizeof(float));
    memset(fRec4, 0, 3 * sizeof(float));
    memset(fRec5, 0, 3 * sizeof(float));
    memset(fRec6, 0, 2 * sizeof(float));
    memset(fRec7, 0, 2 * sizeof(float));
    memset(fRec8, 0, 2 * sizeof(float));
    memset(fRec9, 0, 3 * sizeof(float));
    memset(fRec10, 0, 3 * sizeof(float));
    memset(fRec11, 0, 2 * sizeof(float));
    memset(fRec12, 0, 2 * sizeof(float));
}

void Distortion::update() {
    fSlow0 = 1.0f - trigger.getValue();
    float fSlow1 = 0.5f;
    iSlow2 = int((uint((fSlow1 - 1)) & 4095u));
    iSlow3 = int((uint(fSlow1) & 4095u));
    float fSlow4 = (1.0f / tanf((piOverSR * fentry0)));
    fSlow5 = (fSlow4 - 1);
    fSlow6 = (1.0f / (1 + fSlow4));
    float fSlow7 = (1.0f / tanf((twoOverSR * (srPi - (6.283185f * fentry1)))));
    float fSlow8 = (1 + fSlow7);
    fSlow9 = (1.0f / fSlow8);
    fSlow10 = (0 - ((fSlow7 - 1) / fSlow8));
    iSlow11 = int(lowHighPass.getValue());
    float fSlow12 = tanf(piOverSR * float(highcut.getValue()));
    fSlow13 = (2 * (1 - (1.0f / (fSlow12 * fSlow12))));
    float fSlow14 = (1.0f / fSlow12);
    fSlow15 = (1 + ((fSlow14 - 0.765367f) / fSlow12));
    fSlow16 = (1.0f / (1 + ((0.765367f + fSlow14) / fSlow12)));
    fSlow17 = (1 + ((fSlow14 - 1.847759f) / fSlow12));
    fSlow18 = (1.0f / (1 + ((1.847759f + fSlow14) / fSlow12)));
    float fSlow19 = piOverSR * float(lowcut.getValue());
    fSlow20 = (1.0f / (1 + fSlow19));
    fSlow21 = (1 - fSlow19);
    iSlow22 = int(lowHighCut.getValue());
    fSlow23 = driveLevel.getValue();
    fSlow24 = powf(10.0f, (2 * drive.getValue()));
    fSlow25 = 9.999871e-04f * powf(10, 5.000000e-02f * (float(driveGain.getValue()) - 10));
}

//~ 1300 mus
void Distortion::process(Packet &packet) {
    for (uint i = 0; i < packet.numSamples; i++) {
        float S0[2];
        float S1[2];
        float S2[2];
        float fTempi0 = packet.samples[i];

        float fTemp0 = fTempi0 + fSlow0 * fRec1[1];
        fVec0[iota & 4095u] = fTemp0;
        fRec1[0] = (0.5f * (fVec0[(iota - iSlow3) & 4095u] + fVec0[(iota - iSlow2) & 4095u]));
        S2[0] = fRec1[0];
        fVec1[0] = (fSlow6 * fRec1[0]);
        fRec3[0] = (fVec1[1] + (fSlow6 * (fRec1[0] + (fSlow5 * fRec3[1]))));
        float fTemp1 = (fSlow9 * fRec3[0]);
        fVec2[0] = fTemp1;
        fRec2[0] = ((fVec2[0] + (fSlow10 * fRec2[1])) - fVec2[1]);
        S2[1] = fRec2[0];
        float fTemp2 = S2[iSlow11];
        S1[0] = fTemp2;
        fVec3[0] = fSlow20 * fTemp2;
        fRec7[0] = fSlow20 * (fTemp2 + fSlow21 * fRec7[1]) - fVec3[1];
        fVec4[0] = fSlow20 * fRec7[0];
        fRec6[0] = ((fSlow20 * (fRec7[0] + (fSlow21 * fRec6[1]))) - fVec4[1]);
        fRec5[0] = (fRec6[0] - (fSlow18 * ((fSlow17 * fRec5[2]) + (fSlow13 * fRec5[1]))));
        fRec4[0] = ((fSlow18 * (fRec5[2] + (fRec5[0] + (2 * fRec5[1])))) -
                    (fSlow16 * ((fSlow15 * fRec4[2]) + (fSlow13 * fRec4[1]))));
        S1[1] = fSlow16 * (fRec4[2] + fRec4[0] + 2 * fRec4[1]);
        float fTemp3 = max(-1.0f, min(1.0f, fSlow24 * (fSlow23 + S1[iSlow22])));
        float fTemp4 = fTemp3 * (1 - 0.333333f * fTemp3 * fTemp3);
        fVec5[0] = fTemp4;
        fRec0[0] = fVec5[0] + (0.995f * fRec0[1]) - fVec5[1];
        fRec8[0] = fSlow25 + 0.999f * fRec8[1];
        float fTemp5 = fRec8[0] * fRec0[0];
        S0[0] = fTemp5;
        fVec6[0] = fSlow20 * fTemp5;
        fRec12[0] = ((fSlow20 * (fTemp5 + (fSlow21 * fRec12[1]))) - fVec6[1]);
        fVec7[0] = (fSlow20 * fRec12[0]);
        fRec11[0] = ((fSlow20 * (fRec12[0] + (fSlow21 * fRec11[1]))) - fVec7[1]);
        fRec10[0] = (fRec11[0] - (fSlow18 * ((fSlow17 * fRec10[2]) + (fSlow13 * fRec10[1]))));
        fRec9[0] = ((fSlow18 * (fRec10[2] + (fRec10[0] + (2 * fRec10[1])))) -
                    (fSlow16 * (fSlow15 * fRec9[2] + fSlow13 * fRec9[1])));
        S0[1] = fSlow16 * (fRec9[2] + (fRec9[0] + 2 * fRec9[1]));
        packet.samples[i] = S0[iSlow22];

        // post processing
        fRec9[2] = fRec9[1];
        fRec9[1] = fRec9[0];
        fRec10[2] = fRec10[1];
        fRec10[1] = fRec10[0];
        fRec11[1] = fRec11[0];
        fVec7[1] = fVec7[0];
        fRec12[1] = fRec12[0];
        fVec6[1] = fVec6[0];
        fRec8[1] = fRec8[0];
        fRec0[1] = fRec0[0];
        fVec5[1] = fVec5[0];
        fRec4[2] = fRec4[1];
        fRec4[1] = fRec4[0];
        fRec5[2] = fRec5[1];
        fRec5[1] = fRec5[0];
        fRec6[1] = fRec6[0];
        fVec4[1] = fVec4[0];
        fRec7[1] = fRec7[0];
        fVec3[1] = fVec3[0];
        fRec2[1] = fRec2[0];
        fVec2[1] = fVec2[0];
        fRec3[1] = fRec3[0];
        fVec1[1] = fVec1[0];
        fRec1[1] = fRec1[0];
        iota++;
    }
}