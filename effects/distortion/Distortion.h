#ifndef EFFECTPEDAL_GDISTORTION_H
#define EFFECTPEDAL_GDISTORTION_H

#include "../../api/AudioProcessor.h"
#include "../../api/BooleanProperty.h"
#include "../../api/FloatProperty.h"
#include "../../api/IntegerProperty.h"

class Distortion : public AudioProcessor {
public:
    FloatProperty trigger, driveLevel, drive;
    BooleanProperty lowHighPass, lowHighCut;
    IntegerProperty highcut, lowcut, lowpass, highpass, driveGain;

    Distortion();

    void init(unsigned int sampleRate, unsigned int maxFrameSize) override;

    void process(Packet &packet) override;

    void update() override;

private:
    static constexpr float fentry0 = 5000.0f;
    static constexpr float fentry1 = 130.0f;

    float fSlow0, fSlow5, fSlow6, fSlow9, fSlow10, fSlow13, fSlow15, fSlow16, fSlow17, fSlow18, fSlow20, fSlow21,
            fSlow23, fSlow24, fSlow25;
    uint iSlow2, iSlow3, iSlow11, iSlow22;

    float fVec5[2];
    float fRec0[2];
    float piOverSR;
    float fVec1[2];
    float fRec3[2];
    uint iota;
    float fVec0[4096];
    float fRec1[2];
    float srPi;
    float twoOverSR;
    float fVec2[2];
    float fRec2[2];
    float fVec3[2];
    float fRec7[2];
    float fVec4[2];
    float fRec6[2];
    float fRec5[3];
    float fRec4[3];
    float fRec8[2];
    float fVec6[2];
    float fRec12[2];
    float fVec7[2];
    float fRec11[2];
    float fRec10[3];
    float fRec9[3];
};


#endif //EFFECTPEDAL_GDISTORTION_H
