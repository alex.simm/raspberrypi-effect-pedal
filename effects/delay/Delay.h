#ifndef EFFECTPEDAL_DELAY_H
#define EFFECTPEDAL_DELAY_H

#include "../../api/AudioProcessor.h"
#include "../../api/FloatProperty.h"
#include "../../api/IntegerProperty.h"

class Delay : public AudioProcessor {

public:
    IntegerProperty bpm;
    FloatProperty feedback, mix;

    Delay();

    ~Delay() override;

    void init(unsigned int sampleRate, unsigned int maxFrameSize) override;

    void update() override;

    void process(Packet &packet) override;

private:
    float *delayBuffer = nullptr;
    int delayBufferSamples = 0;
    int delayWritePosition = 0;
    float currentDelayTime = 0;
};

#endif //EFFECTPEDAL_DELAY_H
