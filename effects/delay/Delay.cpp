#include "Delay.h"
#include <algorithm>
#include <cmath>
#include "../../utils/utils.h"

using namespace std;

Delay::Delay() : AudioProcessor("Delay (fade)"),
                 bpm("BPM", 30, 200, 120, 2),
                 feedback("Feedback", 0.0f, 0.9f, 0.4f, 0.05f),
                 mix("Mix", 0.0f, 1.0f, 0.4f, 0.05f) {
    properties.push_back(&bpm);
    properties.push_back(&feedback);
    properties.push_back(&mix);
}

Delay::~Delay() {
    delete delayBuffer;
    delayBuffer = nullptr;
}

void Delay::init(unsigned int sampleRate, unsigned int maxFrameSize) {
    AudioProcessor::init(sampleRate, maxFrameSize);

    delayBufferSamples = (int) (Utils::bpmToSeconds(bpm.min) * float(sampleRate)) + 1;
    if (delayBufferSamples < 1)
        delayBufferSamples = 1;

    delayBuffer = new float[delayBufferSamples];
    fill(delayBuffer, delayBuffer + delayBufferSamples, 0);

    delayWritePosition = 0;
}

void Delay::update() {
    currentDelayTime = Utils::bpmToSeconds(bpm.getValue()) * (float) sampleRate;
}

//~ 390 mus
void Delay::process(Packet &packet) {
    float currentFeedback = feedback.getValue();
    float currentMix = mix.getValue();

    int localWritePosition = delayWritePosition;
    float in, out;
    float readPosition;
    int localReadPosition;
    float fraction, delayed1, delayed2;

    for (uint sample = 0; sample < packet.numSamples; ++sample) {
        in = packet.samples[sample];

        readPosition = fmodf((float) localWritePosition - currentDelayTime + (float) delayBufferSamples,
                             float(delayBufferSamples));
        localReadPosition = (int) floorf(readPosition);

        if (localReadPosition != localWritePosition) {
            fraction = readPosition - (float) localReadPosition;
            delayed1 = delayBuffer[localReadPosition];
            delayed2 = delayBuffer[(localReadPosition + 1) % delayBufferSamples];
            out = delayed1 + fraction * (delayed2 - delayed1);

            packet.samples[sample] = in + currentMix * (out - in);
            delayBuffer[localWritePosition] = in + out * currentFeedback;
        }

        if (++localWritePosition >= delayBufferSamples)
            localWritePosition -= delayBufferSamples;
    }

    delayWritePosition = localWritePosition;
}