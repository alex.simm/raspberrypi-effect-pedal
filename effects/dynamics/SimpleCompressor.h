#ifndef EFFECTPEDAL_COMPRESSOR_H
#define EFFECTPEDAL_COMPRESSOR_H

#include <cmath>
#include "../../api/AudioProcessor.h"
#include "../../api/FloatProperty.h"
#include "../../api/ListProperty.h"
#include "../../api/IntegerProperty.h"

class SimpleCompressor : public AudioProcessor {

public:
    FloatProperty makeupGain;
    IntegerProperty threshold;

    SimpleCompressor();

    ~SimpleCompressor() override;

    void init(unsigned int sampleRate, unsigned int maxFrameSize) override;

    void update() override;

    void process(Packet &packet) override;

private:
    const float inverseE = (float) (1.0f / M_E);

    float xl = 0.0f;
    float yl = 0.0f;
    float xg = 0.0f;
    float yg = 0.0f;
    float control = 0.0f;
    float inputLevel = 0.0f;
    float ylPrev = 0.0f;
    float inverseSampleRate = 1.0f;
    float *mixedDownInput = nullptr;
    float alphaA = 0.0f, alphaR = 0.0f;

    float calculateAttackOrRelease(float value);
};


#endif //EFFECTPEDAL_COMPRESSOR_H
