#include "Gain.h"
#include <cmath>

using namespace std;

Gain::Gain() : AudioProcessor("Gain"),
               gain("Gain", 0.9f, 1.1f, 1.0f, 0.05f),
               shape("Shape", 0.0f, 10.0f, 5.0f, 0.5f) {
    properties.push_back(&gain);
    properties.push_back(&shape);
}

void Gain::update() {
    float a1 = 1.0f / (std::exp(shape.getValue()) - 1.0f);
    factor = (std::exp(gain.getValue() * shape.getValue()) - 1) * a1;
}

void Gain::process(Packet &packet) {
    for (int i = 0; i < packet.numSamples; i++) {
        packet.samples[i] *= factor;
    }
}