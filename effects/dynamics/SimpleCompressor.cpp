#include "SimpleCompressor.h"

using namespace std;

//Expander/Noise funktioniert nicht
SimpleCompressor::SimpleCompressor() : AudioProcessor("Compressor (simple)"),
                                       threshold("Threshold (dB)", -60, 0, -24),
                                       makeupGain("Makeup gain (dB)", -12.0f, 12.0f, 0.0f, 0.5f) {
    mixedDownInput = new float[1024];

    properties.push_back(&threshold);
    properties.push_back(&makeupGain);
}

SimpleCompressor::~SimpleCompressor() {
    delete mixedDownInput;
    mixedDownInput = nullptr;
}

void SimpleCompressor::init(unsigned int sampleRate, unsigned int maxFrameSize) {
    AudioProcessor::init(sampleRate, maxFrameSize);

    delete mixedDownInput;
    mixedDownInput = new float[maxFrameSize];

    inputLevel = 0.0f;
    ylPrev = 0.0f;
    inverseSampleRate = 1.0f / (float) sampleRate;
}

void SimpleCompressor::update() {
    alphaA = calculateAttackOrRelease(0.002f);
    alphaR = calculateAttackOrRelease(0.3f);
}

void SimpleCompressor::process(Packet &packet) {
    for (uint i = 0; i < packet.numSamples; ++i) {
        mixedDownInput[i] = packet.samples[i];
    }

    auto T = (float) threshold.getValue();
    float R = 50.0f;
    float makeupGainVal = makeupGain.getValue();

    for (uint sample = 0; sample < packet.numSamples; ++sample) {
        inputLevel = powf(mixedDownInput[sample], 2.0f);
        xg = (inputLevel <= 1e-6f) ? -60.0f : 10.0f * log10f(inputLevel);
        if (xg < T)
            yg = xg;
        else
            yg = T + (xg - T) / R;

        xl = xg - yg;
        if (xl > ylPrev)
            yl = alphaA * ylPrev + (1.0f - alphaA) * xl;
        else
            yl = alphaR * ylPrev + (1.0f - alphaR) * xl;

        control = powf(10.0f, (makeupGainVal - yl) * 0.05f);
        ylPrev = yl;
        packet.samples[sample] *= control;
    }
}

float SimpleCompressor::calculateAttackOrRelease(float value) {
    if (value == 0.0f)
        return 0.0f;
    else
        return pow(inverseE, inverseSampleRate / value);
}
