#ifndef EFFECTPEDAL_GAIN_H
#define EFFECTPEDAL_GAIN_H

#include "../../api/AudioProcessor.h"
#include "../../api/FloatProperty.h"

class Gain : public AudioProcessor {
public:
    FloatProperty gain, shape;

    Gain();

    void update() override;

    void process(Packet &packet) override;

private:
    float factor = 0.0;
};


#endif //EFFECTPEDAL_GAIN_H
