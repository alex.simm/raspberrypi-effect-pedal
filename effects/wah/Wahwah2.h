#ifndef EFFECTPEDAL_WAHWAH2_H
#define EFFECTPEDAL_WAHWAH2_H

#include "../../api/AudioProcessor.h"
#include "../../api/ListProperty.h"
#include "../../api/FloatProperty.h"

class Wahwah2 : public AudioProcessor {
public:
    ListProperty<std::string> preset;
    FloatProperty pregain;

    Wahwah2();

    void update() override;

    void process(Packet &packet) override;

private:
    //Circuit parameters
    //Using these makes it straight-forward to model other
    //variants of the circuit
    float Lp; //RLC tank inductor
    float Cf; //feedback capacitor
    float Ci; //input capacitor
    float re; //equivalent resistance looking into input BJT base
    float Rp; //resistor placed parallel with the inductor
    float Ri; //input feed resistor
    float Rs; //RLC tank to BJT base resistor (dry mix)

    //Gain-setting components
    float Rc; //BJT gain stage collector resistor
    float Rpot; //Pot resistance value
    float Rbias; //Typically 470k bias resistor shows up in parallel with output
    float Re; //BJT gain stage emitter resistor
    float gf; //forward gain of BJT amplifier

    //High-Pass biquad coefficients
    float b0h, b1h, b2h;
    float a0h, a1h, a2h;

    //Band-Pass biquad coefficients
    float b0b, b2b;
    float a0b, a1b, a2b;

    //Final combined biquad coefficients used by run_filter()
    float b0;
    float b1;
    float b2;
    float a0, a0c;
    float a1, a1c;
    float a2, a2c;
    float ax; //this is used to store 1/a0 to avoid several 1/x operations

    //First order high-pass filter coefficients
    //y[n] = ghpf * ( x[n] - x[n-1] ) - a1p*y[n-1]
    float a1p;
    float ghpf;

    //biquad state variables
    float y1;
    float y2;
    float x1;
    float x2;

    //First order high-pass filter state variables
    float yh1;
    float xh1;

    inline float clip(float x) {
        float thrs = 0.8f;
        float nthrs = -0.72f;
        float f = 1.25f;

        //Hard limiting
        if (x >= 1.2) x = 1.2f;
        if (x <= -1.12) x = -1.12f;

        //Soft clipping
        if (x > thrs) {
            x -= f * (x - thrs) * (x - thrs);
        }
        if (x < nthrs) {
            x += f * (x - nthrs) * (x - nthrs);
        }

        return x;
    }
};


#endif //EFFECTPEDAL_WAHWAH2_H
