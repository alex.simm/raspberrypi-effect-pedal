#include "Wahwah2.h"
#include <cmath>

using namespace std;

Wahwah2::Wahwah2() : AudioProcessor("Wahwah (2)"),
                     preset("Preset",
                            {"Dunlop GCB-95", "Vox V847", "Dunlop Cry baby", "Clyde McCoy",
                             "Vox+Q", "Extreme"}, 0),
                     pregain("Pregain", 0.0f, 1.0f, 0.2f, 0.05f) {
    properties.push_back(&preset);
    properties.push_back(&pregain);
}

void Wahwah2::update() {
    unsigned int ckt = preset.getIndex();

    //Useful multipliers for short-hand expression of component values
    float k = 1000.0;
    float n = 1.0e-9;

    //helper variables 
    float ro, re, Req, ic, beta;
    switch (ckt) {
        case 0: //Dunlop GCB-95 variant
            //Default values from Crybaby GCB-95 circuit
            this->Rc = 22.0f * k; //BJT gain stage collector resistor
            this->Rpot = 100.0f * k; //Pot resistance value
            this->Rbias = 470.0f * k; //Typically 470k bias resistor shows up in parallel with output
            this->Re = 390.0; //BJT gain stage emitter resistor

            //Equivalent output resistance seen from BJT collector
            ro = this->Rc * this->Rpot / (this->Rc + this->Rpot);
            ro = ro * this->Rbias / (ro + this->Rbias);

            ic = 3.7f / this->Rc;  //Typical bias current
            re = 0.025f / ic; //BJT gain stage equivalent internal emitter resistance
            // gm = Ic/Vt, re = 1/gm
            Req = this->Re + re;

            this->gf = -ro / Req;  //forward gain of transistor stage

            beta = 250.0;  //BJT forward gain
            this->re = beta * Req; //Resistance looking into BJT emitter

            this->Cf = 10.0f * n;
            this->Ci = 10.0f * n;
            this->Rp = 33.0f * k * this->re / (this->re + 33.0f * k);
            this->Lp = 0.5;
            this->Ri = 68.0f * k;
            this->Rs = 1.5f * k;
            break;

        case 1: //Vox V-847 voicing
            this->Rc = 22.0f * k; //BJT gain stage collector resistor
            this->Rpot = 100.0f * k; //Pot resistance value
            this->Rbias = 470.0f * k; //Typically 470k bias resistor shows up in parallel with output
            this->Re = 510.0; //BJT gain stage emitter resistor

            //Equivalent output resistance seen from BJT collector
            ro = this->Rc * this->Rpot / (this->Rc + this->Rpot);
            ro = ro * this->Rbias / (ro + this->Rbias);

            ic = 3.7f / this->Rc;  //Typical bias current
            re = 0.025f / ic; //BJT gain stage equivalent internal emitter resistance
            // gm = Ic/Vt, re = 1/gm
            Req = this->Re + re;

            this->gf = -ro / Req;  //forward gain of transistor stage

            beta = 650.0;  //BJT forward gain
            this->re = beta * Req; //Resistance looking into BJT emitter

            this->Cf = 10.0f * n;
            this->Ci = 10.0f * n;
            this->Ri = 68.0f * k;
            this->Rp = 33.0f * k;
            this->Rp = this->Rp * this->Ri / (this->Rp + this->Ri);
            this->Lp = 0.5;
            this->Rs = 1.5f * k;
            break;

        case 2: //Original Crybaby voicing
            this->Rc = 22.0f * k; //BJT gain stage collector resistor
            this->Rpot = 100.0f * k; //Pot resistance value
            this->Rbias = 470.0f * k; //Typically 470k bias resistor shows up in parallel with output
            this->Re = 470.0; //BJT gain stage emitter resistor

            //Equivalent output resistance seen from BJT collector
            ro = this->Rc * this->Rpot / (this->Rc + this->Rpot);
            ro = ro * this->Rbias / (ro + this->Rbias);

            ic = 3.7f / this->Rc;  //Typical bias current
            re = 0.025f / ic; //BJT gain stage equivalent internal emitter resistance
            // gm = Ic/Vt, re = 1/gm
            Req = this->Re + re;

            this->gf = -ro / Req;  //forward gain of transistor stage

            beta = 250.0;  //BJT forward gain
            this->re = beta * Req; //Resistance looking into BJT emitter

            this->Cf = 10.0f * n;
            this->Ci = 10.0f * n;
            this->Rp = 33.0f * k * this->re / (this->re + 33.0f * k);
            this->Lp = 0.66;
            this->Ri = 68.0f * k;
            this->Rs = 1.5f * k;
            break;

        case 3: //Clyde McCoy voicing
            this->Rc = 22.0f * k; //BJT gain stage collector resistor
            this->Rpot = 100.0f * k; //Pot resistance value
            this->Rbias = 470.0f * k; //Typically 470k bias resistor shows up in parallel with output
            this->Re = 470.0; //BJT gain stage emitter resistor

            //Equivalent output resistance seen from BJT collector
            ro = this->Rc * this->Rpot / (this->Rc + this->Rpot);
            ro = ro * this->Rbias / (ro + this->Rbias);

            ic = 3.7f / this->Rc;  //Typical bias current
            re = 0.025f / ic; //BJT gain stage equivalent internal emitter resistance
            // gm = Ic/Vt, re = 1/gm
            Req = this->Re + re;

            this->gf = -ro / Req;  //forward gain of transistor stage

            beta = 250.0;  //BJT forward gain
            this->re = beta * Req; //Resistance looking into BJT emitter

            this->Cf = 10.0f * n;
            this->Ci = 10.0f * n;
            this->Rp = 100.0f * k * this->re / (this->re + 100.0f * k);
            this->Lp = 0.5;
            this->Ri = 68.0f * k;
            this->Rs = 1.5f * k;
            break;

        case 4: //Vox with a little more vocal voicing that also cuts through distortion
            this->Rc = 22.0f * k; //BJT gain stage collector resistor
            this->Rpot = 100.0f * k; //Pot resistance value
            this->Rbias = 470.0f * k; //Typically 470k bias resistor shows up in parallel with output
            this->Re = 510.0; //BJT gain stage emitter resistor

            //Equivalent output resistance seen from BJT collector
            ro = this->Rc * this->Rpot / (this->Rc + this->Rpot);
            ro = ro * this->Rbias / (ro + this->Rbias);

            ic = 3.7f / this->Rc;  //Typical bias current
            re = 0.025f / ic; //BJT gain stage equivalent internal emitter resistance
            // gm = Ic/Vt, re = 1/gm
            Req = this->Re + re;

            this->gf = -ro / Req;  //forward gain of transistor stage

            beta = 250.0;  //BJT forward gain
            this->re = beta * Req; //Resistance looking into BJT emitter

            this->Cf = 15.0f * n;
            this->Ci = 8.0f * n;
            this->Rp = 47.0f * k * this->re / (this->re + 47.0f * k);
            this->Lp = 0.5f;
            this->Ri = 68.0f * k;
            this->Rs = 800.0;
            break;

        case 5: //Vox with a little more vocal voicing that also cuts through distortion
            this->Rc = 22.0f * k; //BJT gain stage collector resistor
            this->Rpot = 100.0f * k; //Pot resistance value
            this->Rbias = 470.0f * k; //Typically 470k bias resistor shows up in parallel with output
            this->Re = 150.0; //BJT gain stage emitter resistor

            //Equivalent output resistance seen from BJT collector
            ro = this->Rc * this->Rpot / (this->Rc + this->Rpot);
            ro = ro * this->Rbias / (ro + this->Rbias);

            ic = 3.7f / this->Rc;  //Typical bias current
            re = 0.025f / ic; //BJT gain stage equivalent internal emitter resistance
            // gm = Ic/Vt, re = 1/gm
            Req = this->Re + re;

            this->gf = -ro / Req;  //forward gain of transistor stage

            beta = 1200.0;  //BJT forward gain
            this->re = beta * Req; //Resistance looking into BJT emitter

            this->Cf = 10.0f * n;
            this->Ci = 47.0f * n;
            this->Rp = 150.0f * k;
            this->Lp = 0.5f;
            this->Ri = 220.0f * k;
            this->Rs = 100;
            break;
        default:
            break;
    }

    //Compute DSP filter coefficients 
    //Useful constants
    float RpRi = this->Rp * this->Ri / (this->Rp + this->Ri);
    float f0 = (float) (1.0 / (2.0 * M_PI * sqrtf(this->Lp * this->Cf)));
    float w0 = (float) (2.0 * M_PI * f0 / sampleRate);
    float Q = RpRi * sqrtf(this->Cf / this->Lp);
    float c = cos(w0);
    float s = sin(w0);
    float alpha = s / (2.0f * Q);

    //High Pass Biquad Coefficients
    this->b0h = (1.0f + c) / 2.0f;
    this->b1h = -(1.0f + c);
    this->b2h = (1.0f + c) / 2.0f;

    //Band-pass biquad coefficients
    this->b0b = Q * alpha;
    this->b2b = -Q * alpha;
    this->a0b = 1.0f + alpha;
    this->a1b = -2.0f * c;
    this->a2b = 1.0f - alpha;


    //1-pole high pass filter coefficients
    // H(z) = g * (1 - z^-1)/(1 - a1*z^-1)
    // Direct Form 1:
    //      y[n] = ghpf * ( x[n] - x[n-1] ) - a1p*y[n-1]

    this->a1p = -expf(-1 / (this->Ri * this->Ci * sampleRate));
    this->ghpf = this->gf * (1.0f - this->a1p) * 0.5f;   //BJT forward gain worked in here to
    // save extra multiplications in
    // updating biquad coefficients

    //Distill all down to final biquad coefficients
    float Gi = this->Rs / (this->Ri + this->Rs);
    float gbpf = (float) (1.0 / (2.0 * M_PI * f0 * this->Ri * this->Cf));  //band-pass component equivalent gain

    //Final Biquad numerator coefficients
    this->b0 = gbpf * this->b0b + Gi * this->a0b;
    this->b1 = Gi * this->a1b;
    this->b2 = gbpf * this->b2b + Gi * this->a2b;

    //Constants to make denominator coefficients computation more efficient
    //in real-time
    this->a0c = -this->gf * this->b0h;
    this->a1c = -this->gf * this->b1h;
    this->a2c = -this->gf * this->b2h;

    //denominator coefficients (initialization)
    this->a0 = this->a0b;
    this->a1 = -this->a1b; //negated because of addition used in
    this->a2 = -this->a2b; //biquad filter form
    this->ax = 1.0f / this->a0;

    //biquad state variables
    this->y1 = 0.0;
    this->y2 = 0.0;
    this->x1 = 0.0;
    this->x2 = 0.0;

    //First order high-pass filter state variables
    this->yh1 = 0.0;
    this->xh1 = 0.0;
}

//~ 450 mus
void Wahwah2::process(Packet &packet) {
    const float gp = pregain.getValue();
    float gw, y0, hpf, out;

    for (int i = 0; i < packet.numSamples; i++) {
        //The magic numbers below approximate frequency warping characteristic
        //float gw = 4.6-18.4/(4.0 + gp);
        gw = gp * (1.15f - 0.235f * gp); //Approximation of the approximation

        //Update Biquad coefficients
        this->a0 = this->a0b + gw * this->a0c;
        this->a1 = -(this->a1b + gw * this->a1c);
        this->a2 = -(this->a2b + gw * this->a2c);
        this->ax = 1.0f / this->a0;

        //run it through the 1-pole HPF and gain first
        hpf = this->ghpf * (packet.samples[i] - this->xh1) - this->a1p * this->yh1;
        this->xh1 = packet.samples[i];
        this->yh1 = hpf;

        //Apply modulated biquad
        y0 = this->b0 * hpf + this->b1 * this->x1 + this->b2 * this->x2
             + (this->a1 * this->y1 + this->a2 * this->y2);
        y0 *= this->ax;
        out = clip(y0);
        y0 = 0.95f * y0 + 0.05f * out; //Let a little harmonic distortion feed back into the filter loop
        this->x2 = this->x1;
        this->x1 = hpf;
        this->y2 = this->y1;
        this->y1 = y0;

        packet.samples[i] = out;
    }
}