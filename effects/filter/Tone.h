#ifndef EFFECTPEDAL_TONE_H
#define EFFECTPEDAL_TONE_H

#include "../../api/AudioProcessor.h"
#include "../../api/IntegerProperty.h"
#include "../../utils/filter/BWLowPass.h"
#include <mutex>

class Tone : public AudioProcessor {
public:
    IntegerProperty frequency;

    Tone();

    ~Tone() override;

    void init(unsigned int sampleRate, unsigned int maxFrameSize) override;

    void update() override;

    void process(Packet &packet) override;

private:
    BWLowPass filter;
    std::mutex mutex;
};


#endif //EFFECTPEDAL_TONE_H
