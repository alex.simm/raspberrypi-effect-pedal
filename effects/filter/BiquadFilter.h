#ifndef EFFECTPEDAL_GBIQUAD_H
#define EFFECTPEDAL_GBIQUAD_H

#include "../../api/AudioProcessor.h"
#include "../../api/IntegerProperty.h"

class BiquadFilter : public AudioProcessor {
public:
    IntegerProperty frequency;

    BiquadFilter();

    void init(unsigned int sampleRate, unsigned int maxFrameSize) override;

    void process(Packet &packet) override;

    void update() override;

private:
    float param1 = 0;
    float sampleTime = 0;
    float fRec0[3];
};

#endif //EFFECTPEDAL_GBIQUAD_H
