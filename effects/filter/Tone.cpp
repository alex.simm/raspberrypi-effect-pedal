#include "Tone.h"

Tone::Tone() : AudioProcessor("Tone"),
               frequency("LowPass Frequenz", 100, 2000, 1000, 50),
               filter(BWLowPass(2, 0, frequency.getValue())) {
    properties.push_back(&frequency);
}

Tone::~Tone() {
}

void Tone::init(unsigned int sampleRate, unsigned int maxFrameSize) {
    AudioProcessor::init(sampleRate, maxFrameSize);
}

//TODO: mutex testen
void Tone::update() {
    std::lock_guard<std::mutex> guard(mutex);
    filter.updateParams(sampleRate, frequency.getValue());
}

//~ 340 mus
void Tone::process(Packet &packet) {
    std::lock_guard<std::mutex> guard(mutex);
    filter.applyAll(packet.samples, packet.numSamples);
}