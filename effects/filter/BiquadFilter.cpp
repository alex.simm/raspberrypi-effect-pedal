#include "BiquadFilter.h"
#include <cmath>
#include <cstring>

BiquadFilter::BiquadFilter() : AudioProcessor("Biquad filter"),
                               frequency("Frequenz", 300, 3000, 800, 5) {
    properties.push_back(&frequency);
}

void BiquadFilter::init(unsigned int sampleRate, unsigned int maxFrameSize) {
    AudioProcessor::init(sampleRate, maxFrameSize);

    sampleTime = (float) (2 * M_PI / sampleRate);
    memset(fRec0, 0, 3 * sizeof(float));
}

void BiquadFilter::update() {
    float fSlow0 = logf(sampleTime * float(frequency.getValue()));
    float tmp = 0.07238887f + fSlow0 * (1.31282248f + fSlow0 * (0.43359433f + fSlow0 *
                                                                              (0.27547621f + fSlow0 *
                                                                                             (0.06446806f +
                                                                                              0.00506158f * fSlow0))));
    param1 = -1.8442f * cosf(expf(tmp));
}

//~ 240 mus
void BiquadFilter::process(Packet &packet) {
    for (uint i = 0; i < packet.numSamples; i++) {
        fRec0[0] = packet.samples[i] - param1 * fRec0[1] - 0.8502684100000001f * fRec0[2];
        packet.samples[i] = 0.31622776601683794f * (fRec0[0] - 1.059f * fRec0[1]);

        // post processing
        fRec0[2] = fRec0[1];
        fRec0[1] = fRec0[0];
    }
}
