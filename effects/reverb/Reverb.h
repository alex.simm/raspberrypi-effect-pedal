#ifndef EFFECTPEDAL_REVERB_H
#define EFFECTPEDAL_REVERB_H

#include "../../api/AudioProcessor.h"

class Reverb : public AudioProcessor {
public:
    Reverb();

    void process(Packet &packet) override;

private:
    static const unsigned int D1 = 276;
    static const unsigned int D2 = 594;
    static const unsigned int D3 = 958;
    static const unsigned int D4 = 1200;
    static const unsigned int D5 = 1550;
    static const unsigned int D6 = 526;
    static constexpr float a1 = 0.7079, a2 = 0.6496, a3 = 0.6200, a4 = 0.6016, a5 = 0.4334, a6 = 0.5664;
    static constexpr float b1 = 0.99, b2 = 0.95, b3 = 0.90, b4 = 0.80;

    float X1[D1] = {0};
    float X2[D2] = {0};
    float X3[D3] = {0};
    float X4[D4] = {0};
    float X5[D5] = {0};
    float X6[D6] = {0};

    float S1 = 0, S2 = 0, S3 = 0, S4 = 0, S5 = 0, S6 = 0, S7 = 0, S51 = 0;
    int DC1 = 0, DC2 = 0, DC3 = 0, DC4 = 0, DC5 = 1, DC6 = 1;
    int index5 = 0, index6 = 0;
};

#endif //EFFECTPEDAL_REVERB_H
