#include "Reverb.h"

Reverb::Reverb() : AudioProcessor("Reverb") {
}

//~ 710 mus
void Reverb::process(Packet &packet) {
    float in;
    for (int i = 0; i < packet.numSamples; ++i) {
        in = packet.samples[i];

        X1[DC1] = in + a1 * X1[DC1];
        X2[DC2] = in + a2 * X2[DC2];
        X3[DC3] = in + a3 * X3[DC3];
        X4[DC4] = in + a4 * X4[DC4];

        S1 = b1 * X1[DC1];
        S2 = b2 * X2[DC2];
        S3 = b3 * X3[DC3];
        S4 = b4 * X4[DC4];
        S5 = (S1 + S2 + S3 + S4) / 4;

        DC1++;
        if (DC1 >= D1) DC1 = 0;
        DC2++;
        if (DC2 >= D2) DC2 = 0;
        DC3++;
        if (DC3 >= D3) DC3 = 0;
        DC4++;
        if (DC4 >= D4) DC4 = 0;

        X5[index5] = S5 + a5 * X5[DC5];
        S6 = X5[DC5] - a5 * X5[index5];

        DC5++;
        if (DC5 >= D5) DC5 = 0;
        index5++;
        if (index5 >= D5) index5 = 0;

        X6[index6] = (S6 + a6 * X6[DC6]);
        S7 = X6[DC6] - a6 * X6[index6];

        DC6++;
        if (DC6 >= D6) DC6 = 0;
        index6++;
        if (index6 >= D6) index6 = 0;

        packet.samples[i] = S7;
    }
}