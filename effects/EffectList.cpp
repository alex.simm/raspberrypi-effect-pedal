#include "EffectList.h"
#include "../Logger.h"
#include "../utils/utils.h"
#include <algorithm>

#include "delay/Delay.h"
#include "distortion/Distortion.h"
#include "extras/Metronome.h"
#include "filter/BiquadFilter.h"
#include "filter/Tone.h"
#include "overdrive/BottleRocket.h"
#include "overdrive/Overdrive.h"
#include "modulation/Chorus.h"
#include "modulation/Tremolo.h"
#include "reverb/Reverb.h"
#include "wah/Wahwah2.h"
#include "dynamics/Gain.h"
#include "dynamics/SimpleCompressor.h"
#include "modulation/Flanger.h"
#include "overdrive/COverD.h"
#include "caps/DDelay.h"

using namespace std;

EffectList::EffectList() {
    //1. tone
    std::vector<AudioProcessor *> categoryFilters;
    categoryFilters.push_back(new Tone());
    categoryFilters.push_back(new BiquadFilter());

    //2. wahwahs
    categoryFilters.push_back(new Wahwah2());

    //3. octaver

    //4. dynamik: compressor, limiter
    std::vector<AudioProcessor *> categoryDynamic;
    categoryDynamic.push_back(new SimpleCompressor());

    //5. pre-distortion equalizer/gain
    categoryDynamic.push_back(new Gain());

    //6a. overdrive
    std::vector<AudioProcessor *> categoryDistortion;
    categoryDistortion.push_back(new Overdrive());
    categoryDistortion.push_back(new COverD());
    categoryDistortion.push_back(new BottleRocket());

    //6b. distortion
    categoryDistortion.push_back(new Distortion());

    //6c. fuzz

    //7. post-distortion equalizer/gain

    //8. pitch shift: harmonizer

    //9. modulation: chorus, flanger, phaser
    std::vector<AudioProcessor *> categoryModulation;
    categoryModulation.push_back(new Chorus());
    categoryModulation.push_back(new Tremolo());
    categoryModulation.push_back(new Flanger());

    //10. delay
    std::vector<AudioProcessor *> categoryDelay;
    categoryDelay.push_back(new Delay());
    categoryDelay.push_back(new DDelay());

    //11. reverb
    categoryDelay.push_back(new Reverb());

    //12. extras
    std::vector<AudioProcessor *> categoryExtras;
    categoryExtras.push_back(new Metronome());

    // add all categories
    categories.push_back(categoryFilters);
    categories.push_back(categoryDynamic);
    categories.push_back(categoryDistortion);
    categories.push_back(categoryModulation);
    categories.push_back(categoryDelay);
    categories.push_back(categoryExtras);

    // copy categories to main list
    effects = Utils::combineVectors(categories);

    // copy names and disable all effects
    for (const vector<AudioProcessor *> &v : categories) {
        categorisedNames.push_back(Utils::applyTo<string>(v, [](AudioProcessor *a) {
            return a->getName();
        }));
    }
    names = Utils::combineVectors(categorisedNames);

    // disable all effects
    for_each(effects.begin(), effects.end(), [](AudioProcessor *p) {
        p->setMode(AudioProcessor::Mode::DISABLED);
    });
}

const vector<AudioProcessor *> &EffectList::getEffects() const {
    return effects;
}

const std::vector<std::vector<AudioProcessor *> > &EffectList::getEffectCategories() const {
    return categories;
}

const vector<string> &EffectList::getEffectNames() const {
    return names;
}

const std::vector<std::vector<std::string> > &EffectList::getCategorisedEffectNames() const {
    return categorisedNames;
}

AudioProcessor *EffectList::getEffect(const std::string &name) const {
    for (AudioProcessor *a : effects) {
        if (a->getName() == name) return a;
    }
    return nullptr;
}
