#include "Metronome.h"
#include "../../Logger.h"
#include <cmath>

using namespace std;

Metronome::Metronome() : AudioProcessor("Metronom"),
                         beatsPerMinute("Bpm", 1, 300, 60),
                         beatsPerMeasure("Takt", 1, 30, 4),
                         volume1("Volume 1", 0.1f, 1.0f, 0.1f, 0.05f),
                         volume2("Volume 2", 0.1f, 1.0f, 0.1f, 0.05f) {
    properties.push_back(&beatsPerMinute);
    properties.push_back(&beatsPerMeasure);
    properties.push_back(&volume1);
    properties.push_back(&volume2);
    excludeFromPresets = true;
}

void Metronome::init(unsigned int sampleRate, unsigned int maxFrameSize) {
    AudioProcessor::init(sampleRate, maxFrameSize);

    sampleCount = 0;
    beatCount = 0;

    toneSize = sampleRate / 5u;
    tone1 = new float[toneSize];
    tone2 = new float[toneSize];

    tone1Offset = tone2Offset = -1;
}

void Metronome::update() {
    samplesPerBeat = (unsigned int) floor(float(sampleRate) * 60 / float(beatsPerMinute.getValue()));
    beatCount = 0;

    float vol1 = volume1.getValue();
    float vol2 = volume2.getValue();
    auto invSR = float(2 * M_PI / sampleRate);
    float factor = 0;
    for (uint i = 0; i < toneSize - 1; i++) {
        tone1[i] = vol1 * sinf(factor * TONE1_FREQ);
        tone2[i] = vol2 * sinf(factor * TONE2_FREQ);
        factor += invSR;
    }
    Logger::debug("Metronome update: beatsPerMinute=" + to_string(beatsPerMinute.getValue())
                  + ", beatsPerMeasure=" + to_string(beatsPerMeasure.getValue())
                  + ", sampleRate=" + to_string(sampleRate)
                  + ", samplesPerBeat=" + to_string(samplesPerBeat)
                  + ", volumes=" + to_string(vol1) + "/" + to_string(vol2)
                  + ", toneSize=" + to_string(toneSize));
}

//~ 210 mus
void Metronome::process(Packet &packet) {
    const int currentBeatsPerMeasure = beatsPerMeasure.getValue();

    for (uint i = 0; i < packet.numSamples; ++i) {
        if (sampleCount == 0) {
            if (beatCount == 0) {
                tone1Offset = 0;
                tone2Offset = -1;
            }
            else {
                tone2Offset = 0;
                tone1Offset = -1;
            }

            beatCount = (beatCount + 1) % currentBeatsPerMeasure;
        }

        if (tone1Offset >= 0) {
            packet.samples[i] += tone1[tone1Offset++] * packet.maxLevel;
            if (tone1Offset >= toneSize)
                tone1Offset = -1;
        } else if (tone2Offset >= 0) {
            packet.samples[i] += tone2[tone2Offset++] * packet.maxLevel;
            if (tone2Offset >= toneSize)
                tone2Offset = -1;
        }

        sampleCount = (sampleCount + 1) % samplesPerBeat;
    }
}