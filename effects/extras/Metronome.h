#ifndef EFFECTPEDAL_METRONOME_H
#define EFFECTPEDAL_METRONOME_H

#include "../../api/AudioProcessor.h"
#include "../../api/IntegerProperty.h"
#include "../../api/FloatProperty.h"

class Metronome : public AudioProcessor {
public:
    IntegerProperty beatsPerMinute, beatsPerMeasure;
    FloatProperty volume1, volume2;

    Metronome();

    void init(unsigned int sampleRate, unsigned int maxFrameSize) override;

    void update() override;

    void process(Packet &packet) override;

private:
    unsigned int samplesPerBeat = 0;
    unsigned int sampleCount = 0, beatCount = 0;

    const unsigned int TONE1_FREQ = 800;
    const unsigned int TONE2_FREQ = 440;

    float *tone1 = nullptr, *tone2 = nullptr;
    uint toneSize = 0;
    int tone1Offset = 0, tone2Offset = 0;
};


#endif //EFFECTPEDAL_METRONOME_H
