#ifndef EFFECTPEDAL_EFFECTLIST_H
#define EFFECTPEDAL_EFFECTLIST_H

#include <vector>
#include <string>
#include <algorithm>
#include "../api/AudioProcessor.h"

class EffectList {
public:
    EffectList();

    const std::vector<AudioProcessor *> &getEffects() const;

    const std::vector<std::vector<AudioProcessor *>> &getEffectCategories() const;

    const std::vector<std::string> &getEffectNames() const;

    const std::vector<std::vector<std::string>> &getCategorisedEffectNames() const;

    AudioProcessor *getEffect(const std::string &name) const;

private:
    std::vector<AudioProcessor *> effects;
    std::vector<std::vector<AudioProcessor *>> categories;
    std::vector<std::string> names;
    std::vector<std::vector<std::string>> categorisedNames;
};

#endif //EFFECTPEDAL_EFFECTLIST_H
