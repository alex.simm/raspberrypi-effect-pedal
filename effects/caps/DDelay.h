#ifndef EFFECTPEDAL_DDELAY_H
#define EFFECTPEDAL_DDELAY_H

#include "../../api/AudioProcessor.h"
#include "../../api/FloatProperty.h"
#include "../../api/IntegerProperty.h"
#include "../../utils/RingBuffer.h"
#include "../../utils/filter/IIRLowPass.h"
#include "../../api/BooleanProperty.h"

class DDelay : public AudioProcessor {
public:
    IntegerProperty bpm, repetition;

    DDelay();

    ~DDelay() override = default;

    void init(unsigned int sampleRate, unsigned int maxFrameSize) override;

    void process(Packet &packet) override;

    void update() override;

private:
    static const uint MAX_STEPS = 4;
    const float g[MAX_STEPS] = {0.4, 0.7, 0.8, 0.7};

    uint paramDiv;
    int paramTime;
    RingBuffer delays[MAX_STEPS];
};

#endif //EFFECTPEDAL_DDELAY_H
