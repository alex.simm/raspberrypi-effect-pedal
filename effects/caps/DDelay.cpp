#include "DDelay.h"
#include "../../utils/utils.h"
#include <cmath>

using namespace std;

DDelay::DDelay() : AudioProcessor("Delay (fixed)"),
                   bpm("BPM", 30, 200, 120, 2),
                   repetition("Repetition", 2, 4, 4) {
    properties.push_back(&bpm);
    properties.push_back(&repetition);

    paramDiv = 0;
    paramTime = 0;
}

void DDelay::init(unsigned int sampleRate, unsigned int maxFrameSize) {
    AudioProcessor::init(sampleRate, maxFrameSize);

    float l = 2 * sampleRate; //one beat at 30 bpm
    for (auto &delay : delays) {
        delay.init(Utils::nextPowerOf2(lround(l)));
        delay.clear();
    }
}

void DDelay::update() {
    paramDiv = repetition.getValue();
    paramTime = -1 + (int) (Utils::bpmToSeconds(bpm.getValue()) * sampleRate);
}

void DDelay::process(Packet &packet) {
    float x, y;
    for (uint i = 0; i < packet.numSamples; i++) {
        x = packet.samples[i];
        y = x;

        for (uint j = 0; j < paramDiv; ++j) {
            delays[j].put(y);
            y = delays[j].past(paramTime);
            x += g[j] * y;
        }

        packet.samples[i] = x;
    }
}