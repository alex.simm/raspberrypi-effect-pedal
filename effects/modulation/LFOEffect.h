#ifndef EFFECTPEDAL_LFOEFFECT_H
#define EFFECTPEDAL_LFOEFFECT_H

#include <math.h>
#include "../../api/AudioProcessor.h"

class LFOEffect : public AudioProcessor {
public:
    LFOEffect(std::string name);

protected:
    float lfo(float phase, int waveform);

private:
    const float TWO_PI = (float) (2 * M_PI);
};


#endif //EFFECTPEDAL_LFOEFFECT_H
