#include "Chorus.h"
#include <cstring>

using namespace std;

Chorus::Chorus() : LFOEffect("Chorus"),
                   delay("Delay (mus)", 10, 50, 30),
                   width("Width (mus)", 10, 50, 20),
                   depth("Depth", 0.0f, 1.0f, 0.5f, 0.1f),
                   numVoices("Anzahl Stimmen", {2, 3, 4, 5}, 1),
                   frequency("LFO Frequenz (Hz)", 0.05f, 2.0f, 0.2f, 0.05f),
                   waveform("LFO Waveform",
                            {"Sine", "Triangle", "Sawtooth (rising)", "Sawtooth (falling)"}, 0),
                   interpolation("Interpolation", {"None", "Linear", "Cubic"}, 1) {
    properties.push_back(&delay);
    properties.push_back(&width);
    properties.push_back(&depth);
    properties.push_back(&numVoices);
    properties.push_back(&frequency);
    properties.push_back(&waveform);
    properties.push_back(&interpolation);
}

Chorus::~Chorus() {
    delete delayBuffer;
    delayBuffer = nullptr;
}

void Chorus::init(unsigned int sampleRate, unsigned int maxFrameSize) {
    AudioProcessor::init(sampleRate, maxFrameSize);

    delayBufferSamples = (unsigned int) ((delay.max * 0.001f + width.max * 0.001f) * (float) sampleRate) + 1;
    if (delayBufferSamples < 1)
        delayBufferSamples = 1;

    delete delayBuffer;
    delayBuffer = new float[delayBufferSamples];
    memset(delayBuffer, 0, delayBufferSamples * sizeof(float));

    delayWritePosition = 0;
    lfoPhase = 0.0f;
    inverseSampleRate = 1.0f / (float) sampleRate;
}

//~ 2100 - 2200 mus
void Chorus::process(Packet &packet) {
    float currentDelay = delay.getValue() * 0.001f;
    float currentWidth = width.getValue() * 0.001f;
    float currentDepth = depth.getValue();
    unsigned int currentNumVoices = numVoices.getValue() + 2;
    float currentFrequency = frequency.getValue();
    unsigned int currentInterpolation = interpolation.getIndex();
    unsigned int currentWaveform = waveform.getIndex();

    unsigned int localWritePosition = delayWritePosition;
    float phase = lfoPhase;

    float in, out, phaseOffset, localDelayTime, readPosition;
    unsigned int localReadPosition;

    for (uint sample = 0; sample < packet.numSamples; ++sample) {
        in = packet.samples[sample];
        phaseOffset = 0.0f;

        for (uint voice = 0; voice < currentNumVoices - 1; ++voice) {
            localDelayTime = (currentDelay + currentWidth * lfo(phase + phaseOffset, currentWaveform)) *
                             (float) sampleRate;

            readPosition = fmodf((float) localWritePosition - localDelayTime + (float) delayBufferSamples,
                                 delayBufferSamples);
            localReadPosition = (unsigned int) floorf(readPosition);

            switch (currentInterpolation) {
                case 0: {
                    out = delayBuffer[localReadPosition % delayBufferSamples];
                    break;
                }
                case 1: {
                    float fraction = readPosition - (float) localReadPosition;
                    float delayed0 = delayBuffer[(localReadPosition + 0)];
                    float delayed1 = delayBuffer[(localReadPosition + 1) % delayBufferSamples];
                    out = delayed0 + fraction * (delayed1 - delayed0);
                    break;
                }
                case 2: {
                    float fraction = readPosition - (float) localReadPosition;
                    float fractionSqrt = fraction * fraction;
                    float fractionCube = fractionSqrt * fraction;

                    float sample0 = delayBuffer[(localReadPosition - 1 + delayBufferSamples) % delayBufferSamples];
                    float sample1 = delayBuffer[(localReadPosition + 0)];
                    float sample2 = delayBuffer[(localReadPosition + 1) % delayBufferSamples];
                    float sample3 = delayBuffer[(localReadPosition + 2) % delayBufferSamples];

                    float a0 = -0.5f * sample0 + 1.5f * sample1 - 1.5f * sample2 + 0.5f * sample3;
                    float a1 = sample0 - 2.5f * sample1 + 2.0f * sample2 - 0.5f * sample3;
                    float a2 = -0.5f * sample0 + 0.5f * sample2;
                    float a3 = sample1;
                    out = a0 * fractionCube + a1 * fractionSqrt + a2 * fraction + a3;
                    break;
                }
                default:
                    out = 0.0f;
            }

            packet.samples[sample] += out * currentDepth;

            if (currentNumVoices == 3)
                phaseOffset += 0.25f;
            else if (currentNumVoices > 3)
                phaseOffset += 1.0f / (float) (currentNumVoices - 1);
        }

        delayBuffer[localWritePosition] = in;

        if (++localWritePosition >= delayBufferSamples)
            localWritePosition -= delayBufferSamples;

        phase += currentFrequency * inverseSampleRate;
        if (phase >= 1.0f)
            phase -= 1.0f;
    }

    delayWritePosition = localWritePosition;
    lfoPhase = phase;
}