#ifndef EFFECTPEDAL_TREMOLO_H
#define EFFECTPEDAL_TREMOLO_H

#include <cmath>
#include "../../api/AudioProcessor.h"
#include "../../api/FloatProperty.h"
#include "../../api/ListProperty.h"

class Tremolo : public AudioProcessor {

public:
    FloatProperty depth, frequency;

    Tremolo();

    void init(unsigned int sampleRate, unsigned int maxFrameSize) override;

    void process(Packet &packet) override;

private:
    const float TWO_PI = (float) (2.0 * M_PI);

    float lfoPhase = 0.0f;
    float inverseSampleRate = 1.0f;
};

#endif //EFFECTPEDAL_TREMOLO_H
