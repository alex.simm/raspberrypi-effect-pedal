#include "Tremolo.h"

using namespace std;

//TODO: sehr viel white noise
Tremolo::Tremolo() : AudioProcessor("Tremolo"),
                     depth("Depth", 0.0f, 1.0f, 0.5f, 0.1f),
                     frequency("LFO Frequenz (Hz)", 0.0f, 10.0f, 4.0f, 1.0f) {
    properties.push_back(&depth);
    properties.push_back(&frequency);
}

void Tremolo::init(unsigned int sampleRate, unsigned int maxFrameSize) {
    AudioProcessor::init(sampleRate, maxFrameSize);

    lfoPhase = 0.0f;
    inverseSampleRate = 1.0f / (float) sampleRate;
}

//~ 370 mus
void Tremolo::process(Packet &packet) {
    const float currentDepth = depth.getValue();
    const float currentFrequency = frequency.getValue();

    float phase = lfoPhase;
    float wave;

    for (int sample = 0; sample < packet.numSamples; ++sample) {
        wave = 0.5f + 0.5f * sinf(TWO_PI * phase);
        packet.samples[sample] *= 1 - currentDepth + currentDepth * wave;

        phase += currentFrequency * inverseSampleRate;
        if (phase >= 1.0f)
            phase -= 1.0f;
    }

    lfoPhase = phase;
}