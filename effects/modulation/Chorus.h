#ifndef EFFECTPEDAL_CHORUS_H
#define EFFECTPEDAL_CHORUS_H

#include "../../api/AudioProcessor.h"
#include "../../api/FloatProperty.h"
#include "../../api/ListProperty.h"
#include "../../api/IntegerProperty.h"
#include "LFOEffect.h"

class Chorus : public LFOEffect {

public:
    ListProperty<std::string> waveform, interpolation;
    IntegerProperty delay, width;
    FloatProperty depth, frequency;
    ListProperty<unsigned int> numVoices;

    Chorus();

    ~Chorus() override;

    void init(unsigned int sampleRate, unsigned int maxFrameSize) override;

    void process(Packet &packet) override;

private:
    float *delayBuffer = nullptr;
    unsigned int delayBufferSamples;
    unsigned int delayWritePosition;
    float lfoPhase;
    float inverseSampleRate;
};

#endif //EFFECTPEDAL_CHORUS_H
