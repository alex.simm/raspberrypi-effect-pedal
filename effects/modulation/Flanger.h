#ifndef EFFECTPEDAL_FLANGER_H
#define EFFECTPEDAL_FLANGER_H

#include "../../api/AudioProcessor.h"
#include "../../api/FloatProperty.h"
#include "../../api/IntegerProperty.h"
#include "../../api/BooleanProperty.h"

class Flanger : public AudioProcessor {
public:
    IntegerProperty level, bpm, wet;

    Flanger();

    void init(unsigned int sampleRate, unsigned int maxFrameSize) override;

    void process(Packet &packet) override;

    void update() override;

private:
    int iVec0[2];
    uint IOTA;
    double fVec1[4096];
    double fConst1;
    double fRec1[2];
    double fRec2[2];
    double fRec0[2];
    double fSlow0, fSlow1, fSlow2, fSlow3, fSlow4, fSlow5;
};

#endif //EFFECTPEDAL_FLANGER_H
