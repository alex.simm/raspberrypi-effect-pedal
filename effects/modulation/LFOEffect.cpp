#include "LFOEffect.h"

LFOEffect::LFOEffect(std::string name) : AudioProcessor(name) {
}

float LFOEffect::lfo(float phase, int waveform) {
    float out;

    switch (waveform) {
        case 0: {
            out = 0.5f + 0.5f * sinf(TWO_PI * phase);
            break;
        }
        case 1: {
            if (phase < 0.25f)
                out = 0.5f + 2.0f * phase;
            else if (phase < 0.75f)
                out = 1.0f - 2.0f * (phase - 0.25f);
            else
                out = 2.0f * (phase - 0.75f);
            break;
        }
        case 2: {
            if (phase < 0.5f)
                out = 0.5f + phase;
            else
                out = phase - 0.5f;
            break;
        }
        case 3: {
            if (phase < 0.5f)
                out = 0.5f - phase;
            else
                out = 1.5f - phase;
            break;
        }
        default:
            out = 0.0f;
    }

    return out;
}