#include "Flanger.h"
#include <cmath>

using namespace std;

Flanger::Flanger() : AudioProcessor("Flanger"),
                     level("Level", -60, 10, 0, 5), //fHslider0
                     bpm("LFO BPM", 24, 360, 24, 2), //fHslider1
                     wet("Wet", 0, 100, 100, 5) //fVslider0
{
    properties.push_back(&level);
    properties.push_back(&bpm);
    properties.push_back(&wet);
}

void Flanger::init(unsigned int sampleRate, unsigned int maxFrameSize) {
    AudioProcessor::init(sampleRate, maxFrameSize);

    fConst1 = 0.10471975511965977 / sampleRate;
    IOTA = 0;

    for (int l0 = 0; (l0 < 2); l0 = (l0 + 1)) iVec0[l0] = 0;
    for (int l1 = 0; (l1 < 4096); l1 = (l1 + 1)) fVec1[l1] = 0.0;
    for (int l2 = 0; (l2 < 2); l2 = (l2 + 1)) fRec1[l2] = 0.0;
    for (int l3 = 0; (l3 < 2); l3 = (l3 + 1)) fRec2[l3] = 0.0;
    for (int l4 = 0; (l4 < 2); l4 = (l4 + 1)) fRec0[l4] = 0.0;
}

void Flanger::update() {
    fSlow0 = wet.getValue();
    fSlow1 = 1.0 - (0.01 * fSlow0);
    fSlow2 = 0.01 * (std::pow(10.0, (0.050000000000000003 * level.getValue())) * fSlow0);
    fSlow3 = fConst1 * bpm.getValue();
    fSlow4 = std::sin(fSlow3);
    fSlow5 = std::cos(fSlow3);
}

void Flanger::process(Packet &packet) {
    for (uint i = 0; i < packet.numSamples; ++i) {
        double fTemp0 = packet.samples[i];
        double fTemp1 = (fSlow2 * fTemp0);
        iVec0[0] = 1;
        double fTemp2 = ((0.5 * fRec0[1]) - fTemp1);
        fVec1[(IOTA & 4095u)] = fTemp2;
        fRec1[0] = ((fSlow4 * fRec2[1]) + (fSlow5 * fRec1[1]));
        fRec2[0] = ((double((1 - iVec0[1])) + (fSlow5 * fRec2[1])) - (fSlow4 * fRec1[1]));
        double fTemp3 = sampleRate * ((0.0050000000000000001 * (fRec1[0] + 1.0)) + 0.001);
        int iTemp4 = int(fTemp3);
        double fTemp5 = std::floor(fTemp3);
        fRec0[0] = (
                (fVec1[((IOTA - std::min<int>(2049u, std::max<int>(0, iTemp4))) & 4095u)] * (fTemp5 + (1.0 - fTemp3))) +
                ((fTemp3 - fTemp5) * fVec1[((IOTA - std::min<int>(2049u, std::max<int>(0, (iTemp4 + 1)))) & 4095u)]));
        packet.samples[i] = float(((fSlow1 * fTemp0) + (0.5 * (fTemp1 - fRec0[0]))));

        iVec0[1] = iVec0[0];
        IOTA++;
        fRec1[1] = fRec1[0];
        fRec2[1] = fRec2[0];
        fRec0[1] = fRec0[0];
    }
}