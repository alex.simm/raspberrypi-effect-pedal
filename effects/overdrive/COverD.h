#ifndef EFFECTPEDAL_COVERD_H
#define EFFECTPEDAL_COVERD_H

#include "../../api/AudioProcessor.h"
#include "../../api/FloatProperty.h"
#include "../../api/IntegerProperty.h"

class COverD : public AudioProcessor {
public:
    FloatProperty bass, treble, volume;
    IntegerProperty wet;

    COverD();

    void init(unsigned int sampleRate, unsigned int maxFrameSize) override;

    void process(Packet &packet) override;

    void update() override;

private:
    double fRec1[2];
    double fConst1;
    double fConst2;
    double fConst3;
    double fConst4;
    double fConst5;
    double fConst6;
    double fConst7;
    double fConst8;
    double fConst9;
    double fConst10;
    double fConst11;
    double fConst12;
    double fConst13;
    double fConst14;
    double fConst15;
    double fConst16;
    double fConst17;
    double fConst18;
    double fRec2[6];
    double fConst19;
    double fConst20;
    double fConst21;
    double fConst22;
    double fConst23;
    double fConst24;
    double fConst25;
    double fConst26;
    double fConst27;
    double fConst28;
    double fConst29;
    double fConst30;
    double fConst31;
    double fConst32;
    double fConst33;
    double fConst34;
    double fConst35;
    double fConst36;
    double fConst37;
    double fRec3[2];
    double fConst38;
    double fConst39;
    double fConst40;
    double fConst41;
    double fConst42;
    double fConst43;
    double fRec4[2];
    double fConst44;
    double fConst45;
    double fConst46;
    double fConst47;
    double fConst48;
    double fConst49;
    double fConst50;
    double fConst51;
    double fConst52;
    double fConst53;
    double fConst54;
    double fConst55;
    double fConst56;
    double fConst57;
    double fConst58;
    double fConst59;
    double fConst60;
    double fConst61;
    double fConst62;
    double fConst63;
    double fConst64;
    double fConst65;
    double fConst66;
    double fConst67;
    double fConst68;
    double fConst69;
    double fConst70;
    double fConst71;
    double fConst72;
    double fConst73;
    double fConst74;
    double fConst75;
    double fConst76;
    double fConst77;
    double fConst78;
    double fConst79;
    double fConst80;
    double fConst81;
    double fConst82;
    double fConst83;
    double fConst84;
    double fConst85;
    double fConst86;
    double fConst87;
    double fConst88;
    double fConst89;
    double fConst90;
    double fConst91;
    double fConst92;
    double fConst93;
    double fConst94;
    double fConst95;
    double fConst96;
    double fConst97;
    double fConst98;
    double fConst99;
    double fConst100;
    double fConst101;
    double fConst102;
    double fConst103;
    double fConst104;
    double fConst105;
    double fConst106;
    double fConst107;
    double fConst108;
    double fConst109;
    double fConst110;
    double fConst111;
    double fConst112;
    double fConst113;
    double fConst114;
    double fConst115;
    double fConst116;
    double fConst117;
    double fConst118;
    double fRec0[6];
    double fConst119;
    double fConst120;
    double fConst121;
    double fConst122;
    double fConst123;
    double fConst124;
    double fConst125;
    double fConst126;
    double fConst127;
    double fConst128;
    double fConst129;
    double fConst130;
    double fConst131;
    double fConst132;
    double fConst133;
    double fConst134;
    double fConst135;
    double fConst136;
    double fConst137;
    double fConst138;
    double fConst139;
    double fConst140;
    double fConst141;
    double fConst142;
    double fConst143;
    double fConst144;
    double fConst145;
    double fConst146;
    double fConst147;
    double fConst148;
    double fConst149;
    double fConst150;
    double fConst151;
    double fConst152;
    double fConst153;
    double fConst154;
    double fConst155;
    double fConst156;
    double fConst157;
    double fConst158;
    double fConst159;
    double fConst160;
    double fConst161;
    double fConst162;
    double fConst163;
    double fConst164;
    double fConst165;
    double fConst166;
    double fConst167;
    double fConst168;
    double fConst169;
    double fConst170;
    double fConst171;
    double fConst172;
    double fConst173;
    double fConst174;
    double fConst175;
    double fConst176;
    double fConst177;
    double fConst178;
    double fConst179;
    double fConst180;
    double fConst181;
    double fConst182;
    double fConst183;
    double fConst184;
    double fConst185;
    double fConst186;
    double fConst187;

    double wetness, dryness, volumeValue, bassValue, trebleValue;
};

#endif //EFFECTPEDAL_COVERD_H
