#ifndef EFFECTPEDAL_OVERDRIVE_H
#define EFFECTPEDAL_OVERDRIVE_H

#include "../../api/AudioProcessor.h"
#include "../../api/IntegerProperty.h"
#include "../../api/FloatProperty.h"

class Overdrive : public AudioProcessor {
public:
    IntegerProperty wet;
    FloatProperty drive, levelOut;

    Overdrive();

    void init(unsigned int sampleRate, unsigned int maxFrameSize) override;

    void update() override;

    void process(Packet &packet) override;

private:
    float currentWet, currentDrive, driveMinus1, wetSquared, driveAmp, oneMinusWet, currentLevel;
    double decayingDrive[2];
};


#endif //EFFECTPEDAL_OVERDRIVE_H
