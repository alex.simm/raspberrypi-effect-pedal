#include <cmath>
#include "Overdrive.h"

using namespace std;

Overdrive::Overdrive() : AudioProcessor("Overdrive"),
                         wet("Wet", 0, 100, 100),
                         drive("Drive", 1.0f, 20.0f, 15.0f, 0.5f),
                         levelOut("Level out", 0.0f, 1.0f, 0.3f, 0.05f) {
    properties.push_back(&drive);
    properties.push_back(&wet);
    properties.push_back(&levelOut);
}

void Overdrive::init(unsigned int sampleRate, unsigned int maxFrameSize) {
    AudioProcessor::init(sampleRate, maxFrameSize);

    decayingDrive[0] = decayingDrive[1] = 0;
}

void Overdrive::update() {
    currentWet = 0.01f * wet.getValue();
    wetSquared = currentWet * currentWet;
    oneMinusWet = 1.0f - currentWet;
    currentDrive = drive.getValue();
    driveMinus1 = currentDrive - 1;
    driveAmp = powf(10.0f, -0.05f * currentDrive / 2.0f) / 1000.0f;
    currentLevel = levelOut.getValue();
}

//~ 320 mus
void Overdrive::process(Packet &packet) {
    double in, wetIn, norm, factor;

    for (uint i = 0; i < packet.numSamples; i++) {
        in = (double) packet.samples[i];
        wetIn = fabs(currentWet * in);
        decayingDrive[0] = 0.999 * decayingDrive[1] + driveAmp;
        norm = 1 + wetSquared * in * in + driveMinus1 * wetIn;
        factor = oneMinusWet + currentWet * decayingDrive[0] * (currentDrive + wetIn) / norm;
        packet.samples[i] = (float) (in * factor * currentLevel);

        // post processing
        decayingDrive[1] = decayingDrive[0];
    }
}