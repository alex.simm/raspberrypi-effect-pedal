#ifndef EFFECTPEDAL_LOGGER_H
#define EFFECTPEDAL_LOGGER_H

#include <string>
#include <fstream>
#include "utils/Consumer.h"

class Logger : public Consumer<std::string> {
public:
    enum Level {
        DEBUG = 0, INFO = 1, WARNING = 2, ERROR = 3, FATAL = 4
    };

    static Logger &getInstance() {
        static Logger instance;
        return instance;
    }

    Logger(Logger const &) = delete;

    void operator=(Logger const &) = delete;

    Level getLevel() const;

    void setLevel(Level level);

    static void log(Level level, std::string message);

    static void debug(std::string message);

    static void info(std::string message);

    static void warning(std::string message);

    static void error(std::string message);

    static void fatal(std::string message);

protected:
    void consume(std::string *s) override;

private:
    static const unsigned int MAX_FILE_SIZE = 5 * 1024 * 1024;
    const std::string LEVEL_NAMES[5] = {"DEBUG", "INFO", "WARNING", "ERROR", "SEVERE"};

    std::ofstream fileOut;
    Level threshold;

    Logger();

    void logImpl(Level level, std::string message);
};

#endif //EFFECTPEDAL_LOGGER_H
